#ifndef MISSILE_H
#define MISSILE_H

#include "unit.h"

struct missile {
	struct vector location;
	float direction;
	struct unit * target;
	double primer;
};

#endif
