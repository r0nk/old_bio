#ifndef POLY_H
#define POLY_H

#include <stdio.h>
#include <math.h>

struct vector {
	float x,y,z;
};

struct line {
	struct vector a,b;
};

static inline struct vector v_neg(struct vector v)
{
	v.x =-v.x;
	v.y =-v.y;
	v.z =-v.z;
	return v;
}

static inline struct vector normal(struct vector v)
{
	struct vector n;
	n.z = v.x * v.y;
	n.x = v.y * v.z;
	n.y = v.z * v.x;
	return n;
}

static inline void normalize(struct vector *  p )
{
	float w = sqrt( p->x * p->x + p->y * p->y + p->z * p->z );
	p->x /= w;
	p->y /= w;
	p->z /= w;
}

static inline int v_eq(struct vector a, struct vector b)
{
	return 	(	(a.x==b.x) &&
			(a.y==b.y) &&
			(a.z==b.z));
}

static inline struct vector v_add(struct vector a,struct vector b )
{
	struct vector ret;
	ret.x = a.x+b.x;
	ret.y = a.y+b.y;
	ret.z = a.z+b.z;
	return ret;
}

static inline struct vector v_sub(struct vector a,struct vector b )
{
	struct vector ret;
	ret.x = a.x-b.x;
	ret.y = a.y-b.y;
	ret.z = a.z-b.z;
	return ret;
}

static inline struct vector v_cross(struct vector a, struct vector b)
{
	struct vector c = {a.y*b.z - a.z*b.y,
		a.z*b.x - a.x*b.z,
		a.x*b.y - a.y*b.x};
	return c;
}

static inline struct vector v_dot(struct vector a,struct vector b )
{
	struct vector ret;
	ret.x = a.x*b.x;
	ret.y = a.y*b.y;
	ret.z = a.z*b.z;
	return ret;
}

static inline struct vector v_s_mul(double scalar, struct vector v)
{
	v.x*=scalar;
	v.y*=scalar;
	v.z*=scalar;
	return v;
}

static inline struct vector random_direction()
{
	struct vector v;
	v.x = ((rand()%20)-10)/8.0;
	v.y = ((rand()%20)-10)/8.0;
	v.z = ((rand()%20)-10)/8.0;
	return v;
}

static inline float pit_sign(struct vector p1, struct vector p2,
		struct vector p3)
{
	return (p1.x - p3.x) * (p2.z - p3.z) - (p2.x - p3.x) * (p1.z - p3.z);
}

/*for 2d points only*/
static inline int point_in_triangle (struct vector pt,
		struct vector v1, struct vector v2, struct vector v3)
{
	int b1, b2, b3;

	b1 = (pit_sign(pt, v1, v2) < 0.0f);
	b2 = (pit_sign(pt, v2, v3) < 0.0f);
	b3 = (pit_sign(pt, v3, v1) < 0.0f);

	return ((b1 == b2) && (b2 == b3));
}

typedef struct vector point;

static inline void dump_vector(struct vector v)
{
	printf("(%f,%f,%f)",v.x,v.y,v.z);
}

#endif
