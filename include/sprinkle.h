#ifndef SPRINKLE_H
#define SPRINKLE_H

#include "poly.h"
#include "model.h"

struct sprinkle {
	struct vector size;
	int type;
	int level;
};

#define MAX_SPRINKLE_CATALOG_SIZE 500

extern int n_sprinkles_in_catalog;
struct sprinkle sprinkle_catalog[MAX_SPRINKLE_CATALOG_SIZE];

static inline add_sprinkle_to_catalog(struct sprinkle sprinkle)
{
	sprinkle_catalog[n_sprinkles_in_catalog]=sprinkle;
	n_sprinkles_in_catalog++;
}

#endif
