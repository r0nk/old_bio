#ifndef AUDIO_H
#define AUDIO_H

void set_music(int on);
void init_audio();
void play_coin_sound_effect();
void play_laser_sound_effect(float distance);
void play_hit_sound_effect();
void play_explosion_sound_effect();
void play_blip_sound_effect();
void play_lvlup_sound_effect();
void play_teledice_sound_effect();
void play_reel_sound_effect();
void play_risset_sound_effect(float distance);
void play_oom_sound_effect();
void play_sevens_sound_effect();
void play_rocket_sound_effect();
void play_dash_sound_effect();
void play_chips_sound_effect();
void play_planet_impact_sound_effect();

void play_chainsaw_sound_effect();
void stop_chainsaw_sound_effect();

void play_plasma_sound_effect();
void stop_plasma_sound_effect();

void play_big_laser_sound_effect();
void stop_big_laser_sound_effect();

void set_brownian_volume(float volume);

void stop_sounds();

#endif
