#ifndef ITEM_H
#define ITEM_H

#include <stdlib.h>
#include "poly.h"

#define ITEM_TELEDICE                   1
#define ITEM_SHIELD                     2
#define ITEM_CAPACITOR                  3
#define ITEM_DASH                       4
#define ITEM_PLASMA                     5
#define ITEM_LASER                    	6
#define ITEM_VULCAN                    	7
#define ITEM_MISSILE                    8
#define ITEM_SIDE_BLASTER		9
#define ITEM_HOOK			10
#define ITEM_CHAINSAW			11
#define ITEM_FLAMETHROWER		12

#define N_ITEMS 12

struct item {
        int type;
	int discrete; /* whether or not this item is continuously activated */
	struct vector location;
	struct vector direction;
        double cooldown;
        double max_cooldown;
        double duration;
        int active; /* whether or not the player activated this item */
	int dragging; /*whether or not this item is currently being dragged*/
	int state; 
	int hooked_npc; /*the index of the npc hooked, for the hook item*/
	double rotation_angle;
	double rotation_delta;
};

#define ITEM_STATE_HOOK_OPEN		0
#define ITEM_STATE_HOOK_CLOSED		1
#define ITEM_STATE_ON			1
#define ITEM_STATE_OFF			0

static struct item laser_item()
{
        struct item item = {0};
        item.type = ITEM_LASER;
        item.discrete = 0;
        item.max_cooldown = 1.6;
        item.cooldown = item.max_cooldown;
        item.active = 0;
        return item;
}

static struct item capacitor_item()
{
        struct item item = {0};
        item.type = ITEM_CAPACITOR;
        item.discrete = 1;
        item.cooldown = 0;
        item.max_cooldown = 10;
        item.active = 0;
        return item;
}

static struct item dash_item()
{
        struct item item = {0};
        item.type = ITEM_DASH;
        item.discrete = 1;
        item.max_cooldown = 1.0;
        item.cooldown = item.max_cooldown;
        item.active = 0;
        return item;
}

static struct item shield_item()
{
        struct item item = {0};
        item.type = ITEM_SHIELD;
        item.discrete = 0;
        item.cooldown = 1;
        item.max_cooldown = 2;
        return item;
}

static struct item teledice_item()
{
        struct item item = {0};
        item.type = ITEM_TELEDICE;
        item.discrete = 1;
        item.cooldown = 1;
        item.max_cooldown = 2;
        item.active = 0;
        return item;
}
static struct item plasma_item()
{
        struct item item = {0};
        item.type = ITEM_PLASMA;
        item.discrete = 0;
        item.cooldown = 0.0;
        item.max_cooldown = 1.0;
        item.active = 0;
        return item;
}

static struct item side_blaster_item()
{
        struct item item = {0};
        item.type = ITEM_SIDE_BLASTER;
        item.discrete = 1;
        item.cooldown = 0.0;
        item.max_cooldown = 0.4;
        item.active = 0;
        return item;
}

static struct item vulcan_item()
{
        struct item item = {0};
        item.type = ITEM_VULCAN;
        item.discrete = 0;
        item.cooldown = 1.0;
        item.active = 0;
	item.rotation_angle=0;
	item.rotation_delta=0;
        return item;
}

static struct item missile_item()
{
        struct item item = {0};
        item.type = ITEM_MISSILE;
        item.discrete = 1;
        item.cooldown = 1.0;
        item.max_cooldown = 0.60;
        item.active = 0;
	item.rotation_angle=0;
	item.rotation_delta=0;
        return item;
}

static struct item hook_item()
{
        struct item item = {0};
        item.type = ITEM_HOOK;
        item.discrete = 1;
        item.cooldown = 1.0;
        item.max_cooldown = 2.0;
        item.active = 0;
	item.rotation_angle=0;
	item.rotation_delta=0;
        return item;
}

static struct item chainsaw_item()
{
        struct item item = {0};
        item.type = ITEM_CHAINSAW;
        item.discrete = 0;
        item.cooldown = 1.0;
        item.max_cooldown = 2.0;
        item.active = 0;
	item.rotation_angle=0;
	item.rotation_delta=0;
        return item;
}

static struct item flamethrower_item()
{
        struct item item = {0};
        item.type = ITEM_FLAMETHROWER;
        item.discrete = 0;
        item.max_cooldown = 2.0;
        item.cooldown = item.max_cooldown;
        item.active = 0;
	item.rotation_angle=0;
	item.rotation_delta=0;
        return item;
}

static char* get_item_name(struct item item)
{
        switch(item.type){
                case ITEM_TELEDICE:
                        return "teledice";
                case ITEM_SHIELD:
                        return "sheild";
                case ITEM_CAPACITOR:
                        return "capacitor";
                case ITEM_DASH:
                        return "dash circuit";
                case ITEM_PLASMA :
                        return "plasma orb";
                case ITEM_HOOK :
                        return "hook";
                default:
                        return "NAME_NOT_FOUND";
                        break;
        }
}
static struct item pick_item()
{
        int r;
repick:
	r = 1+(rand()%N_ITEMS);
        switch(r){
                case 0:
                case ITEM_TELEDICE:
                        return teledice_item();
                case ITEM_SHIELD:
                        return shield_item();
                case ITEM_CAPACITOR:
                        return capacitor_item();
                case ITEM_DASH:
                        return dash_item();
                case ITEM_PLASMA :
                        return plasma_item();
                case ITEM_LASER :
                        return laser_item();
                case ITEM_VULCAN :
                        return vulcan_item();
                case ITEM_MISSILE :
                        return missile_item();
                case ITEM_SIDE_BLASTER :
                        return side_blaster_item();
                case ITEM_HOOK :
                        return hook_item();
                case ITEM_CHAINSAW :
                        return chainsaw_item();
                case ITEM_FLAMETHROWER :
                        return flamethrower_item();
                default:
                        printf("failed to pick item, r=%i\n",r);
                        exit(2);
                        break;
        }
}

static void dump_item(struct item i)
{
        printf("type:%i\n",i.type);
        printf("cooldown:%f\n",i.cooldown);
        printf("duration:%f\n",i.duration);
        printf("active:%i\n",i.active);
}

#endif
