#ifndef MAP_H
#define MAP_H

#include "room.h"
#include "graph.h"

#define MAX_ROOMS 10
#define MAX_EDGES 20

struct map {
	struct room * current_room;
	int n_doorways;
	int level;/* current floor level */
	int n_rooms;
	struct room room[MAX_ROOMS];
	struct graph graph;
};

void generate_map(struct map * map,int level);
void generate_edges(struct map * map);
void transfer_map(int level);

void move_through_doorway(struct map * map,int t);

extern struct map world_map;

static inline int walkable_char( char c)
{
	return (c && (c!=' ') && (c!='#'));
}

int walkable(int i, int j); /*if current_room[i][j] is walkable*/

#endif
