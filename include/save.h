#ifndef SAVE_H
#define SAVE_H

struct save_state {
	int audio;
	int blaster_kills;
	int life_stolen;
	int coins_spent;
	int unibar_picks;
	int times_leveled;
};

struct save_state global_save_state;

void read_save_state();
void write_save_state();

#endif
