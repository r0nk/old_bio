#ifndef NEIGHBORHOOD
#define NEIGHBORHOOD

#include "poly.h"
#include "graph.h"
#include "room.h"

struct neighborhood {
	struct vector a,b,c,d;
};

struct graph layout_to_neighborhoods(struct layout * layout);
int point_in_neighborhood(struct neighborhood n, struct vector p);

/* this assumes the neighborhood is a rectangle */
static inline struct vector neighborhood_midpoint(struct neighborhood n)
{
	struct vector v;
	v.x = n.a.x + ((n.c.x-n.a.x)/2.0);
	v.y = 0;
	v.z = n.a.z + ((n.c.z-n.a.z)/2.0);
	return v;
}

static inline float neighborhood_area(struct neighborhood n)
{
	float w = n.b.x-n.a.x;
	float l = n.c.z-n.a.z;
	return w*l;
}
 
#endif
