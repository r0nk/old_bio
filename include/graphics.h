#ifndef GRAPHICS_H
#define GPRAHICS_H

#include <GL/glu.h>
#include "poly.h"
#include "unit.h"
#include "game_state.h"

extern int window_width,window_height;
extern float frame_x,frame_y;
extern float frame_ratio;

extern float transition_effect_timer;

void resize_fbo(int w, int h);
int init_graphics();
void reset_lights();
void graphics_draw();
void draw_letter(struct vector location,int c);
void deinit_graphics();

void add_damage_number(struct vector location, float amount);

void draw_unit_shadow(struct unit* u, double d);
void draw_carrot(struct vector location);
void draw_particles(struct game_state* gs, double d);

void reset_particles();
void deinit_models();

extern GLuint font_texture;
extern GLuint whitey_texture;

#endif
