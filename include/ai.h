#ifndef AI_H
#define AI_H

#include "game_state.h"

void update_npcs(struct game_state * gs, double delta);

#endif
