#ifndef CARD_H
#define CARD_H

#include "poly.h"

#define STATS_CARD 		0
#define LIFESTEAL_CARD 		1
#define COINS_CARD 		2
#define POISON_CARD 		3
#define BOUNCE_CARD 		4
#define UNIBAR_CARD 		5
#define ROUGE_CARD 		6
#define LEARN_CARD 		7
#define WRATH_CARD 		8
/* Yes I know how bad it its that the sevens card isn't at 7 */
#define SEVENS_CARD 		9
#define OVERCLOCK_CARD 		10

#define N_CARDS 11

struct card {
	        point location;
		struct vector rotation;
		double rotation_angle;
		int type;
		char * name;
		char * description;
};


static inline struct card stats_card()
{
	struct card card;
	card.type = STATS_CARD;
	card.name = "Stats";
	card.description = "Increases your health, energy, damage, speed.";
	return card;
}

static inline struct card lifesteal_card()
{
	struct card card;
	card.type = LIFESTEAL_CARD;
	card.name = "LIFESTEAL";
	card.description = "Gives your bullets lifesteal";
	return card;
}

static inline struct card coins_card()
{
	struct card card;
	card.type = COINS_CARD;
	card.name = "COINS";
	card.description = "Double the number of coins you have";
	return card;
}

static inline struct card poison_card()
{
	struct card card;
	card.type = POISON_CARD;
	card.name = "POISON";
	card.description = "Your attacks deal damage over time";
	return card;
}

static inline struct card bounce_card()
{
	struct card card;
	card.type = BOUNCE_CARD;
	card.name = "BOUNCE";
	card.description = "Your bullets bounce off of walls.";
	return card;
}

static inline struct card unibar_card()
{
	struct card card;
	card.type = UNIBAR_CARD;
	card.name = "UNIBAR";
	card.description = "Merges your mana and health bars into one";
	return card;
}

static inline struct card rouge_card()
{
	struct card card;
	card.type = ROUGE_CARD;
	card.name = "ROUGELIKE";
	card.description = "Red waves now increase damage instead of hurting you.";
	return card;
}


static inline struct card learn_card()
{
	struct card card;
	card.type = LEARN_CARD;
	card.name = "SMARTS";
	card.description = "Gain levels faster.";
	return card;
}

static inline struct card wrath_card()
{
	struct card card;
	card.type = WRATH_CARD;
	card.name = "WRATH";
	card.description = "Recover energy faster at lower health.";
	return card;
}

static inline struct card sevens_card()
{
	struct card card;
	card.type = SEVENS_CARD;
	card.name = "LUCKY SEVENS";
	card.description = "Components have a chance to activate twice.";
	return card;
}

static inline struct card overclock_card()
{
	struct card card;
	card.type = OVERCLOCK_CARD;
	card.name = "OVERCLOCK";
	card.description = "Dealing damage reduces your cooldowns.";
	return card;
}

static inline struct card pick_card()
{
	struct card card;
	switch(rand()%N_CARDS)
	{
		case STATS_CARD:
			card = stats_card();
			break;
		case LIFESTEAL_CARD:
			card = lifesteal_card();
			break;
		case COINS_CARD:
			card = coins_card();
			break;
		case POISON_CARD:
			card = poison_card();
			break;
		case BOUNCE_CARD:
			card = bounce_card();
			break;
		case UNIBAR_CARD:
			card = unibar_card();
			break;
		case ROUGE_CARD:
			card = rouge_card();
			break;
		case LEARN_CARD:
			card = learn_card();
			break;
		case WRATH_CARD:
			card = wrath_card();
			break;
		case SEVENS_CARD:
			card = sevens_card();
			break;
		case OVERCLOCK_CARD:
			card = overclock_card();
			break;
	}
	card.location = (struct vector){0,0,0};
	card.rotation_angle = 45;
	card.rotation = (struct vector){1,0,0};
	return card;
}

#endif
