#ifndef ANIMATION_H
#define ANIMATION_H

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

void get_mat(struct aiNodeAnim *chan,double dpl, float mat[16]);

#endif
