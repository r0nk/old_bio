#ifndef INVENTORY_H
#define INVENTORY_H

#include <stdlib.h>
#include <stdio.h>
#include "item.h"
#include "poly.h"

/* units are in the game world, items are in inventories */

#define MAX_INVENTORY_SPACE 4

static struct vector box_point[4] = {	{-8,-5.2,0},
					{-8.8,-6,0},
					{-7.2,-6,0},
					{-8,-6.8,0}};

struct inventory {
	int n_items;
	struct item item[MAX_INVENTORY_SPACE];
};

static void add_item(struct inventory * inven, struct item item)
{
	int i;
	for(i=0;i<4;i++){
		if(inven->item[i].type == 0){
			inven->item[i] = item;
			return;
		}
	}
	printf("Could not add item, inventory full\n");
}

static void remove_item(struct inventory * inven,int i)
{
	for(;i<inven->n_items;i++)
		inven->item[i] = inven->item[i+1];
	inven->n_items--;
}

static void swap_items(struct inventory * inven, int a , int b)
{
	struct item temp = inven->item[a];
	inven->item[a] = inven->item[b];
	inven->item[b] = temp;
}

#endif
