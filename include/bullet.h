#ifndef BULLET_H
#define BULLET_H

struct bullet {
        double speed;
        point location;
        struct vector direction;
        double duration;
        double damage;
        int flags;
        struct unit * shooter;
};

static void dump_bullet(struct bullet b){
        printf("speed:%f\n",b.speed);
        printf("location:");
        dump_vector(b.location);
        printf("\n direction:");
        dump_vector(b.direction);
}

#endif
