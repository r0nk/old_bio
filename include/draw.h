#ifndef DRAW_H
#define DRAW_H

#include "poly.h"

void draw_field_line(struct vector start, struct vector end,
		struct vector c1,struct vector c2);
void draw_line(struct vector start, struct vector end, struct vector color,
		float width, float alpha);
void draw_2line(struct vector start, struct vector end,
		struct vector c1, struct vector c2,float a1,float a2,
		float width);

#endif
