#ifndef PARTICLE_H
#define PARTICLE_H

#include "poly.h"

#define MAX_EXPLOSIONS 		5000
#define MAX_SPARKS 		100
#define MAX_TRAILS 		1000
#define MAX_PORTICLES 		1000
#define MAX_MUZZLE 		100
#define MAX_DAMAGE_NUMBERS 	1000

#define PKIND_BULLET 		1
#define PKIND_LVLRING 		2
#define PKIND_SPLAT 		3
#define PKIND_SCORCH 		4

#define CHUNK_DOT 		1
#define CHUNK_BIT 		2

#define SPRINK_INTERGRID	1
#define SPRINK_VALVE		2
#define SPRINK_GEAR		3
#define SPRINK_LEAF		4
#define SPRINK_EXAMPLE		5
#define SPRINK_STORAGE_TANK	6
#define SPRINK_STRAIGHT_PIPE	7

#define INTERGRID_KIND_VERT	0
#define INTERGRID_KIND_HORI	1

struct particle{
	int alive;
	struct vector location;
	struct vector direction;
	struct vector color;
	struct vector size;
	float amount;
	int type;
	int kind;
	double frame;
};

extern struct particle explosion[];
extern struct particle spark[];
extern struct particle trail[];
extern struct particle porticle[];/*portal particles, port-icles, get it?*/
extern struct particle muzzle[];
extern struct particle damage_number[];

#endif
