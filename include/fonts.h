#ifndef FONTS_H
#define FONTS_H

#include "poly.h"

#define FONT_WIDTH  0.5
#define FONT_HEIGHT 0.5

void draw_text(double x, double y, const char * text, struct vector color);

#endif
