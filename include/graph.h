/* as in graph theory */ 
#ifndef GRAPH_H
#define GRAPH_H

struct edge {
	int a;
	int b;
};

static inline void dump_edge(struct edge * e)
{
	printf("%i -> %i\n",e->a,e->b);
}

#define MAX_GRAPH_ELEMENTS 2500

#define MAX_GRAPH_EDGES 7500

struct graph {
	int n_elements;
	void * element[MAX_GRAPH_ELEMENTS];
	int n_edges;
	struct edge edge[MAX_GRAPH_EDGES];
};

static inline void add_element_to_graph(struct graph * g, void * element)
{
	if(g->n_elements>=MAX_GRAPH_ELEMENTS) {
		printf("tried to add over max graph elements\n");
		exit(-17);
	}
	g->element[g->n_elements]=element;
	g->n_elements++;
}
 
static inline void add_edge_to_graph(struct graph * g, struct edge edge)
{
	if(g->n_edges>=MAX_GRAPH_EDGES) {
		printf("tried to add over max graph edges\n");
		exit(-17);
	}
	g->edge[g->n_edges]=edge;
	g->n_edges++;
}

static inline void free_graph(struct graph * g)
{
	int i;
	for(i=0;i<g->n_elements;i++){
		free(g->element[i]);
	}
	g->n_elements=0;
	g->n_edges=0;
}

static inline int degree(struct graph *g , int index)
{
	if(index>(g->n_elements)){
		fprintf(stderr,"ERR:tried to find degree of element out of range:%i\n",index);
		exit(-2345);
		return -1;
	}

	int degree=0;
	int i;
	for(i=0;i<(g->n_edges);i++){
		if(g->edge[i].a==index)
			degree++;
		if(g->edge[i].b==index)
			degree++;
	}
	return degree;
}

static inline int incident(struct graph *g, int index1, int index2)
{
	if(index1>(g->n_elements) || index2>(g->n_elements)){
		fprintf(stderr,"ERR:tried to incident elements out of range:%i,%i\n",index1,index2);
		return -1;
	}

	int i;
	for(i=0;i<(g->n_edges);i++){
		if(g->edge[i].a==index1 && g->edge[i].b==index2)
			return 1;
		if(g->edge[i].b==index2 && g->edge[i].b==index1)
			return 1;
	}
	return 0;
}

static inline int connected (struct graph * g)
{
	char visited[MAX_GRAPH_ELEMENTS];

	int i,j;
	for(i=0;i<MAX_GRAPH_ELEMENTS;i++){
		visited[i]=0;
	}
	visited[0]=1;
	for(i=0;i<g->n_edges;i++){
		for(j=0;j<g->n_edges;j++){
			if(visited[(g->edge[i].a)] || visited[(g->edge[i].b)]){
				visited[(g->edge[i].a)]=1;
				visited[(g->edge[i].b)]=1;
			}
		}
	}
	for(i=0;i<g->n_edges;i++){
		if(!visited[i])
			return 0;
	}
	return 1;
}

#endif
