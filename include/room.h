#ifndef ROOM_H
#define ROOM_H

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

#include "poly.h"
#include "dimensions.h"
#include "model.h"
#include "game_state.h"
#include "game.h"
#include "shop.h"
#include "particle.h"
#include "graph.h"

#define MAX_DOORWAYS 6

#define MAX_VECTOR_LAYOUT_LINES 8000

#define MAX_PERMANENTS	1000
#define MAX_CHUNKS	1000
#define MAX_SPRINKLES 	1000

struct vector_layout {
	int n_lines;
	struct line line[MAX_VECTOR_LAYOUT_LINES];
};

struct layout {
	char tiles[MAX_ROOM_WIDTH][MAX_ROOM_HEIGHT];
};

static inline void dump_layout( struct layout * l )
{
	int i,j;
	for(i=0;i<MAX_ROOM_HEIGHT;i++){
		for(j=0;j<MAX_ROOM_WIDTH;j++){
			if(l->tiles[j][i]=='\0')
				printf(" ",l->tiles[j][i]);
			else
				printf("%c",l->tiles[j][i]);
		}
		printf("\n");
	}
}


struct doorway {
	int index;
	struct vector location;
	int is_connected;
	struct vector color;
};

static inline void dump_doorway(struct doorway * doorway)
{
	printf("doorway->index:%i\n",doorway->index);
	printf("doorway->location:(%f,%f,%f)\n",doorway->location.x,
			doorway->location.y,doorway->location.z);
	printf("doorway->is_connected:%i\n",doorway->is_connected);
}

struct light {
	struct vector location;
	struct vector origin;
	struct vector velocity;
	struct vector color;
};

#define MAX_ROOM_LIGHTS 4

struct room {
	struct vector_layout vlayout;
	struct layout layout;
	struct layout sprinkle_layout;
	struct graph neighborhoods;
	int n_doorways;
	struct doorway doorway[MAX_DOORWAYS];
	struct game_state gs;
	struct vector color;
	struct model model;
	int has_shop;
	struct shop shop;
	int boss_room;
	int starting_room;
	int visited;
	int n_lights;
	struct light light[MAX_ROOM_LIGHTS];
	struct particle permanents[MAX_PERMANENTS];//bullet holes etc
	struct particle chunks[MAX_CHUNKS];// robot bits, etc
	struct particle sprinkles[MAX_SPRINKLES];//computer interfaces, etc
};

static inline void dump_room(struct room * room)
{
	printf("room->layout: TODO\n");
	printf("room->n_doorways: %i\n",room->n_doorways);
	int i;
	for(i=0;i<room->n_doorways;i++){
		printf("room->doorway[%i]:\n",i);
		dump_doorway(&(room->doorway[i]));
	}
}

#define LAYOUT_WALL '#'

struct room generate_room(int i, int level);
void get_layout(struct room * room,char * pathname);

struct shop generate_shop(struct room * room);

static int count_files(const char* directory){
        int i = 0;
        DIR *dir;
        struct dirent *entry;

        dir = opendir(directory);

        while ((entry = readdir(dir)) != NULL) {
		if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")||
		  strcmp(entry->d_name + strlen(entry->d_name) - 7, ".layout")){
			continue;
                }
                i++;
        }
        closedir(dir);
        return i;
}

#endif
