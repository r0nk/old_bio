#ifndef UNIT_H
#define UNIT_H

#include "inventory.h"
#include "pathfinding.h"
#include "speech.h"

#define UNIT_TYPE_PLAYER                1
#define UNIT_TYPE_SHOP                  2
#define UNIT_TYPE_ITEM                  3
#define UNIT_TYPE_NEUTRAL_CREEP         4
#define UNIT_TYPE_RANGER                5
#define UNIT_TYPE_BOSS                  6
#define UNIT_TYPE_MOLE                  7
#define UNIT_TYPE_YO                    8
#define UNIT_TYPE_SIGN                  9
#define UNIT_TYPE_ANTENNA               10
#define UNIT_TYPE_SHOTTY_RANGER         11
#define UNIT_TYPE_SPIDER                12
#define UNIT_TYPE_SENTRY		13
#define UNIT_TYPE_DOUBLE_SENTRY		14
#define UNIT_TYPE_SPINNER		15
#define UNIT_TYPE_SPINNER_BOSS		16
#define UNIT_TYPE_TANK			17
#define UNIT_TYPE_SPADE			18
#define UNIT_TYPE_SHIELDMAN		19
#define UNIT_TYPE_INTERCOM		20

#define PLAYER_TYPE_TELE 		1

#define MAX_CHARACTERS 			1

struct unit {
	double speed;
        double max_speed;
        int moving; /*whether or not this unit is moving*/
        struct vector rotation;
        double rotation_angle;
        struct vector location;
	struct vector destination;
        struct path path;
        double path_timer;
        double health;
        double max_health;
	double mana;
	double max_mana;
	double delta_mana;
        int level;
        int exp;
        int lvld;
        int lvling;
        int coins;
        double cooldown;
        double max_cooldown;
        double cooldown2;/* moles teleportation cooldown */
        int connected_to; /* index of other yo */
        double bullet_speed;
        double bullet_duration;
        double resist;
        double damage;
	float damage_buffer;
        double max_damage;
        struct inventory inventory;
        int type;
	int flags;
#define HAS_TRIGGER                     1
#define HAS_VAIL                        2
#define HAS_VECTOR_FIELD                4
#define HAS_BOUNCE                      8
#define HAS_LIFESTEAL                   16
#define HAS_UNIBAR                   	32
	char rouge;/*wether or not red waves harm (0) or buff (>=1)*/
	double flaming;
	double flame_timer;
	double poison_timer;
        double dash_timer;
	int dash_dir;
	struct speech speech;
	double hit_radius;
	int score;
	struct vector color;
	float hit_timer;
	float learn_rate;
	float wrath;
	char lucky_sevens;
	char overclock;
};

static void dump_unit(struct unit p){
        printf("speed:%f\n",p.speed);
        printf("location:");
        dump_vector(p.location);
        printf("\nhealth:%f\n",p.health);
}

#endif
