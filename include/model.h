#ifndef MODEL_H
#define MODEL_H
#include <GL/glu.h>
#include "poly.h"

struct model floor_tile(struct vector d,struct vector l);
struct model bullet();
struct model player_model();
struct model wall_block(struct vector c,struct vector d,struct vector l);

struct vertice {
	struct vector p;/*position*/
	struct vector n;/*normal*/
	struct vector c;/*color*/
};

struct polygon{
	struct vertice v[3];
};

struct text_coord {
	float u;
	float v;
};

struct mesh {
	struct vector *p;/*position array*/
	struct vector *n;/*normal array*/
	struct vector *t;/*uv array (z isn't used)*/
	unsigned int *indexes;
	GLuint p_vbo;
	GLuint n_vbo;
	GLuint uv_vbo;
	GLuint index_vbo;
	unsigned int n_indexes;
	unsigned int n_vertices;
};

#define MAX_MESHES 16

struct model{
	const struct aiScene * scene;
	GLuint tid;/*texture id for this model*/
	double cur_time;/*the current time of the animation*/
	double last_updated;
	unsigned int cardinality;
	struct polygon *poly;
	struct mesh mesh[MAX_MESHES];
};

void add_submodel(struct model * to, struct model * from);

#endif
