#ifndef MODEL_READER_H
#define MODEL_READER_H

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "model.h"

const struct aiScene * get_aiScene(char * path);
struct model get_model(char * path);

#endif
