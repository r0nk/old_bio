#ifndef FUTIL_H
#define FUTIL_H

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int fsize(int fd)
{
	struct stat buf;
	fstat(fd,&buf);
	return buf.st_size;
}

/*return a char pointing to allocated char array, with file contents*/
char * read_file(char * path)
{
	FILE* file = fopen(path, "rb");
	char* c = NULL;
	if(file != NULL){
		fseek(file, 0, SEEK_END);
		unsigned int size = ftell(file);
		fseek(file, 0, SEEK_SET);
		c = (char*)malloc(size + 1);
		fread(c, size, 1, file);
		c[size] = '\0';
		fclose(file);
	}
	return c;
}

#endif
