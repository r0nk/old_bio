#ifndef FIELD_H
#define FIELD_H

#include "dimensions.h"
#include "poly.h"

struct field
{
	/*for each cell vector:
	 * x = x velocity
	 * y = "pressure"
	 * z = z velocity
	 */
	struct vector cell[MAX_ROOM_WIDTH][MAX_ROOM_HEIGHT];
};

extern double wave_destruction;

#endif
