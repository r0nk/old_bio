#ifndef GAME_STATE_H
#define GAME_STATE_H

#include <stdio.h>

#include "card.h"
#include "unit.h"
#include "bullet.h"
#include "particle.h"
#include "missile.h"
#include "field.h"

#define MAX_NPCS 50
#define MAX_BULLETS 	500
#define MAX_FIRES 	5000
#define MAX_MISSILES 	100

struct game_state {
	struct unit game_player;
	int n_npcs;
	struct unit npc[MAX_NPCS];
	int n_bullets;
	struct bullet bullet[MAX_BULLETS];
	int n_missiles;
	struct missile missile[MAX_MISSILES];
	struct particle fire[MAX_FIRES];
	struct card card1;
	struct card card2;
	struct field bfield;/*blue field, gives speed and fire rate */
	struct field rfield;/*red field, deals damage*/
	struct field gfield;/*green field, heals*/
};

static inline void remove_missile(struct game_state * gs, int i)
{
	for(;i<gs->n_missiles;i++)
		gs->missile[i] = gs->missile[i+1];
	gs->n_missiles--;
}

static inline void add_missile(struct game_state * gs, struct missile b)
{
	if(gs->n_bullets>MAX_MISSILES)
		return;
	gs->missile[gs->n_missiles]=b;
	gs->n_missiles++;
}

static inline void remove_bullet(struct game_state * gs, int i)
{
	for(;i<gs->n_bullets;i++)
		gs->bullet[i] = gs->bullet[i+1];
	gs->n_bullets--;
}

static inline void add_bullet(struct game_state * gs, struct bullet b)
{
	if(gs->n_bullets>MAX_BULLETS)
		return;
	gs->bullet[gs->n_bullets]=b;
	gs->n_bullets++;
}

static inline void remove_npc(struct game_state * gs, int i)
{
	for(;i<gs->n_npcs;i++)
		gs->npc[i] = gs->npc[i+1];
	gs->n_npcs--;
}

static inline void add_npc(struct game_state * gs, struct unit n)
{
	if(gs->n_npcs>MAX_NPCS)
		return;
	gs->npc[gs->n_npcs]=n;
	gs->n_npcs++;
}

#endif
