#ifndef INPUT_H
#define INPUT_H

#include <stdio.h>
#include "graphics.h"
#include "poly.h"
#include "map.h"
#include "camera.h"

#define MAX_KEYS 500

struct controller {
	char a,b,x,y;
	char hat;
	int a1x,a1y;
	int a2x,a2y;
	char rb,lb;
	int lt,rt;
};

struct player_input {
	char using_controller;
	struct controller controller;
	char left_click,right_click;
	double mouse_x,mouse_y;
	int shift_key;
	char keys[MAX_KEYS];
};

struct player_input pi;

static inline void dump_player_input(struct player_input pi)
{
	printf("player_input:\n");
	printf(" left_click:%i, right click:%i\n",pi.left_click,pi.right_click);
	printf(" mouse {%f,%f}\n",pi.mouse_x,pi.mouse_y);
	printf("keys:\n");
	int i;
	for(i=0;i<MAX_KEYS;i++){
		if(pi.keys[i])
			printf("%c",i);
	}
	printf("\n");
}

static inline struct vector world_to_screen(struct game_state * gs,
		struct vector location)
{
	struct vector v = (struct vector) {0,0,0};
	
	v.x = location.z - location.x;
	v.y = -(location.x + location.z);
	v.z = 0;

	v.x += last_camera.x - last_camera.z;
	v.y += last_camera.x + last_camera.z;

	v.x/=1.3333;
	v.y/=2.3333;

	v.x*=0.8;
	v.y*=0.8;

	return v;
}

static inline struct vector screen_to_world(struct game_state * gs,int x, int y)
{
	struct vector ret;

	double tile_width = 1024.0/18.0;
	double tile_height = 768.0/23.0;

	double virtual_Tile_X = (x-(window_width/2)) / tile_width;
	double virtual_Tile_Y = (y-(window_height/2)) / tile_height;

	ret.x=(virtual_Tile_X+virtual_Tile_Y) + last_camera.x;
	ret.y=0;
	ret.z=(virtual_Tile_Y-virtual_Tile_X) + last_camera.z;

	return ret;
}

static inline struct vector pixel_to_screen(int x, int y)
{
	struct vector v;
	x -= (window_width/2);
	y -= (window_height/2);
	v.x=x;
	v.y=y;
	v.x/=window_width;
	v.y/=window_height;
	v.x*=(frame_x*2);
	v.y*=(frame_y*2);
	v.x=-v.x;
	v.y=-v.y;
	v.z=0;
	return v;
}
 
#endif
