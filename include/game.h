#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include "poly.h"
#include "room.h"
#include "game_state.h"
#include "pathfinding.h"
#include "model.h"
#include "inventory.h"
#include "unit.h"
#include "bullet.h"

struct room;

extern struct map world_map;

struct unit antenna_npc(struct vector, int);
struct unit yo_npc(struct vector);
struct unit item_npc(struct vector);
struct unit sign_npc(struct vector);
struct unit spinner_npc(struct vector);
struct unit scavenger_npc(struct vector);
struct unit sentry_npc(struct vector);
struct unit ranger_npc(struct vector);
struct unit shotty_ranger_npc(struct vector);
struct unit mole_npc(struct vector);
struct unit boss_npc(struct vector);
struct unit spider_npc(struct vector);

void spawn_boss(struct room*);
void spawn_sign(struct game_state*, int);
void spawn_spiders(struct game_state*, struct vector);
void spawn_mob(struct room*, int, int, int);
void spawn_mobs(struct room*, int);

struct unit init_player(int sp);
void init_fields(struct game_state*);
struct game_state init_game(struct room*, int);

void dump_bullet(struct bullet b);
void dump_unit(struct unit p);

int next_level(int lvl);

void pick_layout(struct room*);

#endif
