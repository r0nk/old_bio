#ifndef FRAME_TEXTURE_H
#define FRAME_TEXTURE_H

struct frame_texture {
	GLuint texture_id;
	GLuint rboId;
	GLuint fboId;
	int width;
	int height;
};

struct frame_texture init_frame_texture( int width, int height);
void update_frame_texture(struct frame_texture * ft);
void draw_to_frame_texture(struct frame_texture * ft);

#endif
