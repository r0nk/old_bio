#ifndef ENGINE_H
#define ENGINE_H

#include <math.h>
#include <pthread.h>
#include <signal.h>

#include "poly.h"
#include "unit.h"
#include "game_state.h"
#include "room.h"

double fps;
long double frames;
long double seconds;

#define SCREEN_MAIN 0
#define SCREEN_OPTIONS 1
#define SCREEN_CHARACTERS 2

int current_menu_screen;

int is_game_over;
int paused;
int game_running;

extern double portal_timer;
extern double max_portal_timer;
extern float planet_impact_timer;

void engine_tick();

static inline void game_exit()
{
	exit(0);
}

static inline double dir_to_rot(int dir){
	switch(dir){
		case 0: return 0;
		case 1: return 270;
		case 2: return 90;
		case 3: return 180;
	}
}

static inline double to_degrees(double r){
	return r * (180.0/M_PI);
}

static inline double to_radians(double d){
	return d * (M_PI/180.0);
}

static inline double face_angle(struct vector from, struct vector to) {
	struct vector d;
	d.x = from.x - to.x;
	d.z = from.z - to.z;

	if(d.x<0.0)
		return to_degrees(-atan(d.z/d.x)) + 90;
	else
		return to_degrees(-atan(d.z/d.x)) - 90;
}

static inline double distance(struct vector a, struct vector b)
{
	return sqrt(pow((b.x-a.x),2)+pow((b.y-a.y),2)+pow((b.z-a.z),2));
}

static inline int is_near(struct vector a, struct vector b,double r)
{
	return (distance(a,b)<r);
}

static inline struct vector rot_to_dvec(float rot)
{
	struct vector dvec;
	float d = to_radians(rot);
	dvec.x=sin(d);
	dvec.y=0;
	dvec.z=cos(d);
	return dvec;
}

inline static double correct_angle(double d)
{
	if((d)<0)
		(d)+=360;
	if((d)>360)
		(d)-=360;
	return d;
}

/*return 1 for clockwise, -1 for counterclock*/
inline static int angle_wise(double current, double target)
{
	target -= current;
	target = correct_angle(target);
	if(target>180){
		return -1;
	} else{
		return 1;
	}
}

void damage_unit(struct unit * u, double amount);
void field_set (struct game_state * gs, int x, int z,int freq,float amount);
void splash (struct game_state * gs, int x, int z,int freq);
void bullet_hit_sparks(struct game_state* gs, int j, double delta);
void explode_missile(struct game_state* gs, int i);
void heal_unit(struct unit* unit, double amount);
void add_mana(struct game_state* gs, struct unit* u, double amount);
void impact_chunks(struct game_state* gs, int j);
void emit_fire(struct game_state * gs, struct vector v);
void player_dealt_damage(struct game_state * gs, float amount);

int layout_wall_intersects_line(struct layout * l, struct vector a, struct vector b);

#endif
