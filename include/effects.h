#ifndef EFFECTS_H
#define EFFECTS_H

#include "game_state.h"

/*all the item effects */

void item_effect(struct game_state * gs, struct item * item,double delta,
		int dir);

void regen_effect(struct game_state * gs, double delta);

void field_effect(struct game_state * gs, struct unit * u, double delta, int f);

void card_effect(struct game_state * gs, int card_type);

#endif
