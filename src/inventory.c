#include "inventory.h"
#include "engine.h"
#include "input.h"

void item_release(struct game_state * gs,struct vector m,int i)
{
	struct room * room = world_map.current_room;
	if(room->has_shop){
		if(m.x>-5 && m.y>-5){
			//sell the item
			gs->game_player.coins+=5+(rand()%10);
			gs->game_player.inventory.item[i].type=0;
			gs->game_player.inventory.item[i].dragging=0;
			play_coin_sound_effect();
			return;
		}
	}

	swap_items(&gs->game_player.inventory, closest_box(m), i);
	gs->game_player.inventory.item[i].dragging=0;
}

void item_drag(struct game_state * gs, double delta)
{
	struct vector m  = pixel_to_screen(pi.mouse_x,pi.mouse_y);

	int i,t=0;
	for(i=0;i<4;i++){
		if(gs->game_player.inventory.item[i].dragging)
		{
			if(!pi.left_click){
				item_release(gs,m,i);
			}
			return;
		}
	}
	for(i=0;i<4;i++){
		t = (is_near(m,box_point[i],0.5)&& pi.left_click);
		gs->game_player.inventory.item[i].dragging = t;
		if(t)
			return;
	}
}

void player_items(struct game_state * gs, double delta)
{
	struct inventory * inven = &(gs->game_player.inventory);
	inven->item[0].active=
		pi.shift_key || pi.keys['1'] || pi.controller.y || 
		pi.controller.rb;
	inven->item[1].active=
		pi.right_click || pi.keys['2']|| pi.controller.b || 
		pi.controller.rt;
	inven->item[2].active=
		pi.left_click || pi.keys['3'] || pi.controller.x || 
		pi.controller.lt;
	inven->item[3].active=pi.keys[' '] || pi.keys['4']|| pi.controller.a ||
		pi.controller.lb;

	item_drag(gs,delta);

	int i;
	for(i=0;i<4;i++){
		if(inven->item[i].dragging)
			inven->item[2].active=0;
	}

	for(i=0;i<4;i++){
		item_effect(gs,&(inven->item[i]),delta,i);
	}
}

int closest_box(struct vector m)
{
	int i;
	double lowest = 9999999.0;
	double d;
	int low_i=0;
	for(i=0;i<4;i++){
		d = distance(m,box_point[i]);
		if(d<lowest){
			lowest=d;
			low_i=i;
		}
	}
	return low_i;
}

