#include <stdio.h>

#include "save.h"
#include "camera.h"
#include "effects.h"
#include "map.h"
#include "audio.h"
#include "input.h"
#include "engine.h"
#include "missile.h"

int spend_mana(struct game_state * gs, float mana)
{
	if(gs->game_player.flags&HAS_UNIBAR){
		gs->game_player.health-=mana;
		return 1;
	}
	if(gs->game_player.mana>mana){
		gs->game_player.mana-=mana;
		return 1;
	}else{
		play_oom_sound_effect();
		return 0;
	}
}

void regen_effect(struct game_state * gs, double amount){
	if((gs->game_player.health+amount) < (gs->game_player.max_health)){
		gs->game_player.health+=amount;
	}else{
		gs->game_player.health=gs->game_player.max_health;
	}
}

void teledice_effect(struct game_state * gs, double delta,int dir)
{
	double angle = gs->game_player.rotation_angle+dir_to_rot(dir);
	struct vector a = rot_to_dvec(angle);
	a = v_s_mul(5,a);
	struct vector newv = v_add(gs->game_player.location,a);
	if(walkable(newv.x,newv.z)){
		play_teledice_sound_effect();
		gs->game_player.location = v_add(gs->game_player.location,a);
	}
	gs->game_player.hit_timer=0.05;
}

int kite_effect(struct game_state * gs, double delta)
{
	int i;
	for(i=0;i<gs->n_npcs;i++){
		if(is_near(gs->game_player.location,gs->npc[i].location,5)){
			gs->game_player.speed=3;
			return 0;
		}
	}
	/*if we've reached this point, then the kite effect is active*/
	gs->game_player.speed=4;
	return 1;
}

void collect_all_coins(struct game_state * gs)
{
	int i;
	for(i=0;i<gs->n_npcs;i++){
		if(gs->npc[i].type == UNIT_TYPE_ITEM){
			remove_npc(gs,i);
			gs->game_player.coins++;
		}
	}
}

void plasma_effect(struct game_state * gs, double delta,int dir)
{
	double angle = gs->game_player.rotation_angle+dir_to_rot(dir);
	struct vector a = v_add(gs->game_player.location,rot_to_dvec(angle));
	int i;
	int n=0;
	for(i=0;i<gs->n_npcs;i++){
		if(gs->npc[i].type==UNIT_TYPE_ITEM)
			return;
		if(is_near(a,gs->npc[i].location,8) &&
				!wall_intersects_line(a,gs->npc[i].location)){
			n++;
		}
	}
	for(i=0;i<gs->n_npcs;i++){
		if(is_near(a,gs->npc[i].location,8) &&
				!wall_intersects_line(a,gs->npc[i].location)){
			player_dealt_damage(gs,gs->game_player.damage*0.8*n*delta);
			damage_unit(&gs->npc[i],
					gs->game_player.damage*0.8*n*delta);
		}
	}
}

void fire_blaster(struct game_state * gs, double delta,int dir)
{
	struct vector v = rot_to_dvec(gs->game_player.rotation_angle+
			(dir_to_rot(dir)));
	fire_bullet(gs,&gs->game_player,v);
}

void fire_side_blaster(struct game_state * gs, double delta,int dir)
{
	struct vector v;
	struct vector a = {0};
	double angle;
	if(dir==1 ||dir==2){
		angle = gs->game_player.rotation_angle+dir_to_rot(dir);
		a = rot_to_dvec(angle);
		a.x/=2;
		a.z/=2;
		a = v_add(a,gs->game_player.location);
		angle = gs->game_player.rotation_angle + ((rand()%10)-5);
	}else{
		a = v_add(a,gs->game_player.location);
		angle = gs->game_player.rotation_angle+dir_to_rot(dir);
	}

	v = rot_to_dvec(angle);
	fire_bullet_at(gs,a,&gs->game_player,v);
}

struct unit * nearest_npc(struct game_state * gs, struct vector v){
	double dlow=99999;
	int ilow=0;
	double dist;
	int i;
	for(i=0;i<gs->n_npcs;i++){
		if(gs->npc[i].type==UNIT_TYPE_ITEM)
			continue;
		dist = distance(gs->npc[i].location,v);
		if(dist<dlow){
			dlow=dist;
			ilow = i;
		}
	}
	return &(gs->npc[ilow]);
}

void fire_missile(struct game_state * gs, double delta,int dir)
{
	struct vector v;
	struct missile m = {0};
	struct vector a = {0};
	double angle;

	if(dir==1 ||dir==2){
		angle = gs->game_player.rotation_angle+dir_to_rot(dir);
		a = rot_to_dvec(angle);
		a.x/=2;
		a.z/=2;
		a = v_add(a,gs->game_player.location);
		angle = gs->game_player.rotation_angle;
	}else{
		a = v_add(a,gs->game_player.location);
		angle = gs->game_player.rotation_angle+dir_to_rot(dir);
	}

	if(gs->n_npcs<=0)
		return;
	m.target = nearest_npc(gs,screen_to_world(gs,pi.mouse_x,pi.mouse_y));
	m.direction = angle;
	m.location = a;
	m.primer=0;

	add_missile(gs,m);
	play_rocket_sound_effect();
}

void fire_laser(struct game_state * gs, double delta,int dir)
{
	struct vector v;
	struct vector b,a = {0};
	double angle;
	int i;
	if(dir==1 ||dir==2){
		angle = gs->game_player.rotation_angle+dir_to_rot(dir);
		a = rot_to_dvec(angle);
		a.x/=2;
		a.z/=2;
		a = v_add(a,gs->game_player.location);
		angle = gs->game_player.rotation_angle;
	}else{
		a = v_add(a,gs->game_player.location);
		angle = gs->game_player.rotation_angle+dir_to_rot(dir);
	}
	b = rot_to_dvec(angle);
	b.x*=30;
	b.z*=30;
	b.x+=a.x;
	b.z+=a.z;
	for(i=0;i<gs->n_npcs;i++){
		if(unit_intersects_line(&gs->npc[i],a,b)){
			if(!wall_intersects_line(gs->npc[i].location,
						gs->game_player.location)){
				player_dealt_damage(gs,gs->game_player.damage*3);
				damage_unit(&gs->npc[i],gs->game_player.damage*3);
				gs->npc[i].hit_timer=0.2;
			}
		}
	}
	last_camera.x+=((rand()%10)-5)/10.0;
	last_camera.z+=((rand()%10)-5)/10.0;
}

/*some npcs are immune to hooks*/
int no_hook_npc(int type)
{
	switch(type)
	{
		case UNIT_TYPE_BOSS:
		case UNIT_TYPE_YO:
		case UNIT_TYPE_INTERCOM:
			return 1;
			break;
		default:
			return 0;
			break;
	}
}

void hook_hittest(struct game_state * gs, struct item * item, double delta,
		int dir)
{
	int i;

	for(i=0;i<gs->n_npcs;i++){
		if(no_hook_npc(gs->npc[i].type))
			continue;
		if(is_near(gs->npc[i].location,item->location,1.0)){
			item->hooked_npc=i;
			item->state=ITEM_STATE_HOOK_CLOSED;
			item->duration=0;
			play_reel_sound_effect();
			break;
		}
	}
}

void open_hook(struct game_state * gs, struct item * item, double delta)
{
	if(item->state==ITEM_STATE_HOOK_CLOSED){
		gs->npc[item->hooked_npc].location.x-=
			item->direction.x;
		gs->npc[item->hooked_npc].location.x-=
			item->direction.z;
	}
	item->state=ITEM_STATE_HOOK_OPEN;
}

void update_hook_in_use(struct game_state * gs,struct item * item,double delta, int dir)
{
	float dis=0;
	float hook_speed=1;
	item->cooldown-=delta;
	if(!walkable((int)item->location.x,(int)item->location.z)) {
		item->duration=0;
		open_hook(gs,item,delta);
	}
	if((item->duration)>0){
		item->duration-=delta;
		hook_hittest(gs,item,delta,dir);
	}else{
		item->rotation_angle=180+face_angle(item->location,
				gs->game_player.location);
		item->direction = rot_to_dvec(item->rotation_angle+180);
		item->direction = v_s_mul(0.5,item->direction);
		dis=distance(item->location,gs->game_player.location);
		item->cooldown*=dis;
		if(dis<0.4)
			item->cooldown=0;
		if(item->state==ITEM_STATE_HOOK_CLOSED){
			gs->npc[item->hooked_npc].location =
				item->location;
			if(gs->npc[item->hooked_npc].speed>0.5)
				gs->npc[item->hooked_npc].speed-=delta*5;
		}
	}
	hook_speed=delta*30.0*(1+(dis/8));
	item->location.x+=item->direction.x*hook_speed;
	item->location.y+=item->direction.y*hook_speed;
	item->location.z+=item->direction.z*hook_speed;
}

void update_hook(struct game_state * gs, struct item * item, double delta,
		int dir)
{
	if(item->cooldown>0){
		update_hook_in_use(gs,item,delta,dir);
	}else{
		item->location = gs->game_player.location;
		item->state =ITEM_STATE_HOOK_OPEN;
		if(item->active && spend_mana(gs,30)){
			item->rotation_angle =
				gs->game_player.rotation_angle+dir_to_rot(dir);
			item->direction = rot_to_dvec(item->rotation_angle);
			item->cooldown = item->max_cooldown;
			item->duration = 1;
		}
	}
}

void chainsaw_sparks(struct vector a)
{
	int i,k=0;
	for(i=rand()%MAX_SPARKS;i<MAX_SPARKS;i++){
		k++;
		if(k>10)
			return;
		spark[i].alive=1;
		spark[i].frame=0;
		spark[i].location=a;
		spark[i].location.y+=0.8;
		spark[i].direction=(struct vector) {0,0,0};
		spark[i].direction.x+=spark_dir()/10.0;
		spark[i].direction.y+=spark_dir()/10.0;
		spark[i].direction.z+=spark_dir()/10.0;
	}

}

void chainsaw_hittest(struct game_state * gs, struct item * item, double delta,
		int dir){
	int i;
	struct vector a,b;
	a = gs->game_player.location;

	b = rot_to_dvec(gs->game_player.rotation_angle + dir_to_rot(dir));
	b.x*=2;
	b.z*=2;
	b = v_add(a,b);

	for(i=0;i<gs->n_npcs;i++){
		if(unit_intersects_line(&gs->npc[i],a,b)){
			gs->npc[i].hit_timer=0.1;
			player_dealt_damage(gs,40*delta);
			damage_unit(&gs->npc[i],40*delta);
			last_camera.x+=((rand()%2)-1)/2.0;
			last_camera.z+=((rand()%2)-1)/2.0;
			chainsaw_sparks(gs->npc[i].location);
		}
	}
	if(wall_intersects_line(a,b))
		chainsaw_sparks(b);
}

void update_chainsaw(struct game_state * gs, struct item * item, double delta,
		int dir)
{
	if(spend_mana(gs,delta*40)){
		item->state = ITEM_STATE_ON;
		chainsaw_hittest(gs,item,delta,dir);
		play_chainsaw_sound_effect();
	}else{
		stop_chainsaw_sound_effect();
		item->state = ITEM_STATE_OFF;
		item->cooldown=item->max_cooldown;
	}
}

void spew_fire(struct game_state * gs, 
		struct vector location, struct vector direction)
{
	int rando = rand()%MAX_FIRES;
	struct vector loc = v_add(location,v_s_mul(1.4,direction));
	gs->fire[rando].alive=1;
	gs->fire[rando].frame=0;
	gs->fire[rando].location=loc;
	gs->fire[rando].direction=direction;
	gs->fire[rando].type=(rand()%7)+1;
	gs->fire[rando].size=(struct vector){1,1,1};
}

void update_flamethrower(struct game_state * gs, struct item * item, 
		double delta, int dir)
{
	double angle;
	struct vector direction;
	struct vector loc;
	if(item->active){
		angle = gs->game_player.rotation_angle+dir_to_rot(dir);
		direction = rot_to_dvec(angle);
		loc = gs->game_player.location;
		if(spend_mana(gs,delta*30)){
			spew_fire(gs,loc,direction);
		}else{
			item->cooldown=item->max_cooldown;
		}
	}
}

void update_shield(struct game_state * gs,double delta, int dir)
{
	/*figure out the shield line*/
	double gsd = gs->game_player.rotation_angle+(dir_to_rot(dir));
	double r1 = ((45)+gsd);
	double r2 = ((-45)+gsd);
	struct vector s1 = rot_to_dvec(r1);
	struct vector s2 = rot_to_dvec(r2);
	s1.x*=2;s1.z*=2;
	s2.x*=2;s2.z*=2;
	gs->game_player.speed = gs->game_player.max_speed*0.5;

	s1 = v_add(gs->game_player.location,s1);
	s2 = v_add(gs->game_player.location,s2);

	struct vector b1,b2;
	int i;
	for(i=0;i<gs->n_bullets;i++){
		b1 = gs->bullet[i].location;
		b2 = v_add(b1,gs->bullet[i].direction);
		if(line_intersects_line(s1,s2,b1,b2)){
			if(spend_mana(gs,gs->bullet[i].damage)){
				gs->bullet[i].direction.x*=-1;
				gs->bullet[i].direction.z*=-1;
				gs->bullet[i].shooter= &gs->game_player;
				gs->bullet[i].damage/=2;
				gs->bullet[i].duration-=1;
			}

		}
	}
}

void item_effect(struct game_state * gs, struct item * item, double delta,
		int dir)
{
	int should_seven=0;
	if(item->type==ITEM_HOOK){
		update_hook(gs,item,delta,dir);
		return;
	}
	if(item->cooldown>0){
		item->cooldown-=delta*gs->game_player.cooldown;
		return;
	}
do_effect:
	switch(item->type){
		case 0:
			break;
		case ITEM_TELEDICE:
			if(item->active){
				if(spend_mana(gs,50)){
					should_seven=1;
					teledice_effect(gs,delta,dir);
					item->cooldown=item->max_cooldown;
				}
			}
			break;
		case ITEM_SHIELD:
			if(item->active){
				if(spend_mana(gs,20*delta)){
					update_shield(gs,delta,dir);
				}else{
					item->cooldown=item->max_cooldown;
				}
			}
			break;
		case ITEM_DASH:
			if(item->active &&
					(gs->game_player.dash_timer)<=0 &&
					item->state==ITEM_STATE_OFF){
				if((item->duration>0) || spend_mana(gs,10)){
					item->duration=0.2;
					gs->game_player.dash_timer=0.15;
					gs->game_player.dash_dir=dir;
					play_dash_sound_effect();
					item->state=ITEM_STATE_ON;
				}
			}
			if(gs->game_player.dash_timer<=0 && !item->active)
				item->state=ITEM_STATE_OFF;
			if(((item->duration)>0) && 
					(gs->game_player.dash_timer)<=0){
				item->duration-=delta;
				if(!item->active){
					item->state=ITEM_STATE_OFF;
				}
				if(item->duration<0){
					item->cooldown=item->max_cooldown;
					item->duration=0;
					item->state=ITEM_STATE_OFF;
				}
			}
			break;
		case ITEM_CAPACITOR:
			if(item->active){
				gs->game_player.mana=
						gs->game_player.max_mana;
				gs->game_player.health=
						gs->game_player.max_health;
				item->cooldown = item->max_cooldown;
			}
			break;
		case ITEM_PLASMA:
			if(item->active){
				if(spend_mana(gs,delta*40)){
					should_seven=1;
					play_plasma_sound_effect();
					plasma_effect(gs,delta,dir);
				}else{
					stop_plasma_sound_effect();
					item->cooldown = item->max_cooldown;
				}
			}else{
				stop_plasma_sound_effect();
			}
			break;
		case ITEM_SIDE_BLASTER:
			item->state = (dir==1||dir==2);
			if(item->active){
				if(spend_mana(gs,5)){
					should_seven=1;
					fire_side_blaster(gs,delta,dir);
					item->cooldown = item->max_cooldown;
				}
			}
			break;
		case ITEM_LASER:
			item->state = (dir==1||dir==2);
			if(item->active){
				if(spend_mana(gs,delta*100*item->duration)){
					if(item->duration==0)
						play_big_laser_sound_effect();
					item->duration+=delta;
					if(item->duration>1.0){
						fire_laser(gs,delta,dir);
						item->duration=0.0;
						item->cooldown=item->max_cooldown;
					}
				}else{
					item->duration=0;
					item->cooldown=item->max_cooldown;
					stop_big_laser_sound_effect();
				}
			}else{
				stop_big_laser_sound_effect();
				if(item->duration>0.0){
					item->duration=0;
					item->cooldown=item->max_cooldown;
				}
			}
			break;
		case ITEM_VULCAN:
			if(item->active && spend_mana(gs,delta*2)){
				should_seven=1;
				item->rotation_delta+=delta*5;
			}else{
				item->rotation_delta/=1+delta;
			}

			item->rotation_angle+=item->rotation_delta;
			if(item->rotation_angle>360){
				if(spend_mana(gs,5))
					fire_blaster(gs,delta,dir);
				else
					item->rotation_delta=0;
				item->rotation_angle=0;
			}
			break;
		case ITEM_MISSILE:
			item->state = (dir==1||dir==2);
			if(item->active){
				if(spend_mana(gs,40)){
					should_seven=1;
					fire_missile(gs,delta,dir);
					item->cooldown = item->max_cooldown;
				}
			}
			break;
		case ITEM_HOOK:
			update_hook(gs,item,delta,dir);
			break;
		case ITEM_CHAINSAW:
			if(item->active){
				update_chainsaw(gs,item,delta,dir);
			}else{
				stop_chainsaw_sound_effect();
				item->state = ITEM_STATE_OFF;
			}
			break;
		case ITEM_FLAMETHROWER:
			update_flamethrower(gs,item,delta,dir);
			break;
		default:
			printf("ERR:unrecognized item effect: %i\n",item->type);
			break;
	}
	if(gs->game_player.lucky_sevens && should_seven){
		if((rand()%7) < (gs->game_player.lucky_sevens)){
			play_sevens_sound_effect();
			goto do_effect;
		}
	}
}

void speed_effect( struct game_state * gs, struct unit * u, double amount)
{
	u->speed+=amount*0.2;
	u->cooldown+=amount*0.2;
	add_mana(gs,u,amount);
}

void damage_effect( struct game_state * gs, struct unit * u, double amount)
{
	if(u->rouge>=1 && amount<0)
	{
		u->damage-=amount;
		return;
	}
	if((u->health+amount)>u->max_health){
		u->health = u->max_health;
		return;
	}
	if(amount<0){
		damage_unit(u,-amount*0.6);
		return;
	}
	u->health+=amount;
}

void field_effect(struct game_state * gs, struct unit * u, double delta, int f)
{
	float yoff = 0.07;
	double amount;
	struct vector l = u->location;
	double m=100;
	switch(f){
		case 0:
			amount = m*gs->bfield.cell[(int)l.x][(int)l.z].y*delta;
			if(amount<yoff){
				return;
			}
			speed_effect(gs,u,amount);
			u->color.x-=amount/50;
			u->color.y-=amount/50;
			break;
		case 1:
			amount = m*gs->gfield.cell[(int)l.x][(int)l.z].y*delta;
			if(amount<yoff){
				return;
			}
			damage_effect(gs,u,amount);
			u->color.x-=amount/50;
			u->color.z-=amount/50;
			break;
		case 2:
			amount = m*gs->rfield.cell[(int)l.x][(int)l.z].y*delta;
			if(amount<yoff*2){
				return;
			}
			damage_effect(gs,u,-amount);
			u->color.y-=amount/50;
			u->color.z-=amount/50;
			break;
	}
}

void card_effect(struct game_state * gs, int card_type)
{
	switch(card_type)
	{
		case STATS_CARD:
			gs->game_player.max_health+=5;
			gs->game_player.health = gs->game_player.max_health;
			gs->game_player.max_speed+=0.5;
			gs->game_player.speed= gs->game_player.max_speed;
			gs->game_player.max_cooldown*=1.1;
			gs->game_player.max_mana+=5;
			gs->game_player.delta_mana+=5;
			break;
		case LIFESTEAL_CARD:
			gs->game_player.flags|=HAS_LIFESTEAL;
			break;
		case COINS_CARD:
			gs->game_player.coins*=2;
			break;
		case POISON_CARD:
			gs->game_player.flags|=HAS_VAIL;
			break;
		case BOUNCE_CARD:
			gs->game_player.flags|=HAS_BOUNCE;
			gs->game_player.bullet_duration+=2;
			break;
		case UNIBAR_CARD:
			gs->game_player.flags|=HAS_UNIBAR;
			gs->game_player.max_health+=gs->game_player.max_mana;
			gs->game_player.max_health/=2;
			gs->game_player.health=gs->game_player.max_health;
			global_save_state.unibar_picks++;
			break;
		case ROUGE_CARD:
			gs->game_player.rouge+=1;
			if(wave_destruction>0.4)
				wave_destruction-=0.30;
			if(wave_destruction<=0.1)
				wave_destruction=0.1;
			break;
		case LEARN_CARD:
			gs->game_player.learn_rate*=1.5;
			break;
		case WRATH_CARD:
			gs->game_player.wrath++;
			break;
		case SEVENS_CARD:
			gs->game_player.lucky_sevens++;
			break;
		case OVERCLOCK_CARD:
			gs->game_player.overclock++;
			break;
		default:
			printf("ERR unrecognized card effect\n");
	}
}
