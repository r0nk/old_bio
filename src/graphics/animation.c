#include <stdio.h>

#include "animation.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

float dot_quaternions(struct aiQuaternion *a, struct aiQuaternion *b)
{
	return (a->x*b->x + a->y*b->y + a->z*b->z + a->w*b->w);
}

void normalize_quaternion(struct aiQuaternion *q)
{
	float d = sqrt(dot_quaternions(q, q));
	if (d >= 0.00001) {
		d = 1 / d;
		q->x *= d;
		q->y *= d;
		q->z *= d;
		q->w *= d;
	} else {
		q->x = q->y = q->z = 0;
		q->w = 1;
	}
}

void mix_quaternion(struct aiQuaternion *q, struct aiQuaternion *a, struct aiQuaternion *b, float t)
{
	struct aiQuaternion tmp;
	if (dot_quaternions(a, b) < 0) {
		tmp.x = -a->x; tmp.y = -a->y; tmp.z = -a->z; tmp.w = -a->w;
		a = &tmp;
	}
	q->x = a->x + (t * (b->x - a->x));
	q->y = a->y + (t * (b->y - a->y));
	q->z = a->z + (t * (b->z - a->z));
	q->w = a->w + (t * (b->w - a->w));
	normalize_quaternion(q);
}

void mix_vector(struct aiVector3D *p, struct aiVector3D *a, struct aiVector3D *b, float t)
{
	if(t>1.0){
		return;
	}

	p->x = a->x + (t * (b->x - a->x));
	p->y = a->y + (t * (b->y - a->y));
	p->z = a->z + (t * (b->z - a->z));
}

void trans_mat(struct aiVector3D * t,struct aiVector3D * s,
		struct aiQuaternion *q, float m[16])
{
	struct aiMatrix4x4 p;
	// quat to rotation matrix
	p.a1 = 1 - 2 * (q->y * q->y + q->z * q->z);
	p.a2 = 2 * (q->x * q->y - q->z * q->w);
	p.a3 = 2 * (q->x * q->z + q->y * q->w);
	p.b1 = 2 * (q->x * q->y + q->z * q->w);
	p.b2 = 1 - 2 * (q->x * q->x + q->z * q->z);
	p.b3 = 2 * (q->y * q->z - q->x * q->w);
	p.c1 = 2 * (q->x * q->z - q->y * q->w);
	p.c2 = 2 * (q->y * q->z + q->x * q->w);
	p.c3 = 1 - 2 * (q->x * q->x + q->y * q->y);

	// scale matrix
	p.a1 *= s->x; p.a2 *= s->x; p.a3 *= s->x;
	p.b1 *= s->y; p.b2 *= s->y; p.b3 *= s->y;
	p.c1 *= s->z; p.c2 *= s->z; p.c3 *= s->z;

	// set translation
	p.a4 = t->x; p.b4 = t->y; p.c4 = t->z;
	p.d1 = 0; p.d2 = 0; p.d3 = 0; p.d4=1;

	//transpose to opengl format
	m[0] = p.a1; m[4] = p.a2; m[8] = p.a3; m[12] = p.a4;
	m[1] = p.b1; m[5] = p.b2; m[9] = p.b3; m[13] = p.b4;
	m[2] = p.c1; m[6] = p.c2; m[10] = p.c3; m[14] = p.c4;
	m[3] = p.d1; m[7] = p.d2; m[11] = p.d3; m[15] = p.d4;
}

void get_mat(struct aiNodeAnim *chan,double dpl, float mat[16])
{
	struct aiVector3D v,s;
	struct aiQuaternion q;
	int i,j;/* i--dpl--j */
	if(chan->mNumPositionKeys==1){
		trans_mat( &chan->mPositionKeys[0].mValue,
				&chan->mScalingKeys[0].mValue,
				&chan->mRotationKeys[0].mValue,mat);
		return;
	}
	for(i=0;i<(chan->mNumPositionKeys);i++){
		if(dpl<chan->mPositionKeys[i].mTime)
		{
			j=i;
			i--;
			dpl = dpl-(chan->mPositionKeys[i].mTime);
			dpl/=(chan->mPositionKeys[j].mTime -
					chan->mPositionKeys[i].mTime);
			break;
		}
	}
	mix_vector(&v,&chan->mPositionKeys[i].mValue,
			&chan->mPositionKeys[j].mValue,dpl);
	mix_vector(&s,&chan->mScalingKeys[i].mValue,
			&chan->mScalingKeys[j].mValue,dpl);
	mix_quaternion(&q,&chan->mRotationKeys[i].mValue,
			&chan->mRotationKeys[j].mValue,dpl);
	trans_mat(&v,&s,&q,mat);
}

