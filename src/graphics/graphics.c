#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <SOIL.h>

#include "sprinkle.h"
#include "save.h"
#include "unit.h"
#include "draw.h"
#include "audio.h"
#include "frame_texture.h"
#include "screen.h"
#include "camera.h"
#include "futil.h"
#include "model_reader.h"
#include "input.h"
#include "field.h"
#include "pathfinding.h"
#include "map.h"
#include "game_state.h"
#include "graphics.h"
#include "engine.h"
#include "callbacks.h"
#include "game.h"
#include "inventory.h"
#include "poly.h"
#include "model.h"
#include "room.h"
#include "fonts.h"
#include "ui.h"
#include "particle.h"
#include "neighborhood.h"

SDL_Window * window;
SDL_GLContext glcontext;
int window_width,window_height;
float frame_x,frame_y;

float frame_ratio;

int post_processing;

struct frame_texture base_ft;
struct frame_texture post_ft;
struct frame_texture hud_ft;

struct particle explosion[MAX_EXPLOSIONS];
struct particle trail[MAX_TRAILS];
struct particle spark[MAX_SPARKS];
struct particle porticle[MAX_PORTICLES];
struct particle muzzle[MAX_MUZZLE];
struct particle damage_number[MAX_DAMAGE_NUMBERS];

struct model missile_model;

struct model p_model;
struct model b_model;
struct model ant_model;
struct model r_ant_model;
struct model g_ant_model;
struct model b_ant_model;
struct model mole_boss_model;
struct model yo_boss_model;
struct model d_model;
struct model scav_model;
struct model shotty_ranger_model;
struct model ranger_model;
struct model bullet_model;
struct model block_model;
struct model fh_model;
struct model shp_model;
struct model coin_model;
struct model door_model;
struct model stand_model;
struct model spider_model;
struct model tank_bottom;
struct model tank_top;
struct model sentry_stand;
struct model sentry_top;
struct model spinner_model;
struct model chasis_model;
struct model spade_chasis_model;
struct model crt_head_model;
struct model shieldman_running_model;
struct model shieldman_attacking_model;
struct model intercom_model;

struct model ranger_head_model;

struct model chassy_blaster_model;
struct model chassy_shield_model;
struct model chassy_tele_model;
struct model chassy_capacitor_model;
struct model chassy_dash_model;
struct model chassy_plasma_model;
struct model chassy_laser_model;
struct model chassy_side_laser_model;
struct model chassy_vulcan_model;
struct model chassy_missile_model;
struct model chassy_side_missile_model;
struct model chassy_side_blaster_model;
struct model chassy_chainsaw_off_model;
struct model chassy_chainsaw_on_model;
struct model chassy_flamethrower_model;

struct model hook_open_model;
struct model hook_closed_model;

struct model player_body_model;

struct model left_leg_model;
struct model right_leg_model;

struct model player_leg_still;

struct model explosion_ball_model;
struct model blank_plane_model;

struct model template_chunk;

struct model sprink_screen_model;
struct model sprink_valve_model;
struct model sprink_leaf_model;
struct model sprink_gear_model;
struct model sprink_example_model;
struct model sprink_storage_tank_model;
struct model sprink_straight_pipe_model;

void resize_fbo(int w, int h){
	glDeleteRenderbuffers(1, &base_ft.rboId);
	glDeleteRenderbuffers(1, &post_ft.rboId);
	glDeleteRenderbuffers(1, &hud_ft.rboId);
	glDeleteFramebuffers(1, &base_ft.fboId);
	glDeleteFramebuffers(1, &post_ft.fboId);
	glDeleteFramebuffers(1, &hud_ft.fboId);
	base_ft = init_frame_texture(w, h);
	post_ft = init_frame_texture(w, h);
	hud_ft = init_frame_texture(w, h);
}

void init_models()
{
	missile_model = get_model("models/missile.dae");
	scav_model = get_model("models/scavenger.dae");
	ranger_model = get_model("models/ranger.dae");
	shotty_ranger_model = get_model("models/shotty.dae");
	fh_model = get_model("models/flag_holder.dae");
	shp_model = get_model("models/keeps.dae");
	coin_model = get_model("models/gold_coin.dae");
	door_model = get_model("models/portal.dae");
	spider_model = get_model("models/spider.dae");
	stand_model = get_model("models/pedestal.dae");
	b_model = get_model("models/nest.dae");
	tank_bottom = get_model("models/tank_base.dae");
	tank_top = get_model("models/tank_top.dae");
	sentry_stand = get_model("models/sentry_stand.dae");
	sentry_top = get_model("models/sentry_top.dae");
	spinner_model = get_model("models/spinner.dae");
	chasis_model = get_model("models/chasis.dae");
	spade_chasis_model = get_model("models/spade_chasis.dae");
	shieldman_running_model = get_model("models/shieldman_running.dae");
	shieldman_attacking_model = get_model("models/shieldman_attacking.dae");

	ranger_head_model = get_model("models/ranger_head.dae");

	chassy_blaster_model = get_model("models/blaster.dae");
	chassy_shield_model = get_model("models/shield_component.dae");
	chassy_capacitor_model = get_model("models/capacitor_component.dae");
	chassy_dash_model = get_model("models/dash_component_idle.dae");
	chassy_plasma_model = get_model("models/plasma_component.dae");
	chassy_tele_model = get_model("models/teleporter_component.dae");
	chassy_laser_model = get_model("models/laser_component.dae");
	chassy_side_laser_model = get_model("models/side_laser_component.dae");
	chassy_vulcan_model = get_model("models/vulcan_component.dae");
	chassy_missile_model = get_model("models/missile_component.dae");
	chassy_side_missile_model = get_model("models/side_missile_component.dae");
	chassy_side_blaster_model =
		get_model("models/side_blaster_component.dae");
	chassy_flamethrower_model = 
		get_model("models/flamethrower_component.dae");

	hook_open_model = get_model("models/hook_component_open.dae");
	hook_closed_model = get_model("models/hook_component_closed.dae");

	chassy_chainsaw_on_model =
		get_model("models/chainsaw_on_component.dae");
	chassy_chainsaw_off_model =
		get_model("models/chainsaw_off_component.dae");

	player_body_model = get_model("models/player_body.dae");
	left_leg_model = get_model("models/player_leg.dae");
	right_leg_model = get_model("models/player_leg.dae");
	right_leg_model.cur_time+=0.5;
	player_leg_still = get_model("models/player_leg_still.dae");
	mole_boss_model = get_model("models/mole.dae");
	yo_boss_model = get_model("models/yo.dae");

	explosion_ball_model = get_model("models/explosion_ball.dae");
	blank_plane_model = get_model("models/blank_plane.dae");

	r_ant_model = get_model("models/red_antenna.dae");
	g_ant_model = get_model("models/green_antenna.dae");
	b_ant_model = get_model("models/blue_antenna.dae");
	template_chunk = get_model("models/template_chunk.dae");
	crt_head_model = get_model("models/crt_monitor.dae");

	sprink_valve_model = get_model("models/valve.dae");
	sprink_leaf_model = get_model("models/leaf.dae");
	sprink_gear_model = get_model("models/gear.dae");
	sprink_screen_model = get_model("models/sprinkle_screen.dae");
	sprink_example_model = get_model("models/example_sprinkle.dae");
	sprink_storage_tank_model = get_model("models/storage_tank.dae");
	sprink_straight_pipe_model = get_model("models/pipe_straight.dae");

	intercom_model = get_model("models/intercom.dae");
}


void init_sprinkles()
{
	struct sprinkle sprinkle;

	sprinkle.type=SPRINK_EXAMPLE;
	sprinkle.size=(struct vector){1,1,1};
	sprinkle.level=0;
	add_sprinkle_to_catalog(sprinkle);
}

void init_controller()
{
	SDL_Joystick * joy = NULL;
	if(SDL_NumJoysticks()>0){
		printf("Joystick Detected!\n");
		joy = SDL_JoystickOpen(0);
		if(joy){
			printf("Opened joystick\n");
		}else{
			printf("Couldn't open joystick.\n");
		}

	}
}

int init_window_lib()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS | SDL_INIT_JOYSTICK) != 0) {
		fprintf(stderr,"\nUnable to initialize SDL:  %s\n",
				SDL_GetError());
		return 1;
	}

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	window = SDL_CreateWindow("Back Into Orbit",
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			1024, 768,
			SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
	if(!window)
		printf("!window\n");
	SDL_GLContext glcontext = SDL_GL_CreateContext(window);
	if(!glcontext)
		printf("!glcontext\n");
	glewExperimental = GL_TRUE;
	if(glewInit() != GLEW_OK)
		printf("glew failed\n");

	SDL_GL_GetDrawableSize(window,&window_width,&window_height);

	init_controller();

	return 0;
}

GLuint shader_program;
GLuint lighting_shader_program;
GLuint blur_shader_program;
GLuint tv_shader_program;

GLuint pine_texture;
GLuint font_texture;
GLuint whitey_texture;
GLuint black_texture;
GLuint wall_texture;
GLuint floor_texture;
GLuint player_texture;
GLuint neon_texture;
GLuint portal_texture;
GLuint item_texture;
GLuint perk_texture;
GLuint space_texture;;
GLuint roof_texture;;
GLuint bullet_hole_texture;;
GLuint splat_texture;
GLuint sprink_texture;
GLuint scorch_texture;
GLuint rainbow_texture;

GLuint screen_texture;
GLuint rboId;
GLuint fboId;

/*init the tv screen on the players head*/
void init_screen()
{
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
	// create a texture object
	glGenTextures(1, &screen_texture);
	glBindTexture(GL_TEXTURE_2D, screen_texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	/*automatic mip-map*/
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, SCREEN_WIDTH, SCREEN_HEIGHT,
			0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// create a renderbuffer object to store depth info
	glGenRenderbuffers(1, &rboId);
	glBindRenderbuffer(GL_RENDERBUFFER, rboId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
			SCREEN_WIDTH, SCREEN_HEIGHT);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glGenFramebuffers(1, &fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fboId);

	// attach the texture to FBO color attachment point
	glFramebufferTexture2D(GL_FRAMEBUFFER, // 1. fbo target: GL_FRAMEBUFFER
			GL_COLOR_ATTACHMENT0,  // 2. attachment point
			GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
			screen_texture,             // 4. tex ID
			0);                    // 5. mipmap level: 0(base)

	// attach the renderbuffer to depth attachment point
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,//1. fbo target: GL_FRAMEBUFFER
			GL_DEPTH_ATTACHMENT, // 2. attachment point
			GL_RENDERBUFFER,     // 3. rbo target: GL_RENDERBUFFER
			rboId);              // 4. rbo ID

	// check FBO status
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE){
		printf("ERR: frame buffer incomplete\n");
		exit(-8);
	}

	// switch back to window-system-provided framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void init_textures()
{
	base_ft = init_frame_texture(window_width,window_height);
	post_ft = init_frame_texture(window_width,window_height);
	hud_ft = init_frame_texture(window_width,window_height);
	init_screen();
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	rainbow_texture =
		SOIL_load_OGL_texture("textures/rainbow.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	sprink_texture =
		SOIL_load_OGL_texture("textures/sprink.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	splat_texture =
		SOIL_load_OGL_texture("textures/splat.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	scorch_texture =
		SOIL_load_OGL_texture("textures/scorch.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	bullet_hole_texture =
		SOIL_load_OGL_texture("textures/bullet_holes.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	roof_texture =
		SOIL_load_OGL_texture("textures/roof.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	space_texture =
		SOIL_load_OGL_texture("textures/space.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	perk_texture =
		SOIL_load_OGL_texture("textures/perks.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	item_texture =
		SOIL_load_OGL_texture("textures/items.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	portal_texture =
		SOIL_load_OGL_texture("textures/portal_texture.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	font_texture =
		SOIL_load_OGL_texture("textures/font.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	player_texture =
		SOIL_load_OGL_texture("textures/tv_texture.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_TEXTURE_REPEATS |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	neon_texture =
		SOIL_load_OGL_texture("textures/neon_tv_texture.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_TEXTURE_REPEATS |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	floor_texture =
		SOIL_load_OGL_texture("textures/floor.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_TEXTURE_REPEATS |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	wall_texture =
		SOIL_load_OGL_texture("textures/wall.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_TEXTURE_REPEATS |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	pine_texture =
		SOIL_load_OGL_texture("textures/pine.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);

	whitey_texture =
		SOIL_load_OGL_texture("textures/whitey.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_TEXTURE_REPEATS |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);
	black_texture =
		SOIL_load_OGL_texture("textures/black.png",
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				(SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y |
				 SOIL_FLAG_NTSC_SAFE_RGB |
				 SOIL_FLAG_TEXTURE_REPEATS |
				 SOIL_FLAG_COMPRESS_TO_DXT)
				);

	if(!sprink_texture )
	{
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
	}

	glBindTexture(GL_TEXTURE_2D,pine_texture);
}

void draw_text(double x, double y, const char * text, struct vector color)
{
	glColor3f(0.9,1.0,1.0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, font_texture);
	glEnable(GL_BLEND);
	int i;
	int l = strlen(text);
	char c;
	for(i=0;i<l;i++)
	{
		c=text[i];
		draw_letter((struct vector){x,y,0},c);
		x-=FONT_WIDTH;
	}
	glDisable(GL_BLEND);
	glColor3f(1,1,1);
}

void draw_n_text(double x, double y, int n, char * text, struct vector color)
{
	glColor3f(color.x,color.y,color.z);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, font_texture);
	glEnable(GL_BLEND);
	int i;
	int l = n;
	char c;
	for(i=0;i<l;i++)
	{
		c=text[i];
		draw_letter((struct vector){x,y,0},c);
		x-=FONT_WIDTH;
	}
	glDisable(GL_BLEND);
	draw_carrot((struct vector){x,y,0});
	glColor3f(1,1,1);
}

void init_shaders()
{
	int i=0;
	char log[580];

	char * vertex_shader = read_file("shaders/vertex_shader.glsl");
	char * fragment_shader = read_file("shaders/fragment_shader.glsl");

	char * tv_fragment_shader = read_file("shaders/tv_fragment_shader.glsl");
	char * blur_fragment_shader =
		read_file("shaders/blur_fragment_shader.glsl");
	char * lighting_fragment_shader = 
		read_file("shaders/lighting_fragment_shader.glsl");

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs,1,&vertex_shader,NULL);
	glCompileShader(vs);

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs,1,&fragment_shader,NULL);
	glCompileShader(fs);

	GLuint bs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(bs,1,&blur_fragment_shader,NULL);
	glCompileShader(bs);

	GLuint lfs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(lfs,1,&lighting_fragment_shader,NULL);
	glCompileShader(lfs);

	GLuint tvfs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(tvfs,1,&tv_fragment_shader,NULL);
	glCompileShader(tvfs);

	free(vertex_shader);
	free(fragment_shader);
	free(blur_fragment_shader);
	free(lighting_fragment_shader);
	free(tv_fragment_shader);

	shader_program = glCreateProgram();
	glAttachShader(shader_program,fs);
	glAttachShader(shader_program,vs);
	glLinkProgram(shader_program);

	glGetShaderInfoLog(vs,500,&i,log);
	if(i){
		printf("shader_program log:\n");
		printf("vs info:\n%s\n",log);
	}

	glGetShaderInfoLog(fs,500,&i,log);
	if(i)
		printf("fs info:\n%s\n",log);

	glGetProgramInfoLog(shader_program,180,&i,log);
	if(i)
		printf("gl program info:\n%s\n",log);

	blur_shader_program = glCreateProgram();
	glAttachShader(blur_shader_program,bs);
	glAttachShader(blur_shader_program,vs);
	glLinkProgram(blur_shader_program);

	glGetShaderInfoLog(vs,500,&i,log);
	if(i)
		printf("vs info:\n%s\n",log);

	glGetShaderInfoLog(bs,500,&i,log);
	if(i)
		printf("bs info:\n%s\n",log);

	glGetProgramInfoLog(blur_shader_program,500,&i,log);
	if(i)
		printf("gl program info:\n%s\n",log);

	lighting_shader_program = glCreateProgram();
	glAttachShader(lighting_shader_program,lfs);
	glAttachShader(lighting_shader_program,vs);
	glLinkProgram(lighting_shader_program);

	glGetShaderInfoLog(vs,500,&i,log);
	if(i)
		printf("vs info:\n%s\n",log);

	glGetShaderInfoLog(lfs,500,&i,log);
	if(i)
		printf("lfs info:\n%s\n",log);

	glGetProgramInfoLog(lighting_shader_program,500,&i,log);
	if(i)
		printf("gl program info:\n%s\n",log);

	tv_shader_program = glCreateProgram();
	glAttachShader(tv_shader_program,tvfs);
	glAttachShader(tv_shader_program,vs);
	glLinkProgram(tv_shader_program);

	glGetShaderInfoLog(vs,500,&i,log);
	if(i)
		printf("vs info:\n%s\n",log);

	glGetShaderInfoLog(tvfs,500,&i,log);
	if(i)
		printf("tvfs info:\n%s\n",log);

	glGetProgramInfoLog(tv_shader_program,500,&i,log);
	if(i)
		printf("gl program info:\n%s\n",log);
}

void load_screen()
{
	glClearColor(0,0,0,0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	gluLookAt(0,0,0, 0,0,1, 0,1,0);
	glBegin(GL_TRIANGLES);
	glColor3f(0.337,0.0,0.0);

	glVertex3f(0.5,0.5,0);
	glVertex3f(0,-0.5,0);
	glVertex3f(-0.5,0.5,0);

	glColor3f(0,0,0);

	glVertex3f(0.5,1.3,0.5);
	glVertex3f(0,0.2,0.5);
	glVertex3f(-0.5,1.3,0.5);

	glVertex3f(0.5,1.3,0.5);
	glVertex3f(0.5,0.0,0.5);
	glVertex3f(-0.5,1.3,0.5);

	glVertex3f(0.5,1.3,0.5);
	glVertex3f(-0.5,0.0,0.5);
	glVertex3f(-0.5,1.3,0.5);

	glEnd();

	SDL_GL_SwapWindow(window);
}

void init_gl()
{
	glViewport(0,0,window_width,window_height);
	frame_ratio = window_width / (float) window_height;
	glClearColor(0.0,0.0,0.0,1.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	load_screen();
	init_textures();
	init_shaders();
	//	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE); //for wireframe mode
}

void init_particles()
{
	int i;
	for(i=0;i<MAX_EXPLOSIONS;i++){
		explosion[i].alive=0;
	}
	for(i=0;i<MAX_SPARKS;i++){
		spark[i].alive=0;
	}
	for(i=0;i<MAX_TRAILS;i++){
		trail[i].alive=0;
	}
	for(i=0;i<MAX_PORTICLES;i++){
		porticle[i].alive=0;
	}
}

int init_graphics()
{
	init_window_lib();
	init_gl();
	init_models();
	init_sprinkles();
	return 0;
}

void deinit_graphics()
{
	SDL_Quit();
}

void g_bind_t(int flag,GLuint tid)
{
	if(tid==whitey_texture || tid==screen_texture){
		glBindTexture(GL_TEXTURE_2D, tid);
	}else if(!post_processing){
		glBindTexture(GL_TEXTURE_2D,tid);
	}else{
		if(tid==player_texture){
			glBindTexture(GL_TEXTURE_2D,neon_texture);
			return;
		}
		glBindTexture(GL_TEXTURE_2D,black_texture);
	}
}

void draw_cell(struct field * field, struct vector color, int i , int j)
{
	struct vector s,w,a,d,x;
	s = (struct vector) {i,field->cell[i][j].y,j};
	w = (struct vector) {i,field->cell[i][j+1].y,j+1};
	a = (struct vector) {i-1,field->cell[i-1][j].y,j};
	d = (struct vector) {i+1,field->cell[i+1][j].y,j};
	x = (struct vector) {i,field->cell[i][j-1].y,j-1};

	struct vector cs = color;
	if(i%2){
		draw_field_line(s,a,cs,color);
		draw_field_line(s,d,cs,color);
	}
	if(j%2){
		draw_field_line(s,w,cs,color);
		draw_field_line(s,x,cs,color);
	}
}

void draw_field_lines(struct field * field, struct vector color)
{
	int i,j;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			if(field->cell[i][j].y<0.01)
				continue;
			draw_cell(field,color,i,j);
		}
	}
}

void add_field_block( struct vector * p, float * c, int * ib,
		struct field * field, struct vector color,
		int i, int j, int k,int ibk)
{
	float yoff = 0.00;
	struct vector v[4];
	v[0] = (struct vector) {i,field->cell[i][j].y-yoff,j};
	v[1] = (struct vector) {i,field->cell[i][j+1].y-yoff,j+1};
	v[2] = (struct vector) {i+1,field->cell[i+1][j].y-yoff,j};
	v[3] = (struct vector) {i+1,field->cell[i+1][j+1].y-yoff,j+1};

	p[k]=v[0];
	p[k+1]=v[1];
	p[k+2]=v[2];
	p[k+3]=v[3];

	ib[ibk]=k;
	ib[ibk+1]=k+1;
	ib[ibk+2]=k+2;

	ib[ibk+3]=k+2;
	ib[ibk+4]=k+1;
	ib[ibk+5]=k+3;

	k*= 4 ; //adjust for colors

	int l;
	for(l=0;l<4;l++){
		c[k]  =color.x;
		c[k+1]=color.y;
		c[k+2]=color.z;
		c[k+3]=(v[l].y)-0.1;
		k+=4;
	}
}

void generate_field_buffer(struct field * field, struct vector color,
		int p_buffer, int c_buffer,int ib_buffer)
{
	float * c;
	struct vector * p;
	int * ib;

	int psize = MAX_ROOM_WIDTH*MAX_ROOM_HEIGHT*(4*sizeof(struct vector));
	int csize = MAX_ROOM_WIDTH*MAX_ROOM_HEIGHT*(4*(sizeof(float)*4));
	int ibsize = MAX_ROOM_WIDTH*MAX_ROOM_HEIGHT*(6*sizeof(int));

	p = calloc(1,psize);
	c = calloc(1,csize);
	ib = calloc(1,ibsize);

	int i,j,k=0,ibk=0;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			if(!(field->cell[i][j].y<0.04))
				add_field_block(p,c,ib,field,color,i,j,k,ibk);
			k+=4;
			ibk+=6;
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER,p_buffer);
	glBufferData(GL_ARRAY_BUFFER, psize, p, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,c_buffer);
	glBufferData(GL_ARRAY_BUFFER, csize, c, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ib_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibsize, ib, GL_STATIC_DRAW);

	free(c);
	free(p);
	free(ib);
}

void draw_field_buffer( int p_buffer, int c_buffer,int index_buffer)
{
	int ibsize = MAX_ROOM_WIDTH*MAX_ROOM_HEIGHT*(6*sizeof(int));
	glEnableClientState(GL_VERTEX_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, p_buffer);
	glVertexPointer(3,GL_FLOAT,0,0);

	glEnableClientState(GL_COLOR_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, c_buffer);
	glColorPointer(4,GL_FLOAT,0,0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,index_buffer);

	glDrawElements(GL_TRIANGLES,ibsize,GL_UNSIGNED_INT,0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}

void draw_field(struct field * field,struct vector color)
{
	int p_buffer,c_buffer,index_buffer;

	glGenBuffers(1,&p_buffer);
	glGenBuffers(1,&c_buffer);
	glGenBuffers(1,&index_buffer);

	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);

	generate_field_buffer(field,color,p_buffer,c_buffer,index_buffer);
	draw_field_buffer(p_buffer,c_buffer,index_buffer);
	draw_field_lines(field,color);

	glDisable(GL_BLEND);

	glDeleteBuffers(1,&p_buffer);
	glDeleteBuffers(1,&c_buffer);
	glDeleteBuffers(1,&index_buffer);
}

void generate_porticles(struct doorway * d)
{
	if(post_processing)
		return;
	int r;
	r = rand()%MAX_PORTICLES;
	porticle[r].type= rand()%48;
	porticle[r].location= d->location;
	porticle[r].direction=
		(struct vector) { 0,(rand()%20)/200.0,0};
	porticle[r].color= d->color;
	porticle[r].alive=1;
}

void reset_particles()
{
	int i;
	for(i=0;i<MAX_EXPLOSIONS;i++){
		explosion[i].alive=0;
	}
}

void reset_lights() 
{
	float loc[4];
	float diffuse[4];

	loc[0]=0;
	loc[1]=0;
	loc[2]=0;
	loc[3]=0;
	diffuse[0]=0.0;
	diffuse[1]=0.0;
	diffuse[2]=0.0;
	diffuse[3]=0.0;
	int i;
	for(i=0;i<8;i++){
		glLightfv(GL_LIGHT0+i,GL_POSITION,loc);
		glLightfv(GL_LIGHT0+i,GL_DIFFUSE,diffuse);
	}
}

void draw_doorway(struct doorway * d,int i)
{
	if(i>4)
		printf("i>4\n");

	if(!d->is_connected)
		return;
	if(portal_timer<=0)
		generate_porticles(d);

	glPushMatrix();
	g_bind_t(GL_TEXTURE_2D, portal_texture);
	glTranslatef(d->location.x,d->location.y+0.2,d->location.z);
	glRotatef(-90,1,0,0);
	glScalef(0.3,0.3,0.3);
	glColor4f(1,1,1,1);
	draw_vbo_mesh(&door_model.mesh[0]);
	glPopMatrix();
}

float hud_item_rotation(struct item * i, int side)
{
	if(i->type==ITEM_SIDE_BLASTER && (side==1 || side==2)){
		return 45;
	}
	if(i->type==ITEM_SHIELD || i->type==ITEM_CAPACITOR){
		return 0;
	}
	switch(side){
		case 0: return 45;
		case 1: return 135;
		case 2: return 315;
		case 3: return 225;
	}
}

void draw_hud_item(struct item * i,struct vector l,int side)
{
	if(!(i->type))
		return;
	glEnable(GL_BLEND);
	glPushMatrix();
	g_bind_t(GL_TEXTURE_2D, item_texture);
	struct polygon a,b;

	struct vector s;

	s = (struct vector) {0.4,0.4,1.0};

	a.v[0].p = (struct vector) {s.x ,-s.y,0};
	a.v[1].p = (struct vector) {-s.x,-s.y,0};
	a.v[2].p = (struct vector) {s.x ,s.y,0};

	b.v[0].p = (struct vector) {s.x,s.y,0};
	b.v[1].p = (struct vector) {-s.x,-s.y,0};
	b.v[2].p = (struct vector) {-s.x,s.y,0};

	float w = (1/15.0)*(i->type);
	float n = -(1/15.0);

	glTranslatef(l.x,l.y,0);
	glRotatef(hud_item_rotation(i,side),0,0,1);
	glColor3f(1,1,1);
	glBegin(GL_TRIANGLES);
	glTexCoord2f(1.0,w);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(0.0,w);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(1.0,w+n);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(1.0,w+n);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(0.0,w);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(0.0,w+n);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();
	glDisable(GL_BLEND);
}

void draw_item(struct item * i,struct vector l)
{
	glEnable(GL_BLEND);
	glPushMatrix();
	g_bind_t(GL_TEXTURE_2D, item_texture);
	struct polygon a,b;

	struct vector s;

	s = (struct vector) {0.5,0.5,1.0};

	a.v[0].p = (struct vector) {s.x ,0.0,-s.y};
	a.v[1].p = (struct vector) {-s.x,0.0,-s.y};
	a.v[2].p = (struct vector) {s.x ,0.0,s.y};

	b.v[0].p = (struct vector) {s.x,0,s.y};
	b.v[1].p = (struct vector) {-s.x,0,-s.y};
	b.v[2].p = (struct vector) {-s.x,0,s.y};

	float w;
	w = (1/15.0)*(i->type);
	float n = -(1/15.0);

	glTranslatef(l.x,l.y+2,l.z);
	glRotated(90,-1,0,0);
	glColor3f(1,1,1);
	glBegin(GL_TRIANGLES);
	glTexCoord2f(1.0,w);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(0.0,w);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(1.0,w+n);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(1.0,w+n);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(0.0,w);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(0.0,w+n);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();
	glDisable(GL_BLEND);
}

void draw_shop(struct shop * shop)
{
	g_bind_t(GL_TEXTURE_2D, player_texture);

	glPushMatrix();
	glTranslatef(shop->keeper_location.x,shop->keeper_location.y+2,
			shop->keeper_location.z);
	//glScalef(0.3,1.0,0.3);
	glRotatef(90,0,1,0);
	glRotatef(90,1,0,0);
	glColor4f(1,1,1,1);
	glDisable(GL_BLEND);
	draw_vbo_mesh(&shp_model.mesh[0]);
	glEnable(GL_BLEND);
	glPopMatrix();

	int i;
	for(i=0;i<MAX_TRANSACTIONS;i++){
		glPushMatrix();
		g_bind_t(GL_TEXTURE_2D, player_texture);
		glTranslatef(shop->t[i].location.x,shop->t[i].location.y+0.8,
			shop->t[i].location.z);
		glScalef(0.3,0.5,0.3);
		glColor4f(1,1,1,1);
		glRotatef(-90,1,0,0);
		draw_vbo_mesh(&stand_model.mesh[0]);
		glPopMatrix();
		if(!shop->t[i].sold){
			glPushMatrix();
			draw_item(&(shop->t[i].item),shop->t[i].location);
			glPopMatrix();
		}
	}
}

void draw_fields(struct game_state * gs,double delta)
{
	draw_field(&(gs->rfield),(struct vector) {0.655,0.161,0.161});
	draw_field(&(gs->gfield),(struct vector) {0.129,0.522,0.129});
	draw_field(&(gs->bfield),(struct vector) {0.098,0.396,0.384});
}

float sprink_bar[3];

void draw_hori_sprink_bars(struct particle * p)
{
	int i;
	for(i=0;i<3;i++){
		if(sprink_bar[i]>1.0)
			sprink_bar[i]-=0.1;
		else if (sprink_bar[i]<0.2)
			sprink_bar[i]+=0.1;
		sprink_bar[i]+=(((rand()%3)-1)/30.0);
	}


	glRotatef(180,0,1,0);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);

	glPushMatrix();
	glTranslatef(0.9+sprink_bar[0],-0.1,1.5);
	glScalef(1.20-sprink_bar[0],1.00,0.5);
	glColor4f(0.655,0.38,0.0,1.7-post_processing);
	draw_animation(&blank_plane_model,0.1);
	glPopMatrix();

	float ry = (rand()%100)/100.0;
	glPushMatrix();
	glTranslatef(0.9+sprink_bar[1],-0.1,0.0);
	glScalef(1.20-sprink_bar[1],1.00,0.5);
	glColor4f(0.655,0.161,0.161,1.7-post_processing);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	draw_animation(&blank_plane_model,0.1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.9+sprink_bar[2],-0.1,-1.5);
	glScalef(1.20-sprink_bar[2],1.00,0.5);
	glColor4f(0.129,0.522,0.129,1.7-post_processing);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	draw_animation(&blank_plane_model,0.1);
	glPopMatrix();
}

void draw_vert_sprink_bars(struct particle * p)
{
	int i;
	for(i=0;i<3;i++){
		if(sprink_bar[i]>1.0)
			sprink_bar[i]-=0.1;
		else if (sprink_bar[i]<0.2)
			sprink_bar[i]+=0.1;
		sprink_bar[i]+=(((rand()%3)-1)/30.0);
	}

	g_bind_t(GL_TEXTURE_2D, whitey_texture);

	glPushMatrix();
	glTranslatef(1.2,-0.2,1.9-sprink_bar[0]);
	glScalef(0.35,1.00,1.00+sprink_bar[0]);
	glColor4f(0.655,0.38,0.161,1.7-post_processing);
	draw_animation(&blank_plane_model,0.1);
	glPopMatrix();

	float ry = (rand()%100)/100.0;
	glPushMatrix();
	glTranslatef(0,-0.2,1.9-sprink_bar[1]);
	glScalef(0.35,1.00,1.00+sprink_bar[1]);
	glColor4f(0.655,0.161,0.161,1.7-post_processing);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	draw_animation(&blank_plane_model,0.1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.2,-0.2,1.9-sprink_bar[2]);
	glScalef(0.35,1.00,1.00+sprink_bar[2]);
	glColor4f(0.129,0.522,0.129,1.7-post_processing);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	draw_animation(&blank_plane_model,0.1);
	glPopMatrix();
}

float sprink_wave;

void draw_wave_sprink_screen(struct particle * p)
{
	sprink_wave+=0.02;
	int i;
	for(i=0;i<7;i++){
		glPushMatrix();
		glTranslatef(sin(sprink_wave+(i)),-0.1,-2.5+(i*0.8));
		glScalef(0.50,0.50,0.50);
		glColor4f(0.129,0.522,0.129,1.7-post_processing);
		g_bind_t(GL_TEXTURE_2D, whitey_texture);
		draw_animation(&blank_plane_model,0.1);
		glPopMatrix();

	}
}

void draw_sprink_screen(struct particle * p)
{
	g_bind_t(GL_TEXTURE_2D, player_texture);
	glColor4f(1,1,1,1);
	glPushMatrix();
	glTranslatef(p->location.x,p->location.y,p->location.z);
	glRotatef(90,0,0,1);
	glRotatef(-90*p->direction.x,1,0,0);
	glTranslatef(0,-1,0);
	glScalef(0.25,0.25,0.25);
	draw_animation(&sprink_screen_model,0.1);

	switch(p->kind){
		case 0: draw_vert_sprink_bars(p); break;
		case 1: draw_hori_sprink_bars(p); break;
		case 2: draw_wave_sprink_screen(p); break;
	}

	glPopMatrix();
}

void draw_sprink_storage_tank(struct particle * p)
{
	g_bind_t(GL_TEXTURE_2D, player_texture);
	glColor4f(1,1,1,1);
	glPushMatrix();
	glTranslatef(p->location.x,p->location.y,p->location.z);
	glTranslatef(0,-1,0);
	glScalef(0.8,1,0.8);
	draw_animation(&sprink_storage_tank_model,0.1);
	glPopMatrix();
}

void draw_sprink_valve(struct particle * p)
{
	g_bind_t(GL_TEXTURE_2D, player_texture);
	glColor4f(1,1,1,1);
	glPushMatrix();
	glTranslatef(p->location.x,p->location.y,p->location.z);
	glRotatef(90,0,0,1);
	glRotatef(-90*p->direction.x,1,0,0);
	glTranslatef(0,-1,0);
	glScalef(0.25,0.25,0.25);
	draw_animation(&sprink_valve_model,0.1);

	glPopMatrix();
}

void draw_sprink_example(struct particle * p)
{
	g_bind_t(GL_TEXTURE_2D, player_texture);
	glColor4f(1,1,1,1);
	glPushMatrix();
	glTranslatef(p->location.x,p->location.y,p->location.z);
	draw_animation(&sprink_example_model,0.1);

	glPopMatrix();
}

void draw_straight_pipe(struct particle * p)
{
	g_bind_t(GL_TEXTURE_2D, player_texture);
	glColor4f(1,1,1,1);
	glPushMatrix();
	glTranslatef(p->location.x,p->location.y,p->location.z);
	draw_animation(&sprink_straight_pipe_model,0.1);
	glPopMatrix();
}

void draw_sprinkles(struct room * room)
{
	if(post_processing)
		return;
	int i;
	for(i=0;i<MAX_SPRINKLES;i++){
		if(!room->sprinkles[i].alive)
			continue;
		if(room->sprinkles[i].type == SPRINK_INTERGRID){
			draw_sprink_screen(&(room->sprinkles[i]));
		}
		if(room->sprinkles[i].type == SPRINK_VALVE){
			draw_sprink_valve(&(room->sprinkles[i]));
		}
		if(room->sprinkles[i].type == SPRINK_STORAGE_TANK){
			draw_sprink_storage_tank(&(room->sprinkles[i]));
		}
		if(room->sprinkles[i].type == SPRINK_EXAMPLE){
			draw_sprink_example(&(room->sprinkles[i]));
		}
		if(room->sprinkles[i].type == SPRINK_STRAIGHT_PIPE){
			draw_straight_pipe(&(room->sprinkles[i]));
		}
	}
}

void draw_room(struct room * room)
{
	struct vector light_color = {0.098,0.396,0.384};
	float fc =0.9;
	if(room->gs.n_npcs>0){
		light_color = (struct vector){0.655,0.161,0.161};
	}
	int i;
	float yoff =0.01;
	for(i=0;i<(room->n_doorways);i++){
		draw_doorway(&room->doorway[i],i);
	}

	g_bind_t(GL_TEXTURE_2D, floor_texture);
	glColor3f(fc,fc,fc);
	draw_vbo_mesh(&room->model.mesh[0]);/*floors*/

	if(!post_processing){
		glColor3f(fc-yoff,fc-yoff,fc-yoff);
		draw_vbo_mesh(&room->model.mesh[2]);/* dark sprinkles */

		glColor3f(fc+yoff,fc+yoff,fc+yoff);
		draw_vbo_mesh(&room->model.mesh[3]);/* light sprinkles */
	}

	g_bind_t(GL_TEXTURE_2D, roof_texture);
	glColor3f(1,1,1);
	draw_vbo_mesh(&room->model.mesh[4]);//roof

	g_bind_t(GL_TEXTURE_2D, wall_texture);
	glColor3f(room->color.x,room->color.y,room->color.z);
	draw_vbo_mesh(&room->model.mesh[1]);/* walls*/

	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glColor4f(light_color.x,light_color.y,light_color.z,1.0);
	if(post_processing){
		glColor4f(	light_color.x+0.2,
				light_color.y+0.2,
				light_color.z+0.2,0.4);
	}
	//draw_vbo_mesh(&room->model.mesh[5]);/* floor lights*/

	draw_sprinkles(room);
	if(pi.keys['n'])
		draw_debug_neighborhoods();
}

void draw_yoyo(struct unit u){
	struct vector a,b;
	struct game_state * gs = &(world_map.current_room->gs);
	a = u.location;
	b = gs->npc[u.connected_to].location;
	a.y+=1;
	b.y+=1;
	draw_line(a,b,(struct vector) {1,0,0},5.0,1);
	glPushMatrix();
	glTranslatef(u.location.x,u.location.y,u.location.z);
	glScalef(1,1.5,1);
	glColor4f(1,1,1,1);
	draw_animation(&yo_boss_model,0.1);
	glPopMatrix();
}

void draw_unit_speech(struct game_state * gs, struct unit * u,double d)
{
	struct vector l;
	l = world_to_screen(gs,u->location);
	if(u->speech.line[u->speech.l]!=NULL){
		draw_n_text(l.x,l.y+1,u->speech.n,
				u->speech.line[u->speech.l],
				(struct vector) {0.9,1,1});
		if(!paused)
			u->speech.timer+=d;
		if(u->speech.timer>0.08){
			if(u->speech.n<strlen(u->speech.line[u->speech.l])){
				u->speech.n++;
				u->speech.timer=0;
			}else if(u->speech.timer>1.0){
				u->speech.n=0;
				u->speech.l++;
				u->speech.timer=0;
			}
		}
	}
}

void draw_antenna(struct unit u, double d)
{
	glPushMatrix();
	glTranslatef(u.location.x,u.location.y,u.location.z);
	glRotatef(-90,1,0,0);
	glScalef(1,1,0.25);
	if(post_processing && u.hit_timer<=0)
		glColor4f(1,1,1,sin(u.cooldown));
	switch((int)u.cooldown2){
		case 0:
			draw_vbo_mesh(&r_ant_model.mesh[0]);
			break;
		case 1:
			draw_vbo_mesh(&g_ant_model.mesh[0]);
			break;
		case 2:
			draw_vbo_mesh(&b_ant_model.mesh[0]);
			break;
	}
	glPopMatrix();
}

int movement_rot_angle(int moving)
{
	switch(moving)
	{
		case 1://w
			return 45;
		case 7://saw
		case 2://a
			return 135;
		case 3://wa
			return 90;
		case 4://s
			return 180+45;
		case 6://sa
			return 180;
		case 8://d
			return 180+135;
		case 12://sd
			return 180+90;
		case 5://sw
		case 9://dw
		default:
			return 0;
	}
}

float last_movement_rot=0;
void movement_rot(int moving,double delta)
{
	double mra = movement_rot_angle(moving);

	last_movement_rot=correct_angle(last_movement_rot);
	float l;
	if(mra<last_movement_rot)
		l=last_movement_rot-mra;
	else
		l=mra-last_movement_rot;

	delta*=360.0;
	last_movement_rot+=delta*angle_wise(last_movement_rot,mra);

	if(l<5)
		last_movement_rot=mra;

	glRotatef(last_movement_rot,0,1,0);
}

/* this draws to the players head screen */
void draw_to_screen(double d)
{
	GLuint old_fbo;
	glGetIntegerv(GL_FRAMEBUFFER_BINDING,&old_fbo);
	glPushMatrix();
	glLoadIdentity();
	// set rendering destination to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, fboId);
	glViewport(0,0,SCREEN_WIDTH,SCREEN_HEIGHT);

	// clear buffers
	glClearColor(0,0,0,0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, fboId);
	glBindTexture(GL_TEXTURE_2D, screen_texture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0,
			SCREEN_WIDTH, SCREEN_HEIGHT);
	// unbind FBO
	glBindFramebuffer(GL_FRAMEBUFFER, old_fbo);
	glViewport(0,0,window_width,window_height);
	glPopMatrix();
}

struct model * get_component_model(struct item * i)
{
	switch(i->type)
	{
		case ITEM_TELEDICE:
			return &chassy_tele_model;
			break;
		case ITEM_SHIELD:
			return &chassy_shield_model;
			break;
		case ITEM_CAPACITOR:
			return &chassy_capacitor_model;
			break;
		case ITEM_DASH:
			return &chassy_dash_model;
			break;
		case ITEM_PLASMA:
			return &chassy_plasma_model;
			break;
		case ITEM_LASER:
			if(i->state)
				return &chassy_side_laser_model;
			else
				return &chassy_laser_model;
			break;
		case ITEM_VULCAN:
			return &chassy_vulcan_model;
			break;
		case ITEM_MISSILE:
			if(i->state)
				return &chassy_side_missile_model;
			else
				return &chassy_missile_model;
			break;
		case ITEM_SIDE_BLASTER:
			if(i->state)
				return &chassy_side_blaster_model;
			else
				return &chassy_blaster_model;
			break;
		case ITEM_HOOK:
			return &hook_open_model;
			break;
		case ITEM_CHAINSAW:
			if(i->state==ITEM_STATE_ON)
				return &chassy_chainsaw_on_model;
			else
				return &chassy_chainsaw_off_model;
			break;
		case ITEM_FLAMETHROWER:
			return &chassy_flamethrower_model;
			break;
	}
	return NULL;
}

void draw_player_component(struct item * i, double d)
{
	struct model * m=get_component_model(i);
	if(i->type==ITEM_SIDE_BLASTER && i->state){
		glRotatef(90,0,1,0);
		glTranslatef(-(i->cooldown)*2,0.0,0.0);
	}
	if((i->type==ITEM_MISSILE || i->type==ITEM_LASER) && i->state){
		glRotatef(90,0,1,0);
	}
	if(i->type==ITEM_HOOK){
		if(i->cooldown>0)
			return;
	}
	if(i->type==ITEM_VULCAN){
		glRotatef(i->rotation_angle,0,1,0);
	}
	if(i->type==ITEM_CHAINSAW && i->state == ITEM_STATE_ON){
		glTranslatef((rand()%10)/40.0,(rand()%5)/20.0,(rand()%10)/40.0);
	}
	if(i->type==ITEM_SHIELD && i->active && (i->cooldown)<=0)
	{
		glTranslatef(0,1,0);
		glScalef(2,2,3);
	}

	if(i->type==ITEM_LASER && post_processing)
	{
		glColor4f(1,1,1,i->duration);
	}

	if(m){
		draw_animation(m,d);
	}
}

void draw_player_components(struct unit * u, double d)
{
	glPushMatrix();
	glTranslatef(0.0,0.0,-1.5);
	glRotatef(-90,0,0,1);
	glRotatef(-90,1,0,0);
	draw_player_component(&u->inventory.item[0],d);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1.5,0.0,0.0);
	glRotatef(-90,0,0,1);
	draw_player_component(&u->inventory.item[1],d);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.5,0.0,0.0);
	glRotatef(-90,0,0,1);
	glRotatef(180,1,0,0);
	glRotatef(180, 0,1,0);
	draw_player_component(&u->inventory.item[2],d);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0,0.0,1.5);
	glRotatef(-90,0,0,1);
	glRotatef(90,1,0,0);
	draw_player_component(&u->inventory.item[3],d);
	glPopMatrix();
}

void draw_player_path(struct unit * u, double d)
{
	glPushMatrix();

	glTranslatef(u->destination.x,u->destination.y+0.01,u->destination.z);
	struct vector a = {0,0,1};
	struct vector b = {0,0,-1};
	struct vector c = {0,1,1};
	draw_line(a,b,c,10.0,1);
	a = (struct vector){1,0,0};
	b = (struct vector){-1,0,0};
	draw_line(a,b,c,10.0,1);

	glPopMatrix();
}

void draw_hook(struct item * i, double d)
{
	struct game_state * gs = &(world_map.current_room->gs);
	glPushMatrix();

	struct vector bvec = {i->location.x,i->location.y+1.2,i->location.z};
	struct vector cvec = gs->game_player.location;
	cvec.y+=1.2;
	draw_line(cvec,bvec,(struct vector){0,1,1}, 8.0,1);

	glTranslatef(i->location.x,i->location.y+1.2,i->location.z);
	glRotatef(90+i->rotation_angle,0,1,0);
	glRotatef(90,0,0,1);
	glScalef(0.4,0.4,0.4);
	glColor3f(1,1,1);
	if(i->state==ITEM_STATE_HOOK_CLOSED){
		draw_animation(&hook_closed_model,d);
	} else {
		draw_animation(&hook_open_model,d);
	}
	glPopMatrix();
}

void draw_player_hook(struct unit * u, double d)
{
	int i;
	for(i=0;i<4;i++){
		if((u->inventory.item[i].type == ITEM_HOOK) &&
				(u->inventory.item[i].cooldown>0)){
			draw_hook(&u->inventory.item[i],d);
		}
	}
}

void draw_player_head(struct unit * u, double d)
{
	glPushMatrix();
	glTranslatef(0,1.0,0);
	if(post_processing && u->hit_timer<=0)
		glColor4f(0,1,1,1);
	draw_animation(&ranger_head_model,d);
	if(post_processing)
		glColor4f(1,1,1,1);
	glPopMatrix();
}

void draw_debug_neighborhoods()
{
	if(post_processing)
		return;
	struct graph * g = &(world_map.current_room->neighborhoods);
	struct neighborhood * n;
	float yoff = 0.01;

	int i;
	for(i=0;i<(g->n_elements);i++){
		yoff+=0.001;

		n = (struct neighborhood *) g->element[i];

		glColor4f(i%4,i%3,i%2,0.2);

		glBegin(GL_TRIANGLES);
		glVertex3f(n->a.x, n->a.y+yoff, n->a.z);
		glVertex3f(n->b.x, n->b.y+yoff, n->b.z);
		glVertex3f(n->c.x, n->c.y+yoff, n->c.z);

		glVertex3f(n->a.x, n->a.y+yoff, n->a.z);
		glVertex3f(n->d.x, n->d.y+yoff, n->d.z);
		glVertex3f(n->c.x, n->c.y+yoff, n->c.z);
		glEnd();
	}
}

void draw_debug_wall_intersect_line(struct unit * u, double d)
{
	struct game_state * gs = &(world_map.current_room->gs);
	struct vector a = last_camera;
	struct vector b = screen_to_world(gs,pi.mouse_x,pi.mouse_y);
	struct vector c = {1, wall_intersects_line(a,b),0};
	a.y=0.01;
	b.y+=0.01;
	glPushMatrix();
	draw_line(a,b,c,5,1);
	glPopMatrix();
}

float transition_effect_timer;

void transition_effect(double delta)
{
	if(transition_effect_timer>0){
		transition_effect_timer-=delta;
		glColor4f(1-transition_effect_timer,1-transition_effect_timer,1,1-transition_effect_timer);
	}else{
		transition_effect_timer=0;
	}
}

void static_effect(float amount)
{
	struct vector rdir = random_direction();
	rdir = v_s_mul(amount,rdir);
	glScalef(1+rdir.x,1+rdir.y,1+rdir.z);
	glColor4f(1,1,1,1-amount);
}

float chassy_bounce=0;
float tv_bounce=0;

void dash_effect(struct unit * u, double d)
{
	glScalef(0.7,1,2.4);
	dash_trail(u);
}

void player_color(struct unit *u, double d)
{
	if((u->hit_timer)>0){
		g_bind_t(GL_TEXTURE_2D, whitey_texture);
		glTexCoord2f(0.5,0.5);
		static_effect(u->hit_timer*0.2);
	}else{
		g_bind_t(GL_TEXTURE_2D, player_texture);
		glColor3f( u->color.x, u->color.y, u->color.z);
	}
}

void draw_player(struct unit *u, double d)
{
	double ds = d*(u->max_speed*0.2)*(u->speed/u->max_speed);
	draw_unit_shadow(u,d);

	glPushMatrix();

	player_color(u,d);

	glTranslatef(u->location.x,u->location.y,u->location.z);
	if(u->moving){
		chassy_bounce+=ds*8;
		glTranslatef(0.0,(sin(chassy_bounce*(M_PI/2))/8),0.0);
	}else{
		chassy_bounce=0;
	}

	glPushMatrix();
	glScalef(0.8,0.8,0.8);
	glRotatef(u->rotation_angle-180,u->rotation.x,u->rotation.y,u->rotation.z);
	if(u->dash_timer>0.0)
		dash_effect(u,d);
	glTranslatef(0.0,1.5,0.0);

	glPushMatrix();
	glScalef(0.4,0.5,0.4);
	draw_animation(&chasis_model,d);
	draw_player_components(u,d);
	glPopMatrix();

	glPushMatrix();
	glRotatef(90,0,1,0);
	glScalef(0.7,0.7,0.7);
	glTranslatef(0.0,0.8,-0.2);
	tv_bounce+=d*((4*126.0)/60.0);
	if(pi.keys['t'])
		tv_bounce=1;
	glTranslatef(0.0,(sin(tv_bounce*(M_PI/2))/16),0.0);

	draw_player_head(u,d);

	g_bind_t(GL_TEXTURE_2D, player_texture);
	glPopMatrix();

	glPopMatrix();

	glPushMatrix();
	player_color(u,d);
	glScalef(0.6,0.5,0.6);
	if(u->moving){
		movement_rot(u->moving,d);
		glTranslatef(-0.8,0.0,0.0);
		draw_animation(&left_leg_model,ds);
		glTranslatef(0.8,0.0,0.0);
		draw_animation(&right_leg_model,ds);
	}else{
		glTranslatef(0.0,0.0,0.0);
		draw_animation(&player_leg_still,d);
		glTranslatef(0.8,0.0,0.0);
		draw_animation(&player_leg_still,d);
	}
	glPopMatrix();

	glPopMatrix();

	if((u->hit_timer)>=0){
		u->hit_timer-=d;
	}
	//draw_player_path(u,d);
	draw_player_hook(u,d);
}

void draw_tank(struct game_state * gs, struct unit * u, double d)
{
	glPushMatrix();
	glTranslatef(u->location.x,1+u->location.y,u->location.z);
	glScalef(0.8,0.8,0.8);
	draw_animation(&tank_bottom,d);
	glTranslatef(0,-0.2,0);
	glRotatef(u->rotation_angle-180,
			u->rotation.x,u->rotation.y,u->rotation.z);
	d/=u->max_cooldown;
	draw_animation(&tank_top,d);
	glPopMatrix();
}

void draw_sentry(struct game_state * gs, struct unit * u, double d)
{
	glPushMatrix();
	glTranslatef(u->location.x,u->location.y,u->location.z);
	draw_animation(&sentry_stand,d);
	glRotatef(u->rotation_angle-180,
			u->rotation.x,u->rotation.y,u->rotation.z);
	d/=u->max_cooldown;
	draw_animation(&sentry_top,d);
	glPopMatrix();
}

void draw_double_sentry(struct game_state * gs, struct unit * u, double d)
{
	glPushMatrix();
	glTranslatef(u->location.x,u->location.y,u->location.z);
	draw_animation(&sentry_stand,d);
	d/=u->max_cooldown;
	glRotatef(u->rotation_angle-180,
			u->rotation.x,u->rotation.y,u->rotation.z);
	glTranslatef(0.5,0,0);
	draw_animation(&sentry_top,d);
	glTranslatef(-1,0,0);
	draw_animation(&sentry_top,d);
	g_bind_t(GL_TEXTURE_2D,whitey_texture);
	g_bind_t(GL_TEXTURE_2D,whitey_texture);
	draw_line((struct vector){0,1.5,0},(struct vector){1,1.5,0},
		 	(struct vector){1,0,0},20,1);
	g_bind_t(GL_TEXTURE_2D, player_texture);
	glPopMatrix();
}

double coin_shadow(struct unit * u, double d)
{
	if((u->cooldown) < 1)
		return u->cooldown/2;
	return 0.5;
}

double shadow_yoff=0.0001;
double last_shadow_frame;

void draw_unit_shadow(struct unit *u, double d)
{
	if(frames!=last_shadow_frame){
		shadow_yoff=0.0001;
		last_shadow_frame=frames;
	}
	shadow_yoff+=0.0001;
	if(shadow_yoff>0.0050)
		shadow_yoff=0.0001;
	double yoff = shadow_yoff;

#define SHADOW_RESOLUTION 16
	glEnable(GL_BLEND);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);

	double h = u->hit_radius;
	if(u->type == UNIT_TYPE_ITEM)
		h=coin_shadow(u,d);

	struct vector a[SHADOW_RESOLUTION];
	float y=0.01;
	struct vector dvec = {0};
	double dir =0;

	int i;
	for(i=0;i<SHADOW_RESOLUTION;i++){
		dvec = rot_to_dvec(i*(360/SHADOW_RESOLUTION));
		dvec.x*=h;
		dvec.z*=h;
		a[i] = dvec;
	}

	glPushMatrix();
	glTranslatef(u->location.x,u->location.y+yoff,u->location.z);
	glBegin(GL_TRIANGLE_FAN);

	glColor4f(0,0,0,0.3);
	glVertex3f(0,0,0);
	glColor4f(0,0,0,0.0);

	for(i=0;i<SHADOW_RESOLUTION;i++){
		glVertex3f(a[i].x,a[i].y,a[i].z);
	}
	glVertex3f(a[0].x,a[0].y,a[0].z);

	glEnd();
	glPopMatrix();

	glDisable(GL_BLEND);
}

void draw_spade_components(struct unit * u, double d)
{
	draw_player_components(u,d);
}

void draw_spade(struct game_state * gs, struct unit * u, double d)
{
	glPushMatrix();
	glTranslatef(u->location.x,u->location.y+2,u->location.z);
	glRotatef(u->rotation_angle-180,u->rotation.x,u->rotation.y,
			u->rotation.z);
	glScalef(0.50,0.50,0.50);
	glPushMatrix();
	glTranslatef(0,-0.5,0);
	draw_animation(&crt_head_model,d);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,-1.5,0);
	glScalef(0.7,0.8,0.7);
	draw_animation(&spade_chasis_model,d);
	draw_spade_components(u,d);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,-4,0);
	glScalef(1.2,1.2,1.2);
	if(u->moving){
		glTranslatef(-0.8,0.0,0.0);
		draw_animation(&left_leg_model,d);
		glTranslatef(0.8,0.0,0.0);
		draw_animation(&right_leg_model,d);
	}else{
		glTranslatef(0.0,0.0,0.0);
		draw_animation(&player_leg_still,d);
		glTranslatef(0.8,0.0,0.0);
		draw_animation(&player_leg_still,d);
	}
	glPopMatrix();

	glPopMatrix();
}

void draw_ranger(struct unit * u, double d)
{
	if(post_processing && (u->hit_timer<=0) ){
		glColor4f(1,1,1,1-(u->cooldown*2));
	}
	glPushMatrix();
	glTranslatef(u->location.x,u->location.y,u->location.z);
	glRotatef(u->rotation_angle-180,
			u->rotation.x,u->rotation.y,u->rotation.z);
	draw_animation(&ranger_model,d);
	glPopMatrix();
}

void draw_unit(struct game_state * gs, struct unit *u,double d)
{
	draw_unit_shadow(u,d);
	glPushMatrix();

	glColor4f(u->color.x,u->color.y,u->color.z,1);
	if(u->poison_timer>0){
		glColor4f(u->color.x/2,1,u->color.z/2,1);
	}

	if(u->hit_timer>0){
		g_bind_t(GL_TEXTURE_2D, whitey_texture);
		glTexCoord2f(0.5,0.5);
		static_effect(u->hit_timer*0.2);
	}else{
		g_bind_t(GL_TEXTURE_2D, player_texture);
	}

	switch(u->type){
		case UNIT_TYPE_PLAYER:
			draw_model(p_model,u->location,
					u->rotation_angle,u->rotation);
			break;
		case UNIT_TYPE_SHOP:
			draw_model(shp_model,u->location,
					u->rotation_angle,u->rotation);
			break;
		case UNIT_TYPE_ITEM:
			glTranslatef(u->location.x,u->location.y+0.8,u->location.z);
			glRotatef(u->rotation_angle,
					u->rotation.x,u->rotation.y,u->rotation.z);
			glScalef(0.5,0.5,0.05);
			if(u->cooldown<1){
				glScalef(u->cooldown,u->cooldown,u->cooldown);
			}
			draw_vbo_mesh(&coin_model.mesh[0]);
			break;
		case UNIT_TYPE_NEUTRAL_CREEP:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y,u->location.z);
			glRotatef(u->rotation_angle-180,u->rotation.x,u->rotation.y,u->rotation.z);
			draw_animation(&scav_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_SHIELDMAN:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y,u->location.z);
			glRotatef(u->rotation_angle-180,
					u->rotation.x,u->rotation.y,u->rotation.z);
			if(u->cooldown2>=0){
				draw_animation(&shieldman_attacking_model,d);
			}else{
				draw_animation(&shieldman_running_model,d);
			}
			glPopMatrix();
			break;
		case UNIT_TYPE_BOSS:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y,u->location.z);
			glScalef(1.3,1.3,1.3);
			draw_animation(&b_model,d);
			glPopMatrix();
			draw_unit_shadow(u,d);
			draw_unit_shadow(u,d);
			draw_unit_shadow(u,d);
			break;
		case UNIT_TYPE_RANGER:
			draw_ranger(u,d);
			break;
		case UNIT_TYPE_MOLE:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y,u->location.z);
			glRotatef(u->rotation_angle-90,u->rotation.x,u->rotation.y,u->rotation.z);
			draw_animation(&mole_boss_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_YO:
			draw_yoyo(*u);
			break;
		case UNIT_TYPE_SIGN:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y,u->location.z);
			glRotatef(u->rotation_angle-90,u->rotation.x,u->rotation.y,u->rotation.z);
			draw_animation(&fh_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_INTERCOM:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y+1,u->location.z);
			glRotatef(u->rotation_angle-180,
				u->rotation.x,u->rotation.y,u->rotation.z);
			glScalef(0.5,0.5,0.5);
			draw_animation(&intercom_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_ANTENNA:
			draw_antenna(*u,d);
			break;
		case UNIT_TYPE_SHOTTY_RANGER:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y,u->location.z);
			glRotatef(u->rotation_angle-180,u->rotation.x,u->rotation.y,u->rotation.z);
			draw_animation(&shotty_ranger_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_SPIDER:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y,u->location.z);
			glRotatef(u->rotation_angle-90,u->rotation.x,u->rotation.y,u->rotation.z);
			glScalef(0.7,0.7,0.7);
			draw_animation(&spider_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_SENTRY:
			draw_sentry(gs,u,d);
			break;
		case UNIT_TYPE_DOUBLE_SENTRY:
			draw_double_sentry(gs,u,d);
			break;
		case UNIT_TYPE_SPINNER:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y+1,u->location.z);
			glRotatef(180,1,0,0);
			glRotatef(u->rotation_angle-90,u->rotation.x,u->rotation.y,u->rotation.z);
			draw_animation(&spinner_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_SPINNER_BOSS:
			glPushMatrix();
			glTranslatef(u->location.x,u->location.y+1,u->location.z);
			glRotatef(180,1,0,0);
			glScalef(1.5,1.5,1.5);
			glRotatef(u->rotation_angle-90,u->rotation.x,u->rotation.y,u->rotation.z);
			draw_animation(&spinner_model,d);
			glPopMatrix();
			break;
		case UNIT_TYPE_TANK:
			draw_tank(gs,u,d);
			break;
		case UNIT_TYPE_SPADE:
			draw_spade(gs,u,d);
			break;
		default:
			printf("unrecognized model type, not drawing\n");
			break;
	}
	glPopMatrix();

}

void draw_path(struct path path)
{
	int i;
	for(i=0;i<path.n_interpoints;i++){
		if(path.interpoint[i].x == 0 && path.interpoint[i].z==0)
			break;
		if(path.interpoint[i+1].x == 0 && path.interpoint[i+1].z==0)
			break;
		path.interpoint[i].y=0.2;
		draw_line(path.interpoint[i],path.interpoint[i+1],
				(struct vector) {1,1,1},5.0,1);
	}
}

void draw_bullet(struct bullet b)
{
	glEnable(GL_BLEND);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	struct vector v = b.location;
	struct vector w = b.location;
	struct vector color = {1,1,1};
	struct vector trail_color = {1,1,0};
	if(b.flags&HAS_VAIL)
		trail_color=(struct vector) {0,1,0};

	v.x += b.direction.x*1.1;
	v.y += b.direction.y+1.21;
	v.z += b.direction.z*1.1;

	w.x-=b.direction.x*2;
	w.y+=1.21;
	w.z-=b.direction.z*2;

	if(post_processing){
		draw_2line(w,v,trail_color,trail_color,0,1,20);
	}

	v.x -= b.direction.x*0.1;
	v.y += 0.01;
	v.z -= b.direction.z*0.1;

	w.x += b.direction.x*2.5;
	w.y += 0.01;
	w.z += b.direction.z*2.5;


	draw_line(w,v,color,16,1);

	w.y=0.1;
	v.y=0.1;
	draw_line(w,v,(struct vector){0,0,0},16,0.2);

	glDisable(GL_BLEND);
}

void draw_bullets(struct game_state * gs, double d)
{
	int i;
	for(i=0;i<gs->n_bullets;i++){
		draw_bullet(gs->bullet[i]);
	}
}

void dash_trail(struct unit * u)
{
	int r = rand()%MAX_TRAILS;
	double d = u->rotation_angle+dir_to_rot(u->dash_dir)-180;
	struct vector dvec = rot_to_dvec(d);
	trail[r].location=u->location;
	trail[r].location.x-=dvec.x;
	trail[r].location.z-=dvec.z;
	trail[r].direction=dvec;
	trail[r].color=(struct vector){0,1,1};
	trail[r].alive=1;
}

void missile_trail(struct game_state * gs, int i , double d)
{
	int r = rand()%MAX_TRAILS;
	struct vector dvec = rot_to_dvec(gs->missile[i].direction);
	trail[r].location= gs->missile[i].location;
	trail[r].location.x-=dvec.x;
	trail[r].location.z-=dvec.z;
	trail[r].direction=dvec;
	trail[r].color=(struct vector){1,1,0};
	trail[r].alive=1;
}

double target_delta=0;

void draw_missile_target(struct game_state * gs, int i , double d)
{
	target_delta+=d*360;
	glPushMatrix();
	struct vector loc = gs->missile[i].target->location;
	glTranslatef(loc.x,loc.y,loc.z);
	glRotatef(target_delta,0,1,0);
	draw_animation(&fh_model,d);
	glPopMatrix();
}

void draw_missiles(struct game_state * gs, double d)
{
	int i;
	struct vector l;
	for(i=0;i<gs->n_missiles;i++){
		missile_trail(gs,i,d);

		glPushMatrix();
		l = gs->missile[i].location;
		glTranslatef(l.x,l.y+1,l.z);
		glScalef(0.4,0.4,0.4);
		glRotatef(gs->missile[i].direction+90,0,1,0);
		glRotatef(90,0,0,1);
		g_bind_t(GL_TEXTURE_2D, player_texture);
		draw_animation(&missile_model);
		glPopMatrix();
		draw_missile_target(gs,i,d);
	}
}

struct vector laser_point(struct game_state * gs, double d, int dir)
{
	int i;
	double drad = to_radians(gs->game_player.rotation_angle+
			dir_to_rot(dir));
	if(dir==1||dir==2){
		drad = to_radians(gs->game_player.rotation_angle);
	}

	struct vector v;
	struct vector a = gs->game_player.location;
	a.y+=1.2;
	double s = sin(drad);
	double c = cos(drad);
	for(i=30;i>1;i--){
		v.x=s;
		v.z=c;
		v.x*=i;
		v.z*=i;
		v=v_add(a,v);
		v.y=1.2;
		if(!wall_intersects_line(a,v)){
			v.x+=s;
			v.z+=c;
			return v;
		}
	}
	return v;
}

void draw_laser(struct game_state * gs, double d,int dir)
{
	struct vector color1 = {1,0,0};
	struct vector color2 = {1,0.5,0.5};
	struct vector a,b;
	a = gs->game_player.location;
	a.y=1.2;
	b = laser_point(gs,d,dir);

	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	draw_line(a,b,color1,32.0,1);
	a.y+=0.01;
	b.y+=0.01;
	draw_line(a,b,color2,16.0,1);
}

void draw_lasers(struct game_state * gs, double d)
{
	int i;
	struct item * item;
	for(i=0;i<4;i++){
		item = &(gs->game_player.inventory.item[i]);
		if((item->type==ITEM_LASER) && item->active
				&& item->duration>0.95){
			draw_laser(gs,d,i);
		}
	}
}

void draw_lightning(struct vector from, struct vector to)
{
	struct vector color = {0,1,1};
	int midpoints = 5;
	double fa;
	double dis = distance(from,to);
	struct vector dvec;
	struct vector last_dvec = from;
	int i;
	for(i=0;i<midpoints;i++){
		fa = face_angle(last_dvec,to);
		dvec = v_s_mul(dis/(midpoints+1),
				rot_to_dvec(fa+(rand()%30-15)));
		dvec = v_add(last_dvec,dvec);
		draw_line(last_dvec,dvec,color,5.0,1);
		last_dvec = dvec;
	}
	draw_line(last_dvec,to,color,5.0,1);
}

void draw_plasma_component(struct game_state * gs, double d, int dir)
{
	int i;
	struct vector n;
	double angle = gs->game_player.rotation_angle+dir_to_rot(dir);
	struct vector dvec = rot_to_dvec(angle);
	dvec.x*=0.8;
	dvec.z*=0.8;
	struct vector p = v_add(gs->game_player.location,dvec);
	p.y+=1.3;

	for(i=0;i<gs->n_npcs;i++){
		if(gs->npc[i].type==UNIT_TYPE_ITEM)
			continue;
		n = gs->npc[i].location;
		if(gs->npc[i].type!=UNIT_TYPE_SPIDER)
			n.y+=1.3;

		if((is_near(p,n,8)&&!wall_intersects_line(p,n))){
			draw_lightning(p,n);
		}
	}
}

void draw_plasma_components(struct game_state * gs, double d)
{
	int i;
	for(i=0;i<4;i++){
		if((gs->game_player.inventory.item[i].type==ITEM_PLASMA) &&
			(gs->game_player.inventory.item[i].active) &&
			(gs->game_player.inventory.item[i].cooldown <= 0.0)) {
				draw_plasma_component(gs,d,i);
			}
	}
}

void draw_debug_line(struct vector start, struct vector end, float width)
{
	struct vector c1 = (struct vector) {0,1,0};
	struct vector c2 = (struct vector) {0,0,1};
	draw_2line(start,end,c1,c2,1,1,width);
}

void draw_vector_layout(struct vector_layout *vl)
{
	int i;
	for(i=0;i<(vl->n_lines);i++){
		draw_debug_line(vl->line[i].a,vl->line[i].b,5.0);
	}
}

void draw_fire(struct particle * p)
{
	struct polygon a,b;
	float size = p->size.x;

	struct vector l,s;
	l = p->location;
	l.y+=1.2;

	glColor4f(1-(p->frame*3),1-(p->frame),1-(p->frame*0.5),1);

	glPushMatrix();
	glTranslatef(l.x,l.y,l.z);
	glScalef(0.3*p->size.x+p->frame,
			0.3*p->size.y+p->frame,
			0.3*p->size.z+p->frame);
	glRotated(-(rand()%45),1,0,0);

	draw_animation(&explosion_ball_model,0.1);

	glPopMatrix();
}

void draw_fires(struct game_state * gs,double d)
{
	int i;
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	for(i=0;i<MAX_FIRES;i++){
		if(gs->fire[i].alive){
			draw_fire(&gs->fire[i]);
		}
	}
}

void draw_models(struct game_state * gs,double d)
{
	//draw_vector_layout(&(world_map.current_room->vlayout));

	draw_room((world_map.current_room));
	int i;
	draw_player(&gs->game_player,d);
	for(i=0;i<gs->n_npcs;i++){
		draw_unit(gs,&gs->npc[i],d);
		if((gs->npc[i].hit_timer)>=0)
			gs->npc[i].hit_timer-=d;
	}
	if(world_map.current_room->has_shop)
		draw_shop(&(world_map.current_room->shop));
	draw_particles(gs,d);
	draw_missiles(gs,d);
	draw_lasers(gs,d);
	draw_bullets(gs,d);
	draw_fires(gs,d);
	draw_fields(gs,d);
	draw_plasma_components(gs,d);
}

void draw_exp_triangle(struct vector color)
{
	float alpha = 1.0;
	if(color.x==0.5){
		alpha = 0.5;
	}

	struct polygon a,b;
	a.v[0].p = (struct vector) {-1,0,0};
	a.v[1].p = (struct vector) {0.0,1,0};
	a.v[2].p = (struct vector) {1,0,0};

	b.v[0].p = (struct vector) {-1,0,0};
	b.v[1].p = (struct vector) {0,-1,0};
	b.v[2].p = (struct vector) {1,0,0};

	glBegin(GL_TRIANGLES);
	glColor4f(color.x,color.y,color.z,alpha);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
}

struct vector exp_bar_color(int layer, float ratio)
{
	struct vector c = {0x51/255.0,0x39/255.0,0x75/255.0};
	float multi = (ratio*layer*6)/5.0;
	struct vector ret = {0.5,0.5,0.5};

	if(ratio==1){
		c.x+=(rand()%10)/50.0;
		c.y+=(rand()%10)/50.0;
		c.z+=(rand()%10)/50.0;
		return c;
	}

	if(multi>1)
		return c;
	else
		return ret;
	return ret;
}

void draw_exp_bar(struct unit u, float x, float y)
{
	struct vector c = {0x31/255.0,0x39/255.0,0x75/255.0};
	float h = u.exp/((float)next_level(u.level));
	if(u.lvld)
		h=1.0;
	float r = 0.75;
	float s = 0.20;
	glPushMatrix();
	glTranslatef(x,y,1.2);

	glPushMatrix();
	glTranslatef(0,r,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(1,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(r/2,r/2,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(2,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-r/2,r/2,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(2,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-r,0,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(3,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(r,0,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(3,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,0,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(3,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(r/2,-r/2,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(4,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-r/2,-r/2,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(4,h));
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,-r,0);
	glScalef(s,s,s);
	draw_exp_triangle(exp_bar_color(5,h));
	glPopMatrix();

	glPopMatrix();
}

void draw_mana_bar(struct unit u, float x, float y)
{
	struct polygon a,b;
	float height = 1.0;
	struct vector c = {0x22/255.0,0x66/255.0,0x66/255.0};
	float slide = 1.0;
	double h = u.max_mana/40;

	a.v[0].p = (struct vector) {x-h+slide  ,y+0.0,0.1};
	a.v[1].p = (struct vector) {x+slide,y+0.0,0.1};
	a.v[2].p = (struct vector) {x-h-0.0  ,y+height,0.1};

	b.v[0].p = (struct vector) {x-h-0.0  ,y+height,0.1};
	b.v[1].p = (struct vector) {x-0.0,y+height,0.1};
	b.v[2].p = (struct vector) {x+slide,y+0.0,0.1};

	glBegin(GL_TRIANGLES);
	glColor4f(0.2,0.2,0.2,0.2);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();

	h = u.mana/40;
	a.v[0].p = (struct vector) {x-h+slide  ,y+0.0,0.0};
	a.v[1].p = (struct vector) {x+slide,y+0.0,0.0};
	a.v[2].p = (struct vector) {x-h-0.0  ,y+height,0.0};

	b.v[0].p = (struct vector) {x-h-0.0  ,y+height,0.0};
	b.v[1].p = (struct vector) {x-0.0,y+height,0.0};
	b.v[2].p = (struct vector) {x+slide,y+0.0,0.0};

	b.v[0].c = c;
	b.v[1].c = c;
	b.v[2].c = c;
	a.v[0].c = c;
	a.v[1].c = c;
	a.v[2].c = c;

	draw_poly(a);
	draw_poly(b);
}

void draw_unibar(struct unit * u , float x, float y)
{
	struct polygon a,b;
	float height = 1.0;
	struct vector c = {0x54/255.0,0x94/255.0,0x31/255};
	float slide = 1.0;
	double h = u->max_health/40;

	a.v[0].p = (struct vector) {x-h+slide  ,y+0.0,0.1};
	a.v[1].p = (struct vector) {x+slide,y+0.0,0.1};
	a.v[2].p = (struct vector) {x-h-0.0  ,y+height,0.1};

	b.v[0].p = (struct vector) {x-h-0.0  ,y+height,0.1};
	b.v[1].p = (struct vector) {x-0.0,y+height,0.1};
	b.v[2].p = (struct vector) {x+slide,y+0.0,0.1};

	glBegin(GL_TRIANGLES);
	glColor4f(0.2,0.2,0.2,0.2);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();

	h = u->health/40;
	a.v[0].p = (struct vector) {x-h+slide  ,y+0.0,0.0};
	a.v[1].p = (struct vector) {x+slide,y+0.0,0.0};
	a.v[2].p = (struct vector) {x-h-0.0  ,y+height,0.0};

	b.v[0].p = (struct vector) {x-h-0.0  ,y+height,0.0};
	b.v[1].p = (struct vector) {x-0.0,y+height,0.0};
	b.v[2].p = (struct vector) {x+slide,y+0.0,0.0};

	a.v[0].c = c;
	a.v[1].c = c;
	a.v[2].c = c;
	b.v[0].c = c;
	b.v[1].c = c;
	b.v[2].c = c;

	draw_poly(a);
	draw_poly(b);

	y+=1;
	h = u->max_health/40;

	a.v[0].p = (struct vector) {x-h  ,y+0.0,0.1};
	a.v[1].p = (struct vector) {x+0.0,y+0.0,0.1};
	a.v[2].p = (struct vector) {x-h+slide  ,y+height,0.1};

	b.v[0].p = (struct vector) {x-h+slide  ,y+height,0.1};
	b.v[1].p = (struct vector) {x+slide,y+height,0.1};
	b.v[2].p = (struct vector) {x+0.0,y+0.0,0.1};

	b.v[0].c = (struct vector) {0.2,0.2,0.2};
	b.v[1].c = (struct vector) {0.2,0.2,0.2};
	b.v[2].c = (struct vector) {0.2,0.2,0.2};
	a.v[0].c = (struct vector) {0.2,0.2,0.2};
	a.v[1].c = (struct vector) {0.2,0.2,0.2};
	a.v[2].c = (struct vector) {0.2,0.2,0.2};

	glBegin(GL_TRIANGLES);
	glColor4f(0.2,0.2,0.2,0.2);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();

	h = u->health/40;
	a.v[0].p = (struct vector) {x-h  ,y+0.0,0.0};
	a.v[1].p = (struct vector) {x+0.0,y+0.0,0.0};
	a.v[2].p = (struct vector) {x-h+slide  ,y+height,0.0};

	b.v[0].p = (struct vector) {x-h+slide  ,y+height,0.0};
	b.v[1].p = (struct vector) {x+slide,y+height,0.0};
	b.v[2].p = (struct vector) {x+0.0,y+0.0,0.0};

	b.v[0].c = c;
	b.v[1].c = c;
	b.v[2].c = c;
	a.v[0].c = c;
	a.v[1].c = c;
	a.v[2].c = c;

	draw_poly(a);
	draw_poly(b);
}


void draw_health_bar(struct unit u, float x, float y)
{
	struct polygon a,b;
	float height=1.0;
	float slide = 1.0;
	struct vector c = {0x54/255.0,0x94/255.0,0x31/255};
	if(u.health<30)
		c = (struct vector) {1,1,0};
	if(u.health<15)
		c = (struct vector) {1,0,0};
	if(u.health<0)
		return;
	double h = u.max_health/20;

	a.v[0].p = (struct vector) {x-h  ,y+0.0,0.1};
	a.v[1].p = (struct vector) {x+0.0,y+0.0,0.1};
	a.v[2].p = (struct vector) {x-h+slide  ,y+height,0.1};

	b.v[0].p = (struct vector) {x-h+slide  ,y+height,0.1};
	b.v[1].p = (struct vector) {x+slide,y+height,0.1};
	b.v[2].p = (struct vector) {x+0.0,y+0.0,0.1};

	b.v[0].c = (struct vector) {0.2,0.2,0.2};
	b.v[1].c = (struct vector) {0.2,0.2,0.2};
	b.v[2].c = (struct vector) {0.2,0.2,0.2};
	a.v[0].c = (struct vector) {0.2,0.2,0.2};
	a.v[1].c = (struct vector) {0.2,0.2,0.2};
	a.v[2].c = (struct vector) {0.2,0.2,0.2};

	glBegin(GL_TRIANGLES);
	glColor4f(0.2,0.2,0.2,0.2);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();

	h = u.health/20;
	a.v[0].p = (struct vector) {x-h  ,y+0.0,0.0};
	a.v[1].p = (struct vector) {x+0.0,y+0.0,0.0};
	a.v[2].p = (struct vector) {x-h+slide  ,y+height,0.0};

	b.v[0].p = (struct vector) {x-h+slide  ,y+height,0.0};
	b.v[1].p = (struct vector) {x+slide,y+height,0.0};
	b.v[2].p = (struct vector) {x+0.0,y+0.0,0.0};

	b.v[0].c = c;
	b.v[1].c = c;
	b.v[2].c = c;
	a.v[0].c = c;
	a.v[1].c = c;
	a.v[2].c = c;

	draw_poly(a);
	draw_poly(b);
}

void draw_coins(struct unit *u,double x, double y)
{
	int i;
	char str[30];

	sprintf(str,"$%i",u->coins);
	draw_text(x,y+1,str, (struct vector) {0.5,0.5,0.5});
}

void draw_level(struct unit *u,double x, double y)
{
	int i;
	char str[30];
	sprintf(str,"%i",u->level);
	draw_text(x,y-(i*FONT_HEIGHT),str, (struct vector) {0,1,1});
}

void draw_fps(double x, double y)
{
	char str[30];
	sprintf(str,"%i",(int)fps);
	draw_text(x,y,str,(struct vector) {0,1,1} );
}

void draw_text_field(struct text_field t)
{
	struct vector l = t.location;
	draw_text(l.x,l.y,t.text,t.color);
}

void draw_button(struct button button)
{
	struct polygon a,b;

	struct vector l,s;
	l = button.location;
	s = button.size;

	a.v[0].p = (struct vector) {l.x+s.x,l.y+0.0,0.1};
	a.v[1].p = (struct vector) {l.x,l.y,0.1};
	a.v[2].p = (struct vector) {l.x+s.x,l.y+s.y,0.1};

	b.v[0].p = (struct vector) {l.x+s.x,l.y+s.y,0.1};
	b.v[1].p = (struct vector) {l.x,l.y,0.1};
	b.v[2].p = (struct vector) {l.x+0.0,l.y+s.y,0.1};

	b.v[0].c = button.color;
	b.v[1].c = button.color;
	b.v[2].c = button.color;
	a.v[0].c = button.color;
	a.v[1].c = button.color;
	a.v[2].c = button.color;

	a.v[0].n = (struct vector) {0,0,1};
	a.v[1].n = (struct vector) {0,0,1};
	a.v[2].n = (struct vector) {0,0,1};

	b.v[0].n = (struct vector) {0,0,1};
	b.v[1].n = (struct vector) {0,0,1};
	b.v[2].n = (struct vector) {0,0,1};

	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	draw_poly(a);
	draw_poly(b);

	draw_text(l.x+(s.x-0.7),l.y+0.55,button.text,(struct vector) {0,0,0} );
}

void draw_box(struct vector l,struct vector color,float alpha)
{
	glPushMatrix();
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	glEnable(GL_BLEND);
	struct polygon a,b;

	struct vector s = (struct vector) {0.4,0.4,0};

	a.v[0].p = (struct vector) {s.x ,-s.y,0.2};
	a.v[1].p = (struct vector) {-s.x,-s.y,0.2};
	a.v[2].p = (struct vector) {s.x ,s.y,0.2};

	b.v[0].p = (struct vector) {s.x,s.y,0.2};
	b.v[1].p = (struct vector) {-s.x,-s.y,0.2};
	b.v[2].p = (struct vector) {-s.x,s.y,0.2};

	a.v[0].n = normal(a.v[0].p);
	a.v[1].n = normal(a.v[1].p);
	a.v[2].n = normal(a.v[2].p);

	b.v[0].n = normal(b.v[0].p);
	b.v[1].n = normal(b.v[1].p);
	b.v[2].n = normal(b.v[2].p);

	glTranslatef(l.x,l.y,l.z);
	glBegin(GL_TRIANGLES);
	glColor4f(color.x,color.y,color.z,alpha);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glDisable(GL_BLEND);
	glPopMatrix();
}

float carrot_timer=0;
void draw_carrot(struct vector location)
{
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	struct polygon a,b;

	struct vector l,s;
	l = location;

	s = (struct vector) {FONT_WIDTH,FONT_HEIGHT,1.0};
	l.x-=(s.x/2);
	l.y-=(s.y/2);

	a.v[0].p = (struct vector) {l.x+s.x,l.y+0.0,l.z};
	a.v[1].p = l;
	a.v[2].p = (struct vector) {l.x+s.x,l.y+s.y,l.z};

	b.v[0].p = (struct vector) {l.x+s.x,l.y+s.y,l.z};
	b.v[1].p = l;
	b.v[2].p = (struct vector) {l.x+0.0,l.y+s.y,l.z};

	glColor4f(1,1,1,1+sin(carrot_timer*5));
	glBegin(GL_TRIANGLES);
	glTexCoord2f(0.5,0.5);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
}

void draw_letter(struct vector location,int c)
{
	struct polygon a,b;

	struct vector l,s;
	l = location;

	s = (struct vector) {FONT_WIDTH,FONT_HEIGHT,1.0};
	l.x-=(s.x/2);
	l.y-=(s.y/2);

	a.v[0].p = (struct vector) {l.x+s.x,l.y+0.0,l.z};
	a.v[1].p = l;
	a.v[2].p = (struct vector) {l.x+s.x,l.y+s.y,l.z};

	b.v[0].p = (struct vector) {l.x+s.x,l.y+s.y,l.z};
	b.v[1].p = l;
	b.v[2].p = (struct vector) {l.x+0.0,l.y+s.y,l.z};

	double w = 1/95.1;
	float n = w*(c-31);

	glBegin(GL_TRIANGLES);
	glTexCoord2f(n-w,1.0f);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(n,1.0f);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(n-w,0.0f);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(n-w,0.0f);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(n,1.0f);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(n,0.0f);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
}

void draw_card(struct game_state * gs, struct card * c,struct vector l)
{
	glPushMatrix();
	g_bind_t(GL_TEXTURE_2D, perk_texture);
	struct polygon a,b;

	struct vector s;

	s = (struct vector) {3,3,1.0};

	a.v[0].p = (struct vector) {s.x ,-s.y,0};
	a.v[1].p = (struct vector) {-s.x,-s.y,0};
	a.v[2].p = (struct vector) {s.x ,s.y,0};

	b.v[0].p = (struct vector) {s.x,s.y,0};
	b.v[1].p = (struct vector) {-s.x,-s.y,0};
	b.v[2].p = (struct vector) {-s.x,s.y,0};

	float w;
	if(c->type>=1)
		w = (1/15.0)*(1+c->type);
	else
		w = (1/15.0);
	float n = -(1/15.0);


	glTranslatef(l.x,l.y,l.z);
	glBegin(GL_TRIANGLES);
	glTexCoord2f(1.0,w);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(0.0,w);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(1.0,w+n);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(1.0,w+n);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(0.0,w);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(0.0,w+n);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();
}

/* to give cards that silky smooth transition the ladies love */
float floating_selected_card=0.0f;

void draw_cards_hud(struct game_state * gs,double d)
{
	struct vector a,b;
	struct vector v = pixel_to_screen(pi.mouse_x,pi.mouse_y);
	int selected_card = (v.x<0)+1;

	if(floating_selected_card > -1.0 && selected_card==1)
		floating_selected_card-=d*8;
	if(floating_selected_card < 1.0 && selected_card==2)
		floating_selected_card+=d*8;

	if(floating_selected_card>1.0)
		floating_selected_card=1;
	if(floating_selected_card<-1.0)
		floating_selected_card=-1;

	float fsc = floating_selected_card*0.05;

	glEnable(GL_BLEND);
	draw_text(2,6,"PICK-A-PERK",(struct vector) {0,1,1} );
	draw_text(6,4,gs->card1.name,(struct vector){1,1,1});
	draw_text(-4,4,gs->card2.name,(struct vector){1,1,1});

	glPushMatrix();
	glScalef(0.7,0.7,0.7);
	if(selected_card==1)
		draw_text(9,-7,gs->card1.description,(struct vector){1,1,1});
	else
		draw_text(9,-7,gs->card2.description,(struct vector){1,1,1});
	glPopMatrix();

	a = (struct vector) {4,0,5};
	b = (struct vector) {-4,0,5};

	glPushMatrix();
	glScalef(1.0-fsc,1.0-fsc,1.0-fsc);
	glColor4f(1.0-fsc*3,1.0-fsc*3,1.0-fsc*3,1.0-fsc*3);
	draw_card(gs,&gs->card1,a);
	glPopMatrix();

	glPushMatrix();
	glScalef(1.0+fsc,1.0+fsc,1.0+fsc);
	glColor4f(1.0+fsc*3,1.0+fsc*3,1.0+fsc*3,1.0+fsc*3);
	draw_card(gs,&gs->card2,b);
	glPopMatrix();
}


void draw_ui(struct ui * ui)
{
	if(post_processing)
		return;
	int i;
	for(i=0;i<ui->n_buttons;i++){
		draw_button(ui->button[i]);
	}
	for(i=0;i<ui->n_text_fields;i++){
		draw_text_field(ui->text_field[i]);
	}
}


void add_damage_number(struct vector location, float amount)
{
	int r = rand()%MAX_DAMAGE_NUMBERS;
	damage_number[r].amount=amount;
	damage_number[r].location=location;
	damage_number[r].location.x+=((rand()%10)-10)/10.0;
	damage_number[r].location.z+=((rand()%10)-10)/10.0;
	damage_number[r].frame=0;
	damage_number[r].alive=1;
}

void draw_prices(struct game_state * gs, struct shop * shop)
{
	int i;
	struct vector v;
	char price[10];
	for(i=0;i<MAX_TRANSACTIONS;i++){
		if(!shop->t[i].sold){
			sprintf(price,"%i",shop->t[i].price);
		}else{
			sprintf(price,"SOLD");
		}
		v=world_to_screen(gs,shop->t[i].location);
		draw_text(v.x,v.y,price,(struct vector) {1.0,0.7,0.0});
	}

	v=world_to_screen(gs,shop->keeper_location);
	v.y++;
	draw_text(v.x,v.y,shop->keeper_line,(struct vector) {1.0,0.7,0.0});
}

void draw_speeches(struct game_state * gs, double d)
{
	draw_unit_speech(gs,&gs->game_player,d);
	int i;
	for(i=0;i<(gs->n_npcs);i++){
		draw_unit_speech(gs,&gs->npc[i],d);
	}
}

void draw_frame_box(float w, float h,float depth)
{
	w*=2;
	h*=2;
	glPushMatrix();
	glLoadIdentity();
	struct polygon a,b;

	struct vector l,s;
	l = (struct vector) {0,0,0};
	l.z++;

	s = (struct vector) {w,h,1.0};
	l.x-=(s.x/2);
	l.y-=(s.y/2);

	a.v[0].p = (struct vector) {s.x,0.0,0};
	a.v[1].p = (struct vector) {0,0,0};
	a.v[2].p = (struct vector) {s.x,s.y,0};
	b.v[0].p = (struct vector) {s.x,s.y,0};
	b.v[1].p = (struct vector) {0,0,0};
	b.v[2].p = (struct vector) {0.0,s.y,0};

	glTranslatef(l.x,l.y,l.z-depth-100);

	glBegin(GL_TRIANGLES);
	glTexCoord2f(1,0);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(0,0);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(1,1);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(1,1);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(0,0);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(0,1);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();

}

void draw_background()
{
	glPushMatrix();
	g_bind_t(GL_TEXTURE_2D, space_texture);
	struct polygon a,b;

	struct vector l,s;
	l = (struct vector) {0,0,0};
	l.z++;

	s = (struct vector) {20,20,1.0};
	l.x-=(s.x/2);
	l.y-=(s.y/2);

	a.v[0].p = (struct vector) {s.x,0.0,0};
	a.v[1].p = (struct vector) {0,0,0};
	a.v[2].p = (struct vector) {s.x,s.y,0};
	b.v[0].p = (struct vector) {s.x,s.y,0};
	b.v[1].p = (struct vector) {0,0,0};
	b.v[2].p = (struct vector) {0.0,s.y,0};

	glColor3f( 1, 1, 1);
	glTranslatef(l.x,l.y,l.z-100);
	/*
	   glRotated(-45,1,0,0);
	   glRotated(45,0,1,0);
	 */

	glBegin(GL_TRIANGLES);
	glTexCoord2f(0,1);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(0,0);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(1,1);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(1,1);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(0,0);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(1,0);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();
}

void draw_explosion(struct particle * p)
{
	struct polygon a,b;
	float size = p->size.x;

	struct vector l,s;
	l = p->location;

	s = (struct vector) {2,2,1.0};
	l.x-=(s.x/2);
	l.y-=(s.y/2);

	if(size>1.0){//fire
		glColor4f( size, size-0.50, size-1,size-(p->frame*2));
	} else {//smoke
		glColor4f( 0.5, 0.5, 0.5, 1-(p->frame*6));
	}

	glPushMatrix();
	glTranslatef(l.x,l.y+1.4,l.z);
	glScalef(0.4*p->size.x+p->frame,
			0.4*p->size.y+p->frame,
			0.4*p->size.z+p->frame);
	glRotated(-(rand()%45),1,0,0);

	draw_animation(&explosion_ball_model,0.1);

	glPopMatrix();
}


void split_explosion(struct particle * p)
{
	int i;
	int rando;
	for(i=0;i<2;i++){
		rando = rand()%MAX_EXPLOSIONS;
		explosion[rando].location = p->location;
		explosion[rando].direction = random_direction();
		explosion[rando].size = p->size;
		explosion[rando].size.x /= 1.3+(0.5*(rand()%2));
		explosion[rando].size.y /= 1.3+(0.5*(rand()%2));
		explosion[rando].size.z /= 1.3+(0.5*(rand()%2));
		if(explosion[rando].size.x<1.0)
			return;
		explosion[rando].frame = 0;
		explosion[rando].alive = 1;
	}
}

void draw_explosions(struct game_state * gs,double d)
{
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	int i;
	d*=1.2;
	for(i=0;i<MAX_EXPLOSIONS;i++){
		if(explosion[i].alive){
			explosion[i].frame += d;
			if(explosion[i].size.x<1){
				explosion[i].frame-=d/1.2;
				explosion[i].direction.y+=0.1;
			}
			explosion[i].location.x+=2.4*d*explosion[i].direction.x;
			explosion[i].location.y+=2.4*d*explosion[i].direction.y;
			explosion[i].location.z+=2.4*d*explosion[i].direction.z;
			draw_explosion(&explosion[i]);
			if(explosion[i].frame>((rand()%3)/10.0)){
				split_explosion(&explosion[i]);
			}
		}
		if(explosion[i].frame>=0.3){
			explosion[i].alive=0;
			explosion[i].frame=0;
		}
	}
}

void draw_spark(struct particle * p)
{
	struct vector v = p->location;
	struct vector w = p->location;
	struct vector color = {1,1,0};
	v.x += p->direction.x*0.5;
	v.y += 0.5;
	v.z += p->direction.z*0.5;
	w.y+=0.5;
	draw_line(w,v,(struct vector){1,1,1},5.0-((p->frame)*5),1);
}

void draw_sparks(struct game_state * gs, double d)
{
	int i;
	d*=2;
	for(i=0;i<MAX_SPARKS;i++){
		if(spark[i].alive){
			draw_spark(&spark[i]);
			spark[i].frame+=d*2;
			spark[i].location.x+=spark[i].direction.x*d*5;
			spark[i].location.z+=spark[i].direction.z*d*5;
			if(spark[i].frame>=1){
				spark[i].alive=0;
				spark[i].frame=0;
			}
		}
	}
}

void draw_trail(struct particle * p)
{
	struct vector v = p->location;
	struct vector w = p->location;
	v.x += p->direction.x;
	v.y += 1.0;
	v.z += p->direction.z;
	w.y+=1.0;

	draw_line(w,v,p->color,20,1.0-(p->frame*2));
}

void draw_porticle(struct particle * p)
{
	struct vector starting = {0.5,0.1,0.5};
	struct vector ending = {0.5,0.1,0.1};
	glPushMatrix();
	glTranslatef(p->location.x,p->location.y,p->location.z);
	glRotatef((p->type*15)-(p->frame*720),0,1,0);

	draw_line(starting,ending,p->color,5.0,1-(p->frame*3));

	glPopMatrix();
}

void draw_trails(struct game_state * gs, double d)
{
	int i;
	for(i=0;i<MAX_TRAILS;i++){
		if(trail[i].alive){
			draw_trail(&trail[i]);
			trail[i].frame+=d*2;
			if(trail[i].frame>=(1.0/3.0)){
				trail[i].alive=0;
				trail[i].frame=0;
			}
		}
	}
}

void draw_porticles(struct game_state * gs, double d)
{
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	int i;
	for(i=0;i<MAX_TRAILS;i++){
		if(porticle[i].alive){
			porticle[i].location = v_add(porticle[i].location,
					porticle[i].direction);
			draw_porticle(&porticle[i]);
			porticle[i].frame+=d;
			if(porticle[i].frame>=0.4){
				porticle[i].alive=0;
				porticle[i].frame=0;
			}
		}
	}
}

void draw_muzzle_flash(struct particle * m)
{
	glPushMatrix();
	glTranslatef(m->location.x,m->location.y,m->location.z);
	glScalef(0.3,0.3,0.3);
	draw_animation(&explosion_ball_model,0.0);
	glPopMatrix();
}

void draw_muzzle_flashes(struct game_state * gs, double d){
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	glColor4f(1,1,1,1);
	int i;
	for(i=0;i<MAX_MUZZLE;i++){
		if(muzzle[i].alive){
			draw_muzzle_flash(&muzzle[i]);
			muzzle[i].alive--;
			muzzle[i].frame=0;
		}
	}
}

void draw_bullet_hole(struct particle * p)
{
	glPushMatrix();
	g_bind_t(GL_TEXTURE_2D, bullet_hole_texture);
	struct polygon a,b;

	struct vector l,s;
	l = p->location;

	s = p->direction;
	s.x/=2;s.z/=2;

	a.v[0].p = (struct vector) {s.x,0.0,s.z};
	a.v[1].p = (struct vector) {-s.x,0,-s.z};
	a.v[2].p = (struct vector) {s.x,s.y,s.z};
	b.v[0].p = (struct vector) {s.x,s.y,s.z};
	b.v[1].p = (struct vector) {-s.x,0,-s.z};
	b.v[2].p = (struct vector) {-s.x,s.y,-s.z};

	float w = 1;
	float n = -1;

	float h = (1/4.0)*p->type;
	float k = -(1/4.0);

	glColor3f( 1, 1, 1);
	glTranslatef(l.x,l.y+0.2,l.z);

	glBegin(GL_TRIANGLES);
	glTexCoord2f(w,h);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(w,h-k);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(w+n,h);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(w+n,h);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(w,h-k);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(w+n,h-k);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();
}

void draw_splat(struct particle * p)
{
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, splat_texture);
	struct polygon a,b;
	glColor4f( 0.8, 0, 0,1);

	struct vector l,s;
	l = p->location;

	s = (struct vector){0.4,0,0.4};

	a.v[0].p = (struct vector) {s.x,s.y,s.z};
	a.v[1].p = (struct vector) {s.x,s.y,-s.z};
	a.v[2].p = (struct vector) {-s.x,s.y,-s.z};

	b.v[0].p = (struct vector) {s.x,s.y,s.z};
	b.v[1].p = (struct vector) {-s.x,s.y,s.z};
	b.v[2].p = (struct vector) {-s.x,s.y,-s.z};

	glTranslatef(l.x,l.y,l.z);

	if(post_processing){
		glScalef(0.8,0.8,0.8);
		glColor4f(1,0.0,0.0,1/(p->frame));
	}

	glBegin(GL_TRIANGLES);
	glTexCoord2f(1,1);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(1,0);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(0,0);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(1,1);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(0,1);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(0,0);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();
}

void draw_scorch(struct particle * p)
{
	glBindTexture(GL_TEXTURE_2D, scorch_texture);
	struct polygon a,b;
	if(post_processing)
		return;
	glPushMatrix();

	glColor4f( 1, 1, 1,1);

	struct vector l,s;
	l = p->location;

	s = (struct vector){1,0,1};

	a.v[0].p = (struct vector) {s.x,s.y,s.z};
	a.v[1].p = (struct vector) {s.x,s.y,-s.z};
	a.v[2].p = (struct vector) {-s.x,s.y,-s.z};

	b.v[0].p = (struct vector) {s.x,s.y,s.z};
	b.v[1].p = (struct vector) {-s.x,s.y,s.z};
	b.v[2].p = (struct vector) {-s.x,s.y,-s.z};

	glTranslatef(l.x,l.y+0.1,l.z);

	glBegin(GL_TRIANGLES);
	glTexCoord2f(1,1);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glTexCoord2f(1,0);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glTexCoord2f(0,0);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glTexCoord2f(1,1);
	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glTexCoord2f(0,1);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glTexCoord2f(0,0);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glPopMatrix();
}


void draw_sprites(struct game_state * gs, double d)
{
	struct room * r = world_map.current_room;

	int i;
	for(i=0;i<MAX_PERMANENTS;i++){
		if(!r->permanents[i].alive)
			continue;
		r->permanents[i].frame+=d;
		if(r->permanents[i].kind==PKIND_BULLET){
			draw_bullet_hole(&r->permanents[i]);
		}
		if(r->permanents[i].kind==PKIND_SPLAT){
			draw_splat(&r->permanents[i]);
		}
		if(r->permanents[i].kind==PKIND_SCORCH){
			draw_scorch(&r->permanents[i]);
		}
	}
}

void draw_chunk(struct particle * p)
{
	glPushMatrix();

	if(p->type==CHUNK_DOT){
		g_bind_t(GL_TEXTURE_2D,whitey_texture);
		glColor4f(1,0,0,1);
		if(post_processing)
			glColor4f(1,0,0,1/p->frame);
		glTranslatef(p->location.x,p->location.y,p->location.z);
		glScalef(0.1,0.1,0.1);
		draw_animation(&explosion_ball_model,0.0);
	}

	if(p->type==CHUNK_BIT){
		g_bind_t(GL_TEXTURE_2D, player_texture);
		glColor4f(1,1,1,1);
		glTranslatef(p->location.x,p->location.y,p->location.z);
		glScalef(0.1,0.1,0.1);
		draw_vbo_mesh(&template_chunk.mesh[0]);
	}

	glPopMatrix();
}

void chunk_tick(struct particle * chunk,double d)
{
	struct room * r = world_map.current_room;
	float yoff = 0.2;
	int rando;
	d*=5;
	if(chunk->location.y<=yoff){
		chunk->location.y=yoff;
		if(!(rand()%40)){
			if((chunk->location.x<0 ||
					chunk->location.x>MAX_ROOM_WIDTH)
			||(chunk->location.z<0 ||
				       	chunk->location.z>MAX_ROOM_HEIGHT)
			||(isnan(chunk->location.x)|| isnan(chunk->location.z))
			||(!walkable((int)chunk->location.x,
						(int)chunk->location.z)))
			{
				chunk->alive=0;
				return;
			}
		}
		if(chunk->type == CHUNK_DOT)
		{
			rando=rand()%MAX_PERMANENTS;
			r->permanents[rando].location = (chunk->location);
			r->permanents[rando].location.y =
				((rand()%1000)/10000.0);
			r->permanents[rando].type = PKIND_SPLAT;
			r->permanents[rando].kind = PKIND_SPLAT;
			r->permanents[rando].alive=1;
			r->permanents[rando].frame=0;
			chunk->alive=0;
		}
	}else{
		chunk->direction.y-=d;
		chunk->location.x += chunk->direction.x*d;
		chunk->location.z += chunk->direction.z*d;
		chunk->location.y += chunk->direction.y*d;
		if(chunk->location.y<yoff)
			play_chips_sound_effect();
	}
}

void draw_chunks(struct game_state * gs, double d)
{
	struct room * r = world_map.current_room;

	int i;
	for(i=0;i<MAX_CHUNKS;i++){
		if(!r->chunks[i].alive)
			continue;
		r->chunks[i].frame+=d;
		chunk_tick(&r->chunks[i],d);
		draw_chunk(&r->chunks[i]);
	}
}

void draw_damage_numbers(struct game_state * gs, double d)
{
	int i;
	struct vector v;
	char string[10];
	for(i=0;i<MAX_DAMAGE_NUMBERS;i++){
		if(!damage_number[i].alive)
			continue;
		damage_number[i].frame+=d;
		if(damage_number[i].frame>0.5)
			damage_number[i].alive=0;
		sprintf(string,"%5.0f",damage_number[i].amount);
		v=world_to_screen(gs,damage_number[i].location);
		v.y+=damage_number[i].frame*2;
		v.x+=2;
		draw_text(v.x,v.y,string,(struct vector) {1.0,0.7,0.0});
	}
}

void draw_particles(struct game_state * gs, double d)
{
	glEnable(GL_BLEND);

	draw_explosions(gs,d);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	draw_trails(gs,d);
	draw_porticles(gs,d);
	draw_sparks(gs,d);
	draw_muzzle_flashes(gs,d);
	draw_chunks(gs,d);
	draw_sprites(gs,d);
	glDisable(GL_BLEND);
}

struct vector component_box_color(int i)
{
	struct vector c,m;
	m = pixel_to_screen(pi.mouse_x,pi.mouse_y);
	c.x = 0.0;
	c.y = 0.5;
	c.z = 0.5;

	if(i==1 && pi.shift_key)
		c = (struct vector) {1,1,1};
	if(i==2 && pi.right_click)
		c = (struct vector) {1,1,1};
	if(i==3 && pi.left_click)
		c = (struct vector) {1,1,1};
	if(i==4 && pi.keys[' '])
		c = (struct vector) {1,1,1};

	if(is_near(m,box_point[i-1],0.5))
		c.z+=5;
	return c;
}

void draw_clock_piece(struct vector color)
{
	glBegin(GL_TRIANGLES);
	glColor4f(color.x,color.y,color.z,1.0);

	glVertex3f(-0.20,0.0,+1.0);
	glVertex3f(0.0,0.3,+1.0);
	glVertex3f(0.20,0.0,+1.0);

	glEnd();
}

void draw_corner(struct vector color, struct item * item)
{
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);
	glEnable(GL_BLEND);
	float a=-0.2,b=0.0,c=0.2;
	glBegin(GL_TRIANGLES);
	glColor4f(color.x,color.y,color.z,1.0);

	glVertex3f(a,a,+1.0);
	glVertex3f(a,c,+1.0);
	glVertex3f(b,c,+1.0);

	glVertex3f(a,a,+1.0);
	glVertex3f(b,a,+1.0);
	glVertex3f(b,c,+1.0);

	glVertex3f(b,a,+1.0);
	glVertex3f(b,b,+1.0);
	glVertex3f(c,b,+1.0);

	glVertex3f(b,a,+1.0);
	glVertex3f(c,a,+1.0);
	glVertex3f(c,b,+1.0);

	glEnd();
	glDisable(GL_BLEND);
}

void draw_corners(struct vector color , struct item * item)
{
	float r;
	if(item->type)
		r=0.8;
	else
		r=0.2;
	glPushMatrix();
	glScalef(0.5,0.5,0.5);

	glPushMatrix();
	glTranslatef(0,-r,0);
	glRotatef(45,0,0,1);
	draw_corner(color,item);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(r,0,0);
	glRotatef(135,0,0,1);
	draw_corner(color,item);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,r,0);
	glRotatef(225,0,0,1);
	draw_corner(color,item);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-r,0,0);
	glRotatef(315,0,0,1);
	draw_corner(color,item);
	glPopMatrix();

	glPopMatrix();
}

void draw_corner_clock(struct vector color, struct item * item)
{
	float r = 360*(item->cooldown/item->max_cooldown);
	glPushMatrix();
	glScalef(0.5,0.5,0.5);
	glPushMatrix();
	glRotatef(r,0,0,1);
	glTranslatef(0,0.7,0);
	draw_clock_piece(color);
	glPopMatrix();
	glPopMatrix();
}

void draw_component_box(struct vector location, struct vector color,
		struct item * item)
{
	glPushMatrix();
	glTranslatef(location.x,location.y,location.z);
	if(item->cooldown<=0)
		draw_corners(color,item);
	else
		draw_corner_clock(color,item);
	glPopMatrix();
}

void draw_components(struct unit u, int x, int y)
{
	struct vector m = pixel_to_screen(pi.mouse_x,pi.mouse_y);

	int i;
	for(i=0;i<4;i++){
		draw_component_box(box_point[i],
				component_box_color(i+1),
				&u.inventory.item[i]);
	}
	for(i=0;i<4;i++){
		if(u.inventory.item[i].dragging){
			draw_hud_item(&u.inventory.item[i],m,i);
		}else{
			draw_hud_item(&u.inventory.item[i], box_point[i],i);
		}
	}

}

void draw_hud(struct game_state * gs,double d)
{
	glUseProgram(shader_program);
	glEnable(GL_BLEND);
	glPushMatrix();
	gluLookAt(0,0,0, 0,0,1, 0,1,0);
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	glTexCoord2f(0.5,0.5);

	if(gs->game_player.lvling && !paused){
		draw_cards_hud(gs,d);
		glDisable(GL_BLEND);
		glPopMatrix();
		return;
	}

	if(gs->game_player.flags&HAS_UNIBAR){
		draw_unibar(&gs->game_player,6.9,-7.0);
	}else{
		draw_mana_bar(gs->game_player,7,-7.1);
		draw_health_bar(gs->game_player,7,-5.9);
	}
	draw_exp_bar(gs->game_player,8.1,-6);

	draw_coins(&gs->game_player,8,-5.5);
	draw_fps(9,7);

	if(paused){
		if(is_game_over)
			draw_ui(&gameover_ui);
		else
			draw_ui(ui);
	}

	if(world_map.current_room->has_shop)
		draw_prices(gs,&world_map.current_room->shop);

	draw_damage_numbers(gs,d);

	draw_speeches(gs,d);

	if(gs->game_player.lvld)
		draw_text(8,-4,"Press ~ to level up",(struct vector) {0,1,1});

	draw_components(gs->game_player,-7,-5);

	glPopMatrix();
	glDisable(GL_BLEND);
}

void game_camera(struct game_state * gs, double d)
{
	glScalef(0.8,0.6*frame_ratio,0.8);
	int zoom = 15;
	d*=8;
	if(gs->game_player.dash_timer>0.0)
		d*=0.5;
	struct vector current_camera;
	struct vector pmv = pixel_to_screen(pi.mouse_x,pi.mouse_y);
	target_camera = gs->game_player.location;
	float mfd=4.0;

	target_camera.z+=pmv.x/mfd;
	target_camera.x-=pmv.x/mfd;

	target_camera.z-=pmv.y/mfd;
	target_camera.x-=pmv.y/mfd;

	current_camera = last_camera;

	current_camera.x += (d * (target_camera.x - last_camera.x));
	current_camera.y += (d * (target_camera.y - last_camera.y));
	current_camera.z += (d * (target_camera.z - last_camera.z));

	gluLookAt(      current_camera.x+zoom,
			current_camera.y+zoom,
			current_camera.z+zoom,
			current_camera.x,
			current_camera.y,
			current_camera.z,
			0,1,0);
	last_camera=current_camera;
}

void update_mouse_light(struct game_state * gs, double d)
{
	struct vector gl = screen_to_world(gs,pi.mouse_x,pi.mouse_y);
	float loc[4];
	float diffuse[4];

	loc[0]=gl.x;
	loc[1]=gl.y+3.0;
	loc[2]=gl.z;
	loc[3]=1;
	diffuse[0]=0.0;
	diffuse[1]=0.5;
	diffuse[2]=0.5;
	diffuse[3]=1.0;
	glLightfv(GL_LIGHT0,GL_POSITION,loc);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuse);
}

void light_tick(struct light * light,double delta)
{
	struct vector v,dv;
	double dist = distance(light->location,light->origin);
	if((distance((struct vector){0,0,0},light->velocity)<0.01)&&
			(dist<0.01)){
		light->velocity = (struct vector) {0,0,0};
		light->location = light->origin;
		return;
	}
	if(!v_eq(light->location,light->origin)){
		dv = rot_to_dvec(face_angle(light->location,light->origin));
		dv = v_s_mul((dist),dv);
		light->velocity = v_add(light->velocity,dv);
		light->velocity = v_s_mul(1-delta,light->velocity);
	}
	v = v_s_mul(delta,light->velocity);
	light->location = v_add(light->location,v);
	light->location.y=3;
}

void tick_lights(struct game_state * gs, double delta)
{
	struct room * room = world_map.current_room;
	struct light * light;
	struct vector dv;
	int i;
	for(i=0;i<MAX_ROOM_LIGHTS;i++){
		light = &(room->light[i]);
		light_tick(light,delta);
	}
}

void draw_room_lights(struct game_state * gs, double delta){
	struct room * room = world_map.current_room;
	float loc[4];
	float diffuse[4];

	tick_lights(gs,delta);

	float yoff;

	int i;
	for(i=0;i<4;i++){
		yoff = distance(room->light[i].location,room->light[i].origin);
		loc[0]=room->light[i].location.x;
		loc[1]=room->light[i].location.y+yoff;
		loc[2]=room->light[i].location.z;
		loc[3]=1.0;
		diffuse[0]=room->light[i].color.x;
		diffuse[1]=room->light[i].color.y;
		diffuse[2]=room->light[i].color.z;
		diffuse[3]=1.0;
		glLightfv(GL_LIGHT0+i,GL_POSITION,loc);
		glLightfv(GL_LIGHT0+i,GL_DIFFUSE,diffuse);
	}
}

void draw_game(struct game_state * gs,double d)
{
	g_bind_t(GL_TEXTURE_2D, whitey_texture);
	if(!post_processing){
		glUseProgram(lighting_shader_program);
	}else{
		glUseProgram(shader_program);
	}

	glPushMatrix();
	//draw_background();
	game_camera(gs,d);
	draw_models(gs,d);
	draw_room_lights(gs,d);
	glPopMatrix();
}

#define MAX_STARS 100

struct vector menu_star[MAX_STARS];
int stars_generated=0;

void generate_stars()
{
	int i;
	for(i=0;i<MAX_STARS;i++){
		menu_star[i] = (struct vector)
		{((rand()%1000)/50.0)-10,((rand()%1000)/50.0)-10,100};
	}
	stars_generated=1;
}

void draw_stars()
{
	float x=0.05;
	float y=0.05;
	if(!stars_generated)
		generate_stars();

	struct vector color = (struct vector) {1,1,1};
	int i;

	glPointSize(1.0);
	glBegin(GL_POINTS);
	for(i=0;i<MAX_STARS;i++){
		struct vector a = menu_star[i];
		glColor4f(1,1,1,1);
		glVertex3f(a.x, a.y, a.z);
	}
	glEnd();
}

struct vector get_selection_v(struct vector starting){
	struct vector v = starting;
	v.z++;
	struct vector pmv = pixel_to_screen(pi.mouse_x,pi.mouse_y);

	if(pmv.y<2.5 && pmv.y > -1.5)
		v.y=pmv.y;
	else
		v.y=2;
	return v;
}

void draw_base_frame(struct game_state * gs, double d)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	frame_x=10;
	frame_y=10/1.333333;
	glOrtho(-frame_x,frame_x,-frame_y,frame_y,0.0f,1000.0f);
	//	glFrustum(-1.0,1.0,-1.0f,1.0f,1.0f,100.0f);
	glMatrixMode(GL_MODELVIEW);

	glUseProgram(shader_program);

	draw_game(gs,d);
}

void draw_frames(double d)
{
	glEnable(GL_BLEND);
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	frame_x=10;
	frame_y=10/1.333333;

	if(planet_impact_timer>-0.000001){
		glColor4f( 1.0, 1.0, 1,1.0);
		glBindTexture(GL_TEXTURE_2D, base_ft.texture_id);
		glPushMatrix();
		transition_effect(d);
		draw_frame_box(frame_x,frame_y,1.0);
		glPopMatrix();

		glColor4f( 1.0, 1.0, 1.0,1.0);
		glBindTexture(GL_TEXTURE_2D, post_ft.texture_id);
		glUseProgram(blur_shader_program);
		draw_frame_box(frame_x,frame_y,0.0);
	}

	glColor4f( 1.0, 1.0, 1,1.0);
	glBindTexture(GL_TEXTURE_2D, hud_ft.texture_id);
	glUseProgram(tv_shader_program);
	draw_frame_box(frame_x,frame_y,-1.0);
}

void draw_post_frame(struct game_state * gs, double d)
{
	draw_base_frame(gs,0);
}

void partial_clear()
{
	glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
	glBlendFuncSeparate(GL_ZERO,GL_ONE,GL_ONE,GL_ONE);
	glBindTexture(GL_TEXTURE_2D,whitey_texture);
	glColor4f( 0.1, 0.1, 0.1,0.3);

	double w=100;
	double h=100;
	glEnable(GL_BLEND);
	struct polygon a,b;
	struct vector l,s;
	l = (struct vector) {0,0,0};
	l.z++;

	s = (struct vector) {w,h,1.0};
	l.x-=(s.x/2);
	l.y-=(s.y/2);

	a.v[0].p = (struct vector) {s.x,-s.y,0};
	a.v[1].p = (struct vector) {-s.x,-s.y,0};
	a.v[2].p = (struct vector) {s.x,s.y,0};
	b.v[0].p = (struct vector) {s.x,s.y,0};
	b.v[1].p = (struct vector) {-s.x,-s.y,0};
	b.v[2].p = (struct vector) {-s.x,s.y,0};

	glDisable(GL_TEXTURE_2D);
	glBegin(GL_TRIANGLES);
	glTexCoord2f(0.5,0.5);
	glVertex3f(a.v[0].p.x, a.v[0].p.y, a.v[0].p.z);
	glVertex3f(a.v[1].p.x, a.v[1].p.y, a.v[1].p.z);
	glVertex3f(a.v[2].p.x, a.v[2].p.y, a.v[2].p.z);

	glVertex3f(b.v[0].p.x, b.v[0].p.y, b.v[0].p.z);
	glVertex3f(b.v[1].p.x, b.v[1].p.y, b.v[1].p.z);
	glVertex3f(b.v[2].p.x, b.v[2].p.y, b.v[2].p.z);
	glEnd();
	glEnable(GL_TEXTURE_2D);

	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClear(GL_DEPTH_BUFFER_BIT);
}

void graphics_draw(struct game_state * gs,double d)
{
	carrot_timer+=d;

	post_processing=0;
	draw_to_frame_texture(&base_ft);
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	draw_base_frame(gs,d);
	update_frame_texture(&base_ft);

	post_processing=1;
	draw_to_frame_texture(&post_ft);
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	draw_post_frame(gs,d);
	update_frame_texture(&post_ft);

	post_processing=0;
	draw_to_frame_texture(&hud_ft);
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	draw_hud(gs,d);
	update_frame_texture(&hud_ft);

	glUseProgram(shader_program);
	post_processing=0;
	draw_frames(d);

	SDL_GL_SwapWindow(window);
	process_events();
}
