#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glu.h>
#include <stdlib.h>

#include "graphics.h"
#include "frame_texture.h"

struct frame_texture init_frame_texture( int width, int height)
{
	// create a texture object
	struct frame_texture ft;
	glGenTextures(1, &ft.texture_id);
	glBindTexture(GL_TEXTURE_2D, ft.texture_id);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	/*automatic mip-map*/
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height,
			0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// create a renderbuffer object to store depth info
	glGenRenderbuffers(1, &ft.rboId);
	glBindRenderbuffer(GL_RENDERBUFFER, ft.rboId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
			width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glGenFramebuffers(1, &ft.fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, ft.fboId);

	// attach the texture to FBO color attachment point
	glFramebufferTexture2D(GL_FRAMEBUFFER, // 1. fbo target: GL_FRAMEBUFFER
			GL_COLOR_ATTACHMENT0,  // 2. attachment point
			GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
			ft.texture_id,             // 4. tex ID
			0);                    // 5. mipmap level: 0(base)

	// attach the renderbuffer to depth attachment point
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,//1. fbo target: GL_FRAMEBUFFER
			GL_DEPTH_ATTACHMENT, // 2. attachment point
			GL_RENDERBUFFER,     // 3. rbo target: GL_RENDERBUFFER
			ft.rboId);              // 4. rbo ID

	// check FBO status
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE){
		printf("ERR: frame buffer incomplete\n");
		exit(-8);
	}

	// switch back to window-system-provided framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	ft.width=width;
	ft.height=height;
	return ft;
}

void draw_to_frame_texture(struct frame_texture * ft)
{
	glLoadIdentity();
	glBindFramebuffer(GL_FRAMEBUFFER, ft->fboId);
	glViewport(0,0,ft->width,ft->height);
}

/* This has to be called after the fbo is drawn to, to update the texture */
void update_frame_texture(struct frame_texture * ft)
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, ft->fboId);
	glBindTexture(GL_TEXTURE_2D, ft->texture_id);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, 
			ft->width,ft->height);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0,0,window_width,window_height);
}
