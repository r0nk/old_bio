#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glu.h>
#include <string.h>

#include "draw.h"
#include "fonts.h"
#include "model.h"
#include "graphics.h"
#include "model_reader.h"
#include "engine.h"

void draw_face(struct aiMesh * mesh,int j)
{       
	int i,index=0;
	for(i=0;i<mesh->mFaces[j].mNumIndices;i++){
		index = mesh->mFaces[j].mIndices[i];
		glNormal3f(mesh->mNormals[index].x, mesh->mNormals[index].y,
				mesh->mNormals[index].z);
		glTexCoord2f(mesh->mTextureCoords[0][index].x,
				mesh->mTextureCoords[0][index].y);
		glVertex3f(mesh->mVertices[index].x, mesh->mVertices[index].y,
				mesh->mVertices[index].z);
	}
}

void draw_mesh(struct aiMesh * mesh)
{       
	int i;

	glBegin(GL_TRIANGLES);
	for(i=0;i<mesh->mNumFaces;i++){
		draw_face(mesh,i);
	}
	glEnd();
}

void draw_vbo_mesh(struct mesh * mesh)
{
	glBindBuffer(GL_ARRAY_BUFFER, mesh->p_vbo);
	glVertexPointer(3,GL_FLOAT,0,0);

	glBindBuffer(GL_ARRAY_BUFFER, mesh->n_vbo);
	glNormalPointer(GL_FLOAT,0,0);

	glBindBuffer(GL_ARRAY_BUFFER, mesh->uv_vbo);
	glTexCoordPointer(3,GL_FLOAT,0,0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->index_vbo);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glDrawElements(GL_TRIANGLES,(mesh->n_indexes),GL_UNSIGNED_INT,0);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void draw_animation(struct model * m,double d)
{       
	float mat[16];
	int i=0;
	struct aiAnimation * a;
	const struct aiScene * scene = m->scene;
	if(!m->scene){
		printf("draw_animation ERR:!m->scene\n");
		return;
	}
	if(m->scene->mNumAnimations <1)
	{       
		glPushMatrix();
		glRotated(-90,1,0,0);
		draw_vbo_mesh(&m->mesh[0]);
		glPopMatrix();
		return;
	}
	a = scene->mAnimations[0];
	if(m->last_updated!=frames){
		m->last_updated=frames;
		m->cur_time+=d;
		if(m->cur_time > a->mDuration){
			m->cur_time-=a->mDuration;
		}
	}
	if(a->mNumChannels<1)
		printf("err:mNumChannels<1 ; %i\n",a->mNumChannels);
	for(i=0;i<scene->mNumMeshes;i++){
		glPushMatrix();
		glRotated(-90,1,0,0);
		if(i < a->mNumChannels){
			get_mat(a->mChannels[i],(m->cur_time),mat);
			glMultMatrixf(mat);
		}
		draw_vbo_mesh(&m->mesh[i]);
		glPopMatrix();
	}
}

void draw_line(struct vector start, struct vector end, struct vector color,
		float width, float alpha)
{       
	draw_2line(start,end,color,color,alpha,alpha,width);
}

void draw_stippled_line(struct vector start, struct vector end,
		struct vector color)
{       
	glPushAttrib(GL_ENABLE_BIT);

	glLineStipple(1,0xF000);
	glEnable(GL_LINE_STIPPLE);
	glLineWidth(3.0);
	glColor3f(color.x, color.y, color.z);
	glBegin(GL_LINES);
	glVertex3d(start.x,start.y,start.z);
	glVertex3d(end.x,end.y,end.z);
	glEnd();

	glPopAttrib();
}

static inline void draw_vertice(struct vertice v)
{       
	glNormal3f(v.n.x, v.n.y, v.n.z);
	glColor3f( v.c.x, v.c.y, v.c.z);
	glVertex3f(v.p.x, v.p.y, v.p.z);
}

void draw_poly(struct polygon p)
{       
	glBegin(GL_TRIANGLES);
	draw_vertice(p.v[0]);
	draw_vertice(p.v[1]);
	draw_vertice(p.v[2]);
	glEnd();
}

void draw_model(struct model model,struct vector loc,
		double angle, struct vector rotation)
{       
	unsigned int i;
	glPushMatrix();
	glTranslatef(loc.x,loc.y,loc.z);
	glRotated(angle,rotation.x,rotation.y,rotation.z);
	for(i=0;i<model.cardinality;i++)
		draw_poly(model.poly[i]);
	glPopMatrix();
}

void draw_2line(struct vector start, struct vector end,
		struct vector c1,struct vector c2, float a1,float a2,
		float width)
{       
	float off = width/100;
	glBegin(GL_TRIANGLES);

	glColor4f( c1.x, c1.y, c1.z,a1);
	glVertex3f(start.x, start.y-off, start.z);
	glVertex3f(start.x, start.y+off, start.z);
	glColor4f( c2.x, c2.y, c2.z,a2);
	glVertex3f(end.x, end.y-off, end.z);

	glColor4f (c2.x, c2.y, c2.z,a2);
	glVertex3f(end.x, end.y-off, end.z);
	glVertex3f(end.x, end.y+off, end.z);
	glColor4f (c1.x, c1.y, c1.z,a1);
	glVertex3f(start.x, start.y+off, start.z);

	//----------------------------------------------------

	glColor4f( c1.x, c1.y, c1.z,a1);
	glVertex3f(start.x, start.y, start.z-off);
	glVertex3f(start.x, start.y, start.z+off);
	glColor4f( c2.x, c2.y, c2.z,a2);
	glVertex3f(end.x, end.y, end.z-off);

	glColor4f (c2.x, c2.y, c2.z,a2);
	glVertex3f(end.x, end.y, end.z-off);
	glVertex3f(end.x, end.y, end.z+off);
	glColor4f (c1.x, c1.y, c1.z,a1);
	glVertex3f(start.x, start.y, start.z+off);

	//----------------------------------------------------

	glColor4f( c1.x, c1.y, c1.z,a1);
	glVertex3f(start.x-off, start.y, start.z);
	glVertex3f(start.x+off, start.y, start.z);
	glColor4f( c2.x, c2.y, c2.z,a2);
	glVertex3f(end.x-off, end.y, end.z);

	glColor4f (c2.x, c2.y, c2.z,a2);
	glVertex3f(end.x-off, end.y, end.z);
	glVertex3f(end.x+off, end.y, end.z);
	glColor4f (c1.x, c1.y, c1.z,a1);
	glVertex3f(start.x+off, start.y, start.z);

	glEnd();
}

void draw_field_line(struct vector start, struct vector end,
		struct vector c1,struct vector c2)
{       
	c1.x+=0.1; c1.y+=0.1; c1.z+=0.1; 
	c2.x+=0.1; c2.y+=0.1; c2.z+=0.1; 
	draw_2line(start,end,c1,c2,(start.y),(end.y-0.02),8.0);
}

