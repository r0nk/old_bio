#include <stdio.h>
#define GLEW_STATIC
#include <GL/glew.h>
#include <stdlib.h>

#include "poly.h"
#include "model.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

void bind_and_copy_buffers(struct mesh * m, int i)
{
	glGenBuffers(1,&m->p_vbo);
	glBindBuffer(GL_ARRAY_BUFFER,m->p_vbo);
	glBufferData(GL_ARRAY_BUFFER,
			m->n_vertices*sizeof(struct vector),
			m->p, GL_STATIC_DRAW);
	glGenBuffers(1,&m->n_vbo);
	glBindBuffer(GL_ARRAY_BUFFER,m->n_vbo);
	glBufferData(GL_ARRAY_BUFFER,
			m->n_vertices*sizeof(struct vector),
			m->n, GL_STATIC_DRAW);
	glGenBuffers(1,&m->uv_vbo);
	glBindBuffer(GL_ARRAY_BUFFER,m->uv_vbo);
	glBufferData(GL_ARRAY_BUFFER,
			m->n_vertices*sizeof(struct vector),
			m->t, GL_STATIC_DRAW);
	glGenBuffers(1,&m->index_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m->index_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			m->n_indexes*sizeof(unsigned int),
			m->indexes,
			GL_STATIC_DRAW);
}

/*store the data from aiScene into our mesh*/
void stamp_mesh(struct model * m, int i)
{
	m->mesh[i] = (struct mesh) {0};
	m->mesh[i].p = calloc(m->scene->mMeshes[i]->mNumVertices,
			sizeof(struct vector));
	m->mesh[i].n = calloc(m->scene->mMeshes[i]->mNumVertices,
			sizeof(struct vector));
	m->mesh[i].t = calloc(m->scene->mMeshes[i]->mNumVertices,
			sizeof(struct vector));
	int j;
	for(j=0;j<m->scene->mMeshes[i]->mNumVertices;j++){
		m->mesh[i].p[j] = (struct vector){
			m->scene->mMeshes[i]->mVertices[j].x,
			m->scene->mMeshes[i]->mVertices[j].y,
			m->scene->mMeshes[i]->mVertices[j].z,
		};
		m->mesh[i].n[j] = (struct vector){
			m->scene->mMeshes[i]->mNormals[j].x,
			m->scene->mMeshes[i]->mNormals[j].y,
			m->scene->mMeshes[i]->mNormals[j].z,
		};
		m->mesh[i].t[j] = (struct vector){
			m->scene->mMeshes[i]->mTextureCoords[0][j].x,
			m->scene->mMeshes[i]->mTextureCoords[0][j].y,
			m->scene->mMeshes[i]->mTextureCoords[0][j].z,
		};
	}

	m->mesh[i].n_vertices = m->scene->mMeshes[i]->mNumVertices;
	m->mesh[i].n_indexes = m->scene->mMeshes[i]->mNumFaces*3;

	m->mesh[i].indexes=
		calloc(m->scene->mMeshes[i]->mNumFaces*3,sizeof(int));
	for(j=0;j<m->scene->mMeshes[i]->mNumFaces;j++){
		m->mesh[i].indexes[(j*3)]   =
			m->scene->mMeshes[i]->mFaces[j].mIndices[0];
		m->mesh[i].indexes[(j*3)+1] =
			m->scene->mMeshes[i]->mFaces[j].mIndices[1];
		m->mesh[i].indexes[(j*3)+2] =
			m->scene->mMeshes[i]->mFaces[j].mIndices[2];
	}

}

void generate_vbo_for_mesh(struct model *m, int i)
{
	stamp_mesh(m,i);
	/*bind the vbos and tell opengl where the data is*/
	bind_and_copy_buffers(&m->mesh[i],i);

	/*unbind the vbos and clean up*/
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	free(m->mesh[i].indexes);
	free(m->mesh[i].p);
	free(m->mesh[i].n);
	free(m->mesh[i].t);
}

void generate_vbos(struct model * m)
{
	int i;
	if(m->scene->mNumMeshes>MAX_MESHES){
		printf("ERR: model has too many meshes\n");
		return;
	}
	for(i=0;i<m->scene->mNumMeshes;i++){
		generate_vbo_for_mesh(m,i);
	}
}

const struct aiScene * get_aiScene(char * path)
{
	const struct aiScene* scene = aiImportFile(path,
			aiProcess_CalcTangentSpace       |
			aiProcess_Triangulate            |
			aiProcess_JoinIdenticalVertices  |
			aiProcess_SortByPType);
	if( !scene)
	{
		printf("import_model ERR:%s\n",aiGetErrorString());
		return NULL;
	}
	return scene;
}

struct model get_model(char * path)
{
	struct model m;
	m.scene = get_aiScene(path);
	generate_vbos(&m);
	return m;
}
