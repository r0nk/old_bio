#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <assimp/cimport.h>
#include "profiler_def.h"

#ifdef PROFILE_GAME
#include <gperftools/profiler.h>
#endif

#include "game_state.h"
#include "graphics.h"
#include "engine.h"
#include "game.h"
#include "audio.h"
#include "map.h"
#include "room.h"
#include "input.h"
#include "ui.h"

unsigned int last_time;

double delta_time()/* the change in time in terms of milliseconds */
{
	int s;
	float ret;
	s = SDL_GetTicks();
	ret = (double)s-last_time;
	last_time=s;
	ret/=1000.0; /* convert to milliseconds */
	return ret;
}

void init(int argc, char * argv[])
{
	int seed=1337;
	read_save_state();
	init_graphics();
	if(argc>=2){
		seed = atoi(argv[1]);
	}else{
		seed = time(NULL);
	}
	printf("seed:%i\n",seed);
	srand(seed);

	init_audio();
	generate_map(&world_map,1);
	init_ui();
	paused=0;
	is_game_over=0;
	game_running=1;
}

int main(int argc, char * argv[])
{
	double d;
	init(argc,argv);
	while(game_running){
		d = delta_time();
		engine_tick(&(world_map.current_room->gs),d);
		graphics_draw(&(world_map.current_room->gs),d);
	}
	deinit_ui();
	return 0;
}
