#include <stdio.h>
#include "save.h"
#include <sys/stat.h>
#include <sys/fcntl.h>

char * save_path = "save_file.save";

void read_save_state()
{
	global_save_state = (struct save_state) {0};
	FILE* file = fopen(save_path,"r");
	if(!file){
		printf("no save file opened\n");
		return;
	}
	fscanf(file,"%i",&global_save_state.audio);
	fscanf(file,"%i",&global_save_state.blaster_kills);
	fscanf(file,"%i",&global_save_state.life_stolen);
	fscanf(file,"%i",&global_save_state.coins_spent);
	fscanf(file,"%i",&global_save_state.unibar_picks);
	fscanf(file,"%i",&global_save_state.times_leveled);
	fclose(file);
}

void write_save_state()
{
	FILE* file = fopen(save_path,"w+");
	if(!file){
		printf("couldn't write to save file\n");
		return;
	}
	fprintf(file,"%i\n",global_save_state.audio);
	fprintf(file,"%i\n",global_save_state.blaster_kills);
	fprintf(file,"%i\n",global_save_state.life_stolen);
	fprintf(file,"%i\n",global_save_state.coins_spent);
	fprintf(file,"%i\n",global_save_state.unibar_picks);
	fprintf(file,"%i\n",global_save_state.times_leveled);
	fclose(file);
}

