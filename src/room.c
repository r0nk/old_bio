#include <string.h>
#include <stdlib.h>

#include "map.h"
#include "engine.h"
#include "model.h"
#include "room.h"
#include "shop.h"
#include "game.h"
#include "sprinkle.h"
#include "neighborhood.h"
#include "procedural.h"

int n_sprinkles_in_catalog=0;

void get_layout(struct room * room,char * pathname)
{
	int fd = open(pathname,O_RDONLY);
	if(fd==-1){
		printf("get_layout() couldn't open");
		exit(29);
	}

	char c[2];
	int x=0,y=0;

	memset(&room->layout,0,sizeof(struct layout));

	while(read(fd,c,1)){
		switch(c[0]){
		case '\n':
			y++;
			x=0;
			continue;
		case ' ':
			room->layout.tiles[x][y]='\0';
			break;
		case 's':
			room->layout.tiles[x][y]='s';
			break;
		case 'L':
			room->light[room->n_lights].location = 
				(struct vector){x,3,y};
			room->light[room->n_lights].origin = 
				(struct vector){x,3,y};
			room->light[room->n_lights].color = 
				(struct vector){1,1,1};
			room->light[room->n_lights].velocity = 
				(struct vector){0,0,0};
			room->n_lights++;
		default:
			room->layout.tiles[x][y]=c[0];
			break;
		}
		x++;
	}
}

void count_doorways(struct room * room)
{
	int n = 0;
	int x,z;
	room->has_shop=0;
	for(z=0;z<MAX_ROOM_HEIGHT;z++){
		for(x=0;x<MAX_ROOM_WIDTH;x++){
			if((room->layout.tiles[x][z]) > ('0'-1)
					&& room->layout.tiles[x][z]<'9'){
				room->doorway[n].location=(struct vector){x,0,z};
				n++;
			}
			if(room->layout.tiles[x][z]=='K'){
				room->has_shop=1;
			}
		}
	}

	room->n_doorways=n;
}

struct vector pick_colors(int i,int level)
{
	struct vector c;
	switch(level){
		case 1://cyan
			c = (struct vector) {0.31,0.572,0.561};
			break;
		case 2://green
			c = (struct vector) {0.408,0.753,0.408};
			break;
		case 3://red
			c = (struct vector) {0.941,0.506,0.506};
			break;
	}
	c = v_s_mul(2,c);
	switch(i){
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			c.x+=(rand()%20)/50.0;
			c.y+=(rand()%20)/50.0;
			c.z+=(rand()%20)/50.0;
			return c;
			break;
		case 8:
			return (struct vector) {0.655,0.38,0.161};
			break;
		case 9:
			return (struct vector) {16/255.0,16/255.0,16/255.0};
			break;
		default:
			return (struct vector) {rand()%255,rand()%255,rand()%255};
			break;
	}
}

void add_f_sprinkle(struct model * model, int x, int z,int i)
{
	int iv = model->mesh[i].n_vertices;
	int ii = model->mesh[i].n_indexes;
	float yoff = 0.0001;

	model->mesh[i].n[iv+0]=(struct vector) {0,1,0};
	model->mesh[i].n[iv+1]=(struct vector) {0,1,0};
	model->mesh[i].n[iv+2]=(struct vector) {0,1,0};
	model->mesh[i].n[iv+3]=(struct vector) {0,1,0};

	model->mesh[i].p[iv+0]=(struct vector) {x+0,yoff,z+0};
	model->mesh[i].p[iv+1]=(struct vector) {x+0,yoff,z+1};
	model->mesh[i].p[iv+2]=(struct vector) {x+1,yoff,z+0};
	model->mesh[i].p[iv+3]=(struct vector) {x+1,yoff,z+1};

	model->mesh[i].t[iv+0]=(struct vector) {0,0,0};
	model->mesh[i].t[iv+1]=(struct vector) {0,1,0};
	model->mesh[i].t[iv+2]=(struct vector) {1,0,0};
	model->mesh[i].t[iv+3]=(struct vector) {1,1,0};

	model->mesh[i].indexes[ii]=iv;
	model->mesh[i].indexes[ii+1]=iv+1;
	model->mesh[i].indexes[ii+2]=iv+2;

	model->mesh[i].indexes[ii+3]=iv+2;
	model->mesh[i].indexes[ii+4]=iv+1;
	model->mesh[i].indexes[ii+5]=iv+3;

	model->mesh[i].n_vertices+=4;
	model->mesh[i].n_indexes+=6;
}

void add_wall_mdl(struct model * model, int x, int z)
{
	int iv = model->mesh[1].n_vertices;
	int ii = model->mesh[1].n_indexes;
	double height = 1.5;

	model->mesh[1].p[iv+0]=(struct vector) {x+0,height,z+0};//unused
	model->mesh[1].p[iv+1]=(struct vector) {x+0,height,z+1};
	model->mesh[1].p[iv+2]=(struct vector) {x+1,height,z+0};
	model->mesh[1].p[iv+3]=(struct vector) {x+1,height,z+1};

	model->mesh[1].p[iv+4]=(struct vector) {x+0,0.0,z+0};//unused
	model->mesh[1].p[iv+5]=(struct vector) {x+0,0.0,z+1};
	model->mesh[1].p[iv+6]=(struct vector) {x+1,0.0,z+0};
	model->mesh[1].p[iv+7]=(struct vector) {x+1,0.0,z+1};

	model->mesh[1].n[iv+0]=normal(model->mesh[1].p[iv+0]);
	model->mesh[1].n[iv+1]=normal(model->mesh[1].p[iv+1]);
	model->mesh[1].n[iv+2]=normal(model->mesh[1].p[iv+2]);
	model->mesh[1].n[iv+3]=normal(model->mesh[1].p[iv+3]);

	model->mesh[1].n[iv+4]=normal(model->mesh[1].p[iv+0]);
	model->mesh[1].n[iv+5]=normal(model->mesh[1].p[iv+1]);
	model->mesh[1].n[iv+6]=normal(model->mesh[1].p[iv+2]);
	model->mesh[1].n[iv+7]=normal(model->mesh[1].p[iv+3]);

	model->mesh[1].t[iv+0]=(struct vector) {0.0,1,0};//unused
	model->mesh[1].t[iv+1]=(struct vector) {0.0,1,0};
	model->mesh[1].t[iv+2]=(struct vector) {1.0,1,0};
	model->mesh[1].t[iv+3]=(struct vector) {0.5,1,0};

	model->mesh[1].t[iv+4]=(struct vector) {0.5,0,0};//unused
	model->mesh[1].t[iv+5]=(struct vector) {0.0,0,0};
	model->mesh[1].t[iv+6]=(struct vector) {1.0,0,0};
	model->mesh[1].t[iv+7]=(struct vector) {0.5,0,0};

	model->mesh[1].indexes[ii]=iv+2;
	model->mesh[1].indexes[ii+1]=iv+3;
	model->mesh[1].indexes[ii+2]=iv+6;

	model->mesh[1].indexes[ii+3]=iv+6;
	model->mesh[1].indexes[ii+4]=iv+7;
	model->mesh[1].indexes[ii+5]=iv+3;

	model->mesh[1].indexes[ii+6]=iv+3;
	model->mesh[1].indexes[ii+7]=iv+1;
	model->mesh[1].indexes[ii+8]=iv+5;

	model->mesh[1].indexes[ii+9]=iv+3;
	model->mesh[1].indexes[ii+10]=iv+5;
	model->mesh[1].indexes[ii+11]=iv+7;

	model->mesh[1].n_vertices+=8;
	model->mesh[1].n_indexes+=12;
}

void add_roof_mdl(struct model * model, int x, int z)
{
	int iv = model->mesh[4].n_vertices;
	int ii = model->mesh[4].n_indexes;

	model->mesh[4].n[iv+0]=(struct vector) {0,1,0};
	model->mesh[4].n[iv+1]=(struct vector) {0,1,0};
	model->mesh[4].n[iv+2]=(struct vector) {0,1,0};
	model->mesh[4].n[iv+3]=(struct vector) {0,1,0};

	model->mesh[4].p[iv+0]=(struct vector) {x+0,1.5,z+0};
	model->mesh[4].p[iv+1]=(struct vector) {x+0,1.5,z+1};
	model->mesh[4].p[iv+2]=(struct vector) {x+1,1.5,z+0};
	model->mesh[4].p[iv+3]=(struct vector) {x+1,1.5,z+1};

	model->mesh[4].t[iv+0]=(struct vector) {0,0,0};
	model->mesh[4].t[iv+1]=(struct vector) {0,1,0};
	model->mesh[4].t[iv+2]=(struct vector) {1,0,0};
	model->mesh[4].t[iv+3]=(struct vector) {1,1,0};

	model->mesh[4].indexes[ii]=iv;
	model->mesh[4].indexes[ii+1]=iv+1;
	model->mesh[4].indexes[ii+2]=iv+2;

	model->mesh[4].indexes[ii+3]=iv+2;
	model->mesh[4].indexes[ii+4]=iv+1;
	model->mesh[4].indexes[ii+5]=iv+3;

	model->mesh[4].n_vertices+=4;
	model->mesh[4].n_indexes+=6;
}

void add_floor_light_mdl(struct room * room,struct model * model, int x, int z)
{
	int iv = model->mesh[5].n_vertices;
	int ii = model->mesh[5].n_indexes;
	float yoff = 0.0002;

	model->mesh[5].n[iv+0]=(struct vector) {0,1,0};
	model->mesh[5].n[iv+1]=(struct vector) {0,1,0};
	model->mesh[5].n[iv+2]=(struct vector) {0,1,0};
	model->mesh[5].n[iv+3]=(struct vector) {0,1,0};

	float x1=(walkable_char(room->layout.tiles[x-1][z]))?-0.2:0;
	float x2=(walkable_char(room->layout.tiles[x+1][z]))?1.2:1;
	float z1=(walkable_char(room->layout.tiles[x][z-1]))?-0.2:0;
	float z2=(walkable_char(room->layout.tiles[x][z+1]))?1.2:1;

	model->mesh[5].p[iv+0]=(struct vector) {x+x1,yoff,z+z1};
	model->mesh[5].p[iv+1]=(struct vector) {x+x1,yoff,z+z2};
	model->mesh[5].p[iv+2]=(struct vector) {x+x2,yoff,z+z1};
	model->mesh[5].p[iv+3]=(struct vector) {x+x2,yoff,z+z2};

	model->mesh[5].t[iv+0]=(struct vector) {0,0,0};
	model->mesh[5].t[iv+1]=(struct vector) {0,1,0};
	model->mesh[5].t[iv+2]=(struct vector) {1,0,0};
	model->mesh[5].t[iv+3]=(struct vector) {1,1,0};

	model->mesh[5].indexes[ii]=iv;
	model->mesh[5].indexes[ii+1]=iv+1;
	model->mesh[5].indexes[ii+2]=iv+2;

	model->mesh[5].indexes[ii+3]=iv+2;
	model->mesh[5].indexes[ii+4]=iv+1;
	model->mesh[5].indexes[ii+5]=iv+3;

	model->mesh[5].n_vertices+=4;
	model->mesh[5].n_indexes+=6;
}

void add_floor_mdl(struct model * model, int x, int z)
{
	int iv = model->mesh[0].n_vertices;
	int ii = model->mesh[0].n_indexes;

	model->mesh[0].n[iv+0]=(struct vector) {0,1,0};
	model->mesh[0].n[iv+1]=(struct vector) {0,1,0};
	model->mesh[0].n[iv+2]=(struct vector) {0,1,0};
	model->mesh[0].n[iv+3]=(struct vector) {0,1,0};

	model->mesh[0].p[iv+0]=(struct vector) {x+0,0,z+0};
	model->mesh[0].p[iv+1]=(struct vector) {x+0,0,z+1};
	model->mesh[0].p[iv+2]=(struct vector) {x+1,0,z+0};
	model->mesh[0].p[iv+3]=(struct vector) {x+1,0,z+1};

	model->mesh[0].t[iv+0]=(struct vector) {0,0,0};
	model->mesh[0].t[iv+1]=(struct vector) {0,1,0};
	model->mesh[0].t[iv+2]=(struct vector) {1,0,0};
	model->mesh[0].t[iv+3]=(struct vector) {1,1,0};

	model->mesh[0].indexes[ii]=iv;
	model->mesh[0].indexes[ii+1]=iv+1;
	model->mesh[0].indexes[ii+2]=iv+2;

	model->mesh[0].indexes[ii+3]=iv+2;
	model->mesh[0].indexes[ii+4]=iv+1;
	model->mesh[0].indexes[ii+5]=iv+3;

	model->mesh[0].n_vertices+=4;
	model->mesh[0].n_indexes+=6;
}

unsigned int room_index_precount(struct room * room)
{
	int x,z;
	unsigned int ret=0;
	for(x=0;x<(MAX_ROOM_WIDTH);x++){
		for(z=0;z<(MAX_ROOM_HEIGHT);z++){
			if(room->layout.tiles[x][z] == LAYOUT_WALL)
				ret+=24;
			else if (room->layout.tiles[x][z])
				ret+=8;
		}
	}
	return ret;
}

unsigned int room_poly_precount(struct room  * room)
{
	int x,z;
	unsigned int ret=0;
	for(x=0;x<(MAX_ROOM_WIDTH);x++){
		for(z=0;z<(MAX_ROOM_HEIGHT);z++){
			if(room->layout.tiles[x][z] == LAYOUT_WALL)
				ret+=16;
			else if (room->layout.tiles[x][z])
				ret+=4;
		}
	}
	return ret;
}

void init_room_mesh(struct room * room, int i,int c)
{
	room->model.mesh[i].p=calloc(3*sizeof(struct vector),c);
	room->model.mesh[i].n=calloc(3*sizeof(struct vector),c);
	room->model.mesh[i].t=calloc(3*sizeof(struct vector),c);
	room->model.mesh[i].indexes=calloc(sizeof(unsigned int),
			room_index_precount(room));
}

void free_room_mesh(struct room * room, int i)
{
	free(room->model.mesh[i].p);
	free(room->model.mesh[i].n);
	free(room->model.mesh[i].t);
	free(room->model.mesh[i].indexes);
}

int sprink_storage_check(struct room * room,int x, int z)
{
	if(	(walkable_char(room->layout.tiles[x][z])) ||
		(walkable_char(room->layout.tiles[x][z+1])) ||
		(walkable_char(room->layout.tiles[x+1][z])) ||
		(walkable_char(room->layout.tiles[x][z-1])) ||
		(walkable_char(room->layout.tiles[x-1][z]))){
		return 0;
	}else{
		return 1;
	}
}

void add_sprink_pipes(struct room * room,int level,int x,int z)
{
	int i=0;
	int r;
	for(i=0;i<3;i++){
		r = rand()%MAX_SPRINKLES;
		z++;
		room->sprinkles[r].alive=1;
		room->sprinkles[r].location=(struct vector) {x+0.1,0.8,z+0.1};
		room->sprinkles[r].type=SPRINK_STRAIGHT_PIPE;
	}
}

void place_wall_sprinkle(struct room * room, int level,int x, int z)
{
	int r = rand()%MAX_SPRINKLES;
	room->sprinkles[r].alive=1;
	room->sprinkles[r].location=(struct vector) {x+0.1,0.8,z+0.1};
	room->sprinkles[r].type=SPRINK_INTERGRID;
	if(level==1 && !(rand()%4))
		room->sprinkles[r].type=SPRINK_VALVE;
	if(level==1 && !(rand()%4)){
		room->sprinkles[r].type=SPRINK_STORAGE_TANK;
		if(!sprink_storage_check(room,x,z)){
			room->sprinkles[r].alive=0;
		}else{
		//	add_sprink_pipes(room,level,x,z);
		}
		return;
	}
	room->sprinkles[r].kind=rand()%3;
	room->sprinkles[r].direction.x=rand()%2;

	if(room->sprinkles[r].direction.x){
		if((x<1)||!(room->layout.tiles[x-1][z] == LAYOUT_WALL)  ||
			!(walkable_char(room->layout.tiles[x-1][z+1])) ||
			!(walkable_char(room->layout.tiles[x][z+1])))
		{
			room->sprinkles[r].alive=0;
			return;
		}
	}else{
		if((z<1)||!(room->layout.tiles[x][z-1] == LAYOUT_WALL)  ||
			!(walkable_char(room->layout.tiles[x+1][z-1])) ||
			!(walkable_char(room->layout.tiles[x+1][z])))
		{
			room->sprinkles[r].alive=0;
			return;
		}
	}


	int i;
	for(i=0;i<MAX_SPRINKLES;i++){
		if(i==r)
			continue;
		if(is_near(room->sprinkles[i].location,
					room->sprinkles[r].location,2)){
			room->sprinkles[r].alive=0;
		}
	}
}

void place_floor_sprinkle(struct room * room, int level,int x, int z)
{
	struct particle p;
	if(!room->sprinkle_layout.tiles[x][z])
		room->sprinkle_layout.tiles[x][z]=1;
	else
		return;

	if(n_sprinkles_in_catalog<=0){
		printf("No sprinkles in catalog.\n");

		return;
	}

	int scr = rand()%n_sprinkles_in_catalog;
	int r = rand()%MAX_SPRINKLES;
	room->sprinkles[r].alive=1;
	room->sprinkles[r].location=(struct vector) {x+0.1,0.0,z+0.1};
	room->sprinkles[r].type=sprinkle_catalog[scr].type;
}

void generate_sprinkles(struct room * room,int level)
{
	int x,z;
	for(x=0;x<(MAX_ROOM_WIDTH);x++){
		for(z=0;z<(MAX_ROOM_HEIGHT);z++){
			if(room->layout.tiles[x][z] == LAYOUT_WALL){
				if(!(rand()%5)){
					place_wall_sprinkle(room,level,x,z);
				}
			}else if (room->layout.tiles[x][z]){
				if(!(rand()%5)){
					//place_floor_sprinkle(room,level,x,z);
					;
				}
			}
		}
	}
}


void vectorize_wall(struct room * room, int x, int z)
{
	int y = 0;
	if(z>0 && !(room->layout.tiles[x][z-1] == LAYOUT_WALL)){
		room->vlayout.line[room->vlayout.n_lines].a=(struct vector){x,y,z};
		room->vlayout.line[room->vlayout.n_lines].b=(struct vector){x+1,y,z};
		room->vlayout.n_lines++;
	}

	if(x>0 && !(room->layout.tiles[x-1][z] == LAYOUT_WALL)){
		room->vlayout.line[room->vlayout.n_lines].a=(struct vector){x,y,z};
		room->vlayout.line[room->vlayout.n_lines].b=(struct vector){x,y,z+1};
		room->vlayout.n_lines++;
	}

	if(!(room->layout.tiles[x+1][z] == LAYOUT_WALL)){
		room->vlayout.line[room->vlayout.n_lines].a=(struct vector){x+1,y,z};
		room->vlayout.line[room->vlayout.n_lines].b=(struct vector){x+1,y,z+1};
		room->vlayout.n_lines++;
	}

	if(!(room->layout.tiles[x][z+1] == LAYOUT_WALL)){
		room->vlayout.line[room->vlayout.n_lines].a=(struct vector){x,y,z+1};
		room->vlayout.line[room->vlayout.n_lines].b=(struct vector){x+1,y,z+1};
		room->vlayout.n_lines++;
	}
}

void remove_vl_line(struct vector_layout * vl, int index)
{
	int i;
	for(i=index;i<vl->n_lines;i++){
		vl->line[i]=vl->line[i+1];
	}
	vl->n_lines--;
}

void collapse_vl_lines(struct vector_layout * vl, int i, int j)
{
	struct vector a = vl->line[i].a;
	struct vector b = vl->line[i].b;
	struct vector c = vl->line[j].b;

	struct vector p = v_sub(c,a);
	struct vector f = v_sub(b,a);
	struct vector x = v_cross(p,f);
	struct vector zero_v = (struct vector) {0,0,0};

	if(v_eq(x,zero_v)){//make sure the angles are the same
		remove_vl_line(vl,j);
		vl->line[i].b=c;
	}
}

void simplify_vector_layout(struct vector_layout * vl)
{
	int i,j;
	struct vector a,b;
	for(i=0;i<(vl->n_lines-1);i++){
		for(j=0;j<(vl->n_lines-1);j++){
			if(i==j)
				continue;
			a = (vl->line[i].b);
			b = (vl->line[j].a);
			if(v_eq(a,b)){
				collapse_vl_lines(vl,i,j);
			}
		}
	}
}

void vectorize_layout(struct room * room)
{
	room->vlayout.n_lines=0;
	int x,z;
	for(x=0;x<(MAX_ROOM_WIDTH);x++){
		for(z=0;z<(MAX_ROOM_HEIGHT);z++){
			if(room->layout.tiles[x][z] == LAYOUT_WALL){
				vectorize_wall(room,x,z);
			}
		}
	}
	int buf = room->vlayout.n_lines+1;
	while(buf!=room->vlayout.n_lines)
	{
		buf = room->vlayout.n_lines;
		simplify_vector_layout(&(room->vlayout));
	}
}

void model_room(struct room * room)
{
	int x,z;

	room->model.mesh[0].n_vertices=0;
	room->model.mesh[0].n_indexes=0;

	int c = room_poly_precount(room);

	init_room_mesh(room,0,c);//floors
	init_room_mesh(room,1,c);//walls (colored)
	init_room_mesh(room,2,c);//sprinkles (little misc decorations)
	init_room_mesh(room,3,c);//sprinkles (little misc decorations)
	init_room_mesh(room,4,c);//roof tiles
	init_room_mesh(room,5,c);//floor light tiles

	for(x=0;x<(MAX_ROOM_WIDTH);x++){
		for(z=0;z<(MAX_ROOM_HEIGHT);z++){
			if(room->layout.tiles[x][z] == LAYOUT_WALL){
				add_wall_mdl(&room->model,x,z);
				add_roof_mdl(&room->model,x,z);
				add_floor_light_mdl(room,&room->model,x,z);
			} else if (room->layout.tiles[x][z]) {
				if(room->sprinkle_layout.tiles[x][z])
					continue;
				add_floor_mdl(&room->model,x,z);
				if(!(rand()%5)){
					add_f_sprinkle(&room->model,x,z,
							(2+rand()%2));
				}
			}
		}
	}

	bind_and_copy_buffers(&room->model.mesh[0]);
	bind_and_copy_buffers(&room->model.mesh[1]);
	bind_and_copy_buffers(&room->model.mesh[2]);
	bind_and_copy_buffers(&room->model.mesh[3]);
	bind_and_copy_buffers(&room->model.mesh[4]);
	bind_and_copy_buffers(&room->model.mesh[5]);

	free_room_mesh(room,0);
	free_room_mesh(room,1);
	free_room_mesh(room,2);
	free_room_mesh(room,3);
	free_room_mesh(room,4);
	free_room_mesh(room,5);
}

void room_lights(struct room * room, int i, int level)
{
	room->color=pick_colors(i,level);
	if(i==8){
		room->light[0].color=(struct vector){1,1,0};
		room->light[1].color=(struct vector){1,1,0};
		return;
	}
	int j;
	for(j=0;j<room->n_lights;j++){
		room->light[j].color = pick_colors(i,level);
	}
}

struct room generate_room(int i,int level)
{
	struct room room = {0};
	room.starting_room=0;
	switch(i)
	{
		case 0:
			get_layout(&room,"layouts/starting.special");
			room.starting_room=level;
			count_doorways(&room);
			break;
		case 8:
			get_layout(&room,"layouts/shop.special");
			room.shop=generate_shop(&room);
			count_doorways(&room);
			break;
		case 9:
			room.boss_room=level;
			if(level==1){
				get_layout(&room,"layouts/boss.special");
			} else if(level==2){
				get_layout(&room,"layouts/mole.special");
			} else if(level==3){
				get_layout(&room,"layouts/spinner.special");
			}
			count_doorways(&room);
			break;
		default:
			generate_layout(&room);
			break;
	}

	room.gs = init_game(&room,level);
	room_lights(&room,i,level);
	generate_sprinkles(&room,level);
	vectorize_layout(&room);
	room.neighborhoods = layout_to_neighborhoods(&room.layout);
	vectorize_layout(&room);
	model_room(&room); 
	return room;
}
