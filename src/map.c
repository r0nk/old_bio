#include <stdlib.h>

#include "engine.h"
#include "camera.h"
#include "map.h"
#include "room.h"

struct doorway * connected_doorway(struct map * map,int i);

inline int walkable(int i, int j)
{
	struct layout * l = &world_map.current_room->layout;
	return (walkable_char(l->tiles[i][j]));
}

struct doorway * get_doorway_by_index(struct map * map, int index)
{
	int i,j;
	if(index==-1)
		return NULL;
	for(i=0;i<map->n_rooms;i++){
		for(j=0;j<(map->room[i].n_doorways);j++){
			if(map->room[i].doorway[j].index == index){
				//printf("dbi(%i): (r:%i,d[%i])\n",index,i,j);
				return &map->room[i].doorway[j];
			}
		}
	}
	return NULL;
}

struct room * get_room_by_doorway_index(struct map * map, int index)
{
	int i,j;
	for(i=0;i<map->n_rooms;i++){
		for(j=0;j<(map->room[i].n_doorways);j++){
			if(map->room[i].doorway[j].index == index){
				return &map->room[i];
			}
		}
	}
	fprintf(stderr,"ERR: get_room_by_doorway_index: door not found\n");
	return NULL;
}

struct doorway * get_nonconnected_door(struct map * map)
{
	int i;
	struct doorway * doorway;
	int rando=rand();

	for(i=0;i<(map->n_doorways*10);i++){
		rando=rand();
		doorway = get_doorway_by_index(map,rando%(map->n_doorways));
		if(doorway==NULL)
			continue;

		if(!doorway->is_connected)
			return doorway;
	}

	/* fine rando you selfish bastard, we'll just brute force it then */
	for(i=0;i<(map->n_doorways);i++){
		doorway = get_doorway_by_index(map,i);
		if(doorway==NULL)
			continue;

		if(!doorway->is_connected)
			return doorway;
	}

	fprintf(stderr,"ERR: next_nonconnected_door: no nonconnected door\n");
	exit(10);
	return NULL;
}

void connect_edges(struct map * map,struct doorway * d1, struct doorway * d2)
{
	struct room *room;

	if(map->graph.n_edges >= MAX_EDGES) {
		printf("WARNING: tried to generate over max edges\n");
		return;
	}
	
	d1->is_connected=1;
	d2->is_connected=1;
	map->graph.edge[map->graph.n_edges].a=d1->index;
	map->graph.edge[map->graph.n_edges].b=d2->index;

	room = get_room_by_doorway_index(map,d2->index);
	if(room->has_shop || room->boss_room || room->starting_room)
		d1->color=room->color;
	else
		d1->color=(struct vector){0.655,0.161,0.161};

	room = get_room_by_doorway_index(map,d1->index);

	if(room->has_shop || room->boss_room || room->starting_room)
		d2->color=room->color;
	else
		d2->color=(struct vector){0.655,0.161,0.161};
	map->graph.n_edges++;
}

void reset_connections(struct map * map)
{
	int i;
	struct doorway * d;
	/* we reset all the connections first */
	for(i=0;i<(map->n_doorways);i++){
		d = get_doorway_by_index(map,i);
		d->is_connected=0;
	}
	for(i=0;i<map->n_rooms;i++){
		map->room[i].visited=0;
	}
	map->graph.n_edges=0;
}

int rooms_connected(struct map * map, struct room * r1, struct room * r2)
{
	struct doorway * d;
	struct room * rb;
	struct room * rb2;
	int i;
	for(i=0;i<(map->n_doorways);i++){
		d = connected_doorway(map, i);
		if(!d)
			continue;
		rb = get_room_by_doorway_index(map,i);
		rb2 = get_room_by_doorway_index(map,d->index);
		if((r1==rb || r2==rb) && (r1==rb2 || r2==rb2)) {
			return 1;
		}
	}
	return 0;
}

void generate_edge(struct map * map)
{
	struct doorway * d1, * d2;
	struct room *room;

	d1 = get_nonconnected_door(map);
	d1->is_connected=1;
	d2 = get_nonconnected_door(map);
	d1->is_connected=0;

	room = get_room_by_doorway_index(map,d1->index);

	if(room->visited){
		d1 = get_nonconnected_door(map);
		room = get_room_by_doorway_index(map,d1->index);
	}

	if(room==get_room_by_doorway_index(map,d2->index))
		d2 = get_nonconnected_door(map);

	if(rooms_connected(map,get_room_by_doorway_index(map,d1->index),
			       	get_room_by_doorway_index(map,d2->index)))
	{
		d2 = get_nonconnected_door(map);
	}

	connect_edges(map,d1,d2);
}

/*make sure the player starting room, the shop, and the boss are all connected 
 * first */
void generate_edges_for_specials(struct map * map)
{
	connect_edges(map,&map->room[0].doorway[0],get_nonconnected_door(map));
	connect_edges(map,&map->room[8].doorway[0],get_nonconnected_door(map));
	connect_edges(map,&map->room[9].doorway[0],get_nonconnected_door(map));
}

/*make sure all the rooms are connected*/
int check_map(struct map * map)
{
	int i,j,k;
	struct doorway * dest_door;
	struct room * dest_room;
	map->current_room->visited=1;
	for(j=0;j<map->n_rooms;j++){
		for(i=0;i<map->n_rooms;i++){
			if(!map->room[i].visited)
				continue;
			for(k=0;k<map->room[i].n_doorways;k++){
				dest_door = connected_doorway(map,
						map->room[i].doorway[k].index);
				if(dest_door==NULL){
					continue;
				}
				dest_room = get_room_by_doorway_index(map,
							dest_door->index);
				dest_room->visited=1;
			}
		}
	}
	for(i=0;i<map->n_rooms;i++){
		if(!map->room[i].visited){
			printf("room[%i] not visited\n",i);
			return 0;
		}
	}
	return 1;
}

void connect_rooms(struct map * map, int i1, int i2)
{
	int i,j;
	for(i=0;i<4;i++){
		if(!map->room[i1].doorway[i].is_connected)
			break;
	}
	for(j=0;j<4;j++){
		if(!map->room[i2].doorway[j].is_connected)
			break;
	}

	connect_edges(map,&map->room[i1].doorway[i],&map->room[i2].doorway[j]);
}

void statically_generate_edges(struct map * map)
{
	connect_rooms(map,0,1);
	connect_rooms(map,1,2);
	connect_rooms(map,1,5);
	connect_rooms(map,1,4);
	connect_rooms(map,2,5);
	connect_rooms(map,2,3);
	connect_rooms(map,3,4);
	connect_rooms(map,3,5);
	connect_rooms(map,3,6);
	connect_rooms(map,4,8);
	connect_rooms(map,4,7);
	connect_rooms(map,5,8);
	connect_rooms(map,6,8);
	connect_rooms(map,7,8);
	connect_rooms(map,7,9);
}

void generate_edges(struct map * map)
{
	map->graph.n_edges=0;
	statically_generate_edges(map);
	if(!check_map(map)){
		fprintf(stderr,"map failed check\n");
		exit(-234);
	}

	/*
	do{
		reset_connections(map);
		map->graph.n_edges=0;
		while(map->graph.n_edges<((map->n_doorways/2))){
			generate_edge(map);
		}
	} while(!check_map(map));
	*/
}

void index_doorways(struct map * map)
{
	int i,j,index = 0;
	for(i=0;i<map->n_rooms;i++){
		for(j=0;j<(map->room[i].n_doorways);j++){
			map->room[i].doorway[j].index=index;
			map->room[i].doorway[j].color=map->room[i].color;
			index++;
		}
	}
	map->n_doorways=index;
}

void update_door_colors(struct map * map)
{
	int i;
	struct doorway * door;
	struct doorway * dest_door;
	struct room * dest_room;
	for(i=0;i<(map->n_doorways);i++){
		door = get_doorway_by_index(map,i);
		dest_door = connected_doorway(map,i);
		if(!dest_door)
			continue;
		dest_room = get_room_by_doorway_index(map,dest_door->index);
		door->color = dest_room->color;
		if( !(dest_room->has_shop) && !(dest_room->boss_room) &&
				!(dest_room->starting_room)){
			if( dest_room->gs.n_npcs<=0){
				door->color=(struct vector){0.098,0.396,0.384};
			}else if(dest_room->gs.n_npcs>0){
				door->color=(struct vector){0.655,0.161,0.161};
			}
		}
	}
}

void generate_rooms(struct map * map)
{
	int i;
	for(i=0;i<MAX_ROOMS;i++){
		map->room[i]=generate_room(i,map->level);
	}
	map->n_rooms=MAX_ROOMS;
	index_doorways(map);
}

void generate_map(struct map * map,int level)
{
	(*map) = (struct map){0};
	map->level=level;
	map->current_room = &map->room[0];

	generate_rooms(map);
	generate_edges(map);

	map->current_room=&map->room[0];
	update_door_colors(map);
}

int other_edge(struct map * map,int index){
	int i;
	for(i=0;i<map->graph.n_edges;i++){
		if(map->graph.edge[i].a==index){
			return map->graph.edge[i].b;
		}
		if(map->graph.edge[i].b==index){
			return map->graph.edge[i].a;
		}
	}
	return -1;
}

struct doorway * connected_doorway(struct map * map,int i)
{
	int o = other_edge(map,i);
	if(o<0){
		return NULL;
	}else{
		return get_doorway_by_index(map,other_edge(map,i));
	}
}

void transfer_rooms(struct map * map, struct room * dest)
{
	reset_lights();
	dest->gs.game_player = map->current_room->gs.game_player;
	map->current_room=dest;
}

void remove_room(struct map * map,int index)
{
	int i;
	if(index>=(MAX_ROOMS-1))
	{
		map->n_rooms--;
		return;
	}
	for(i=index;(i+1)<map->n_rooms;i++){
		map->room[i] = map->room[i+1];
		if(&map->room[i+1]==map->current_room)
		{
			map->current_room=&map->room[i];
		}
	}
	map->n_rooms--;
}

int should_collapse_room(struct map * map, int i)
{
	return (map->room[i].gs.n_npcs<=0 && 
			(&map->room[i]) != map->current_room && 
			(!map->room[i].has_shop));
}

void collapse_rooms(struct map * map)
{
	int i;
	for(i=0;i<map->n_rooms;i++){
		if(should_collapse_room(map,i)){
			remove_room(map,i);
			index_doorways(map);
			generate_edges(map);
		}
	}
	update_door_colors(map);
}

void move_through_doorway(struct map * map,int t)
{
	struct doorway * dest_door;
	struct room * dest_room;
	int should_collapse=0;
	dest_door = connected_doorway(map,t);
	if(dest_door==NULL){
		printf("NULL doorway\n");
		return;
	}
		
	dest_room = get_room_by_doorway_index(map,dest_door->index);

	if(dest_room!=map->current_room){
		if( (dest_room->gs.n_npcs<=0) && 
				(map->current_room->gs.n_npcs<=0)){
			should_collapse=1;
		}
		transfer_rooms(map,dest_room);
	}

	map->current_room->gs.game_player.location = dest_door->location;
	last_camera = map->current_room->gs.game_player.location;
	map->current_room->gs.game_player.location.x += 1;
	map->current_room->gs.game_player.destination = 
		map->current_room->gs.game_player.location;
	portal_timer = max_portal_timer;

	update_door_colors(map);
}

struct unit buffer_player;

void transfer_map(int level)
{
	buffer_player = world_map.current_room->gs.game_player;
	generate_map(&world_map,level);
	world_map.current_room->gs.game_player = buffer_player;
	world_map.current_room->gs.game_player.location=(struct vector){5,0,5};
}
