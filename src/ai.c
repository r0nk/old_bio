#include "ai.h"
#include "dimensions.h"
#include "engine.h"
#include "game.h"
#include "pathfinding.h"
#include "audio.h"

/* line of sight,boolean if obs can see target */
int los(struct unit * obs, struct unit * target)
{
	if(!wall_intersects_line(obs->location,target->location))
		return 1;
	double fangle = face_angle(obs->location,target->location);
	struct vector left=v_s_mul(target->hit_radius,rot_to_dvec(fangle-90));
	struct vector right=v_s_mul(target->hit_radius,rot_to_dvec(fangle+90));
	right = v_add(right,target->location);
	left  = v_add(left,target->location);
	if(!wall_intersects_line(obs->location,left))
		return 1;
	if(!wall_intersects_line(obs->location,right))
		return 1;
	return 0;
}

struct vector random_walkable_location(struct vector d){
	struct vector v = d;

	int i;
	for(i=0;i<10;i++){
		v=d;
		v.x += (int)((rand()%10)-5);
		v.z += (int)((rand()%10)-5);
		if(walkable((int)v.x,(int)v.z) && !wall_intersects_line(d,v)
				&& !(v_eq(v,d))){
			return v;
		}
	}
	return d;
}

void npc_strafe(struct game_state * gs, double delta, int j)
{
	struct vector d = rot_to_dvec(gs->npc[j].rotation_angle);

	if(gs->npc[j].path_timer>0){
		move_towards_facing(&gs->npc[j],delta);
		gs->npc[j].path_timer-=delta;
		return;
	}
	gs->npc[j].destination=random_walkable_location(
			gs->npc[j].location);
	gs->npc[j].path_timer=((rand()%10)/10.0);

	if(!v_eq(gs->npc[j].location,gs->npc[j].destination))
		face(&gs->npc[j],gs->npc[j].destination);
	move_towards_facing(&gs->npc[j],delta);
}

void npc_chase(struct game_state * gs, double delta, int j)
{
	struct vector d = rot_to_dvec(gs->npc[j].rotation_angle);

	if(gs->npc[j].path_timer>0){
		move_towards_facing(&gs->npc[j],delta);
		gs->npc[j].path_timer-=delta;
		return;
	}

	if(is_near(gs->game_player.location,gs->npc[j].location,20)){
		if(los(&gs->npc[j], &gs->game_player)){
			gs->npc[j].destination=gs->game_player.location;
		}
		if(is_near(gs->npc[j].location,gs->npc[j].destination,0.2))
			return;

		face(&gs->npc[j],gs->npc[j].destination);
		move_towards_facing(&gs->npc[j],delta);
	}
}

void update_spinner_boss(struct game_state * gs, double delta,int j)
{
	struct vector dvec;
	dvec = rot_to_dvec(gs->npc[j].rotation_angle);
	fire_bullet(gs,&gs->npc[j],dvec);
	dvec = rot_to_dvec(gs->npc[j].rotation_angle+180);
	fire_bullet(gs,&gs->npc[j],dvec);

	if(!(rand()%30)){
		gs->npc[j].moving=-gs->npc[j].moving;
	}
	gs->npc[j].rotation_angle+=delta*100*gs->npc[j].moving;
}

void update_spider(struct game_state * gs, double delta, int j)
{
	if(gs->npc[j].cooldown<0){
		gs->npc[j].rotation_angle=rand()%360;
		gs->npc[j].cooldown=((rand()%200)+100)/100;
	}else{
		gs->npc[j].cooldown-=delta;
	}

	if(is_near(gs->game_player.location,gs->npc[j].location,0.5)){
		damage_unit(&gs->game_player,10);
		if(gs->game_player.speed>4)
			gs->game_player.speed-=4;
		damage_unit(&gs->npc[j],10);
	}
	move_towards_facing(&gs->npc[j],delta);
}

void update_antenna(struct game_state * gs, double delta, int j)
{
	gs->npc[j].cooldown+=delta*4;
	field_set(gs,gs->npc[j].location.x,gs->npc[j].location.z,
			(int)gs->npc[j].cooldown2, 
			1.5*sin(gs->npc[j].cooldown));
}

void update_ranger(struct game_state * gs, double delta, int j)
{
	struct vector d;
	if(gs->npc[j].cooldown>0){
		gs->npc[j].cooldown-=delta;
		npc_strafe(gs,delta,j);
		return;
	}
	if(is_near(gs->game_player.location,gs->npc[j].location,15.0)
			&& los(&gs->npc[j],&gs->game_player)){
		face(&gs->npc[j],gs->game_player.location);
		d = rot_to_dvec(gs->npc[j].rotation_angle);
		if(gs->npc[j].cooldown2>0){
			gs->npc[j].cooldown2-=delta;
			return;
		}
		fire_bullet(gs,&gs->npc[j],d);
		gs->npc[j].cooldown2=0.2;
	}
	gs->npc[j].cooldown=gs->npc[j].max_cooldown;
	if(gs->npc[j].speed<gs->npc[j].max_speed)
		gs->npc[j].speed+=delta*5;
	if(gs->npc[j].speed>gs->npc[j].max_speed)
		gs->npc[j].speed=gs->npc[j].max_speed;
}

/* where we think the player will be t seconds from now */
struct vector player_predict(struct game_state * gs,float t)
{
	struct vector d;

	if(!gs->game_player.moving)
		return gs->game_player.location;
	d = gs->game_player.location;

	t*=gs->game_player.speed;

	if(gs->game_player.moving&1){
		d.x-=t;
		d.z-=t;
	}
	if(gs->game_player.moving&2){
		d.x-=t;
		d.z+=t;
	}
	if(gs->game_player.moving&4){
		d.x+=t;
		d.z+=t;
	}
	if(gs->game_player.moving&8){
		d.x+=t;
		d.z-=t;
	}

	return d;
}

float planet_impact_timer=0;

void update_spade_speech(struct game_state * gs,double delta, int j)
{
	if(!gs->npc[j].connected_to &&
			gs->npc[j].speech.line[gs->npc[j].speech.l]==NULL){
		gs->game_player.speech.line[0]="Yeah, neither of us";
		gs->game_player.speech.line[1]="have a choice.";
		gs->npc[j].connected_to=1;
	}
	if(gs->npc[j].connected_to==1 &&
		gs->game_player.speech.line[gs->game_player.speech.l]==NULL){
		gs->npc[j].speech = (struct speech){0};
		gs->npc[j].speech.line[0]="...";
		gs->npc[j].connected_to=2;
	}
	if(gs->npc[j].connected_to==3) {
		gs->game_player.speech = (struct speech){0};
		gs->game_player.speech.line[0]="What was that?";
		gs->npc[j].connected_to=4;
		return;
	}
	if(gs->npc[j].connected_to==4 &&
		gs->game_player.speech.line[gs->game_player.speech.l]==NULL){
		gs->npc[j].speech = (struct speech){0};
		gs->npc[j].speech.line[0]="Oh, you didn't realize?";
		gs->npc[j].speech.line[1]="You fixed the engines too late.";
		gs->npc[j].speech.line[2]="She's ejecting excess mass.";
		gs->npc[j].speech.line[3]="Which includes us.";
		gs->npc[j].speech.line[4]="She betrayed you.";
		gs->npc[j].connected_to=5;
		return;
	}
	if(gs->npc[j].connected_to==5 &&
		gs->npc[j].speech.line[gs->npc[j].speech.l]==NULL){
		gs->game_player.speech = (struct speech){0};
		gs->game_player.speech.line[0]="...";
		gs->game_player.speech.line[1]="You gave her no choice.";
		gs->npc[j].connected_to=6;
		return;
	}
	if(gs->npc[j].connected_to==6 &&
		gs->game_player.speech.line[gs->game_player.speech.l]==NULL){
		gs->npc[j].speech = (struct speech){0};
		gs->npc[j].speech.line[0]="That doesn't change how you feel.";
		gs->npc[j].connected_to=7;
		return;
	}
	if(gs->npc[j].connected_to==7 &&
		gs->npc[j].speech.line[gs->npc[j].speech.l]==NULL){
		gs->npc[j].health=250;
		gs->npc[j].connected_to=8;
		planet_impact_timer=30;
		return;
	}
	if(gs->npc[j].connected_to==8 && planet_impact_timer<=7.5){
		gs->npc[j].speech = (struct speech){0};
		gs->npc[j].speech.line[0]="five ";
		gs->npc[j].speech.line[1]="four ";
		gs->npc[j].speech.line[2]="three ";
		gs->npc[j].speech.line[3]="two ";
		gs->npc[j].speech.line[4]="one ";
		gs->npc[j].connected_to=9;
		return;
	}
}

void update_spade(struct game_state * gs, double delta, int j)
{
	struct vector d;
	float dist;
	if(gs->npc[j].health>250){
		update_spade_speech(gs,delta,j);
		return;/*Wait for the player to attack us*/
	}

	if(planet_impact_timer>0.0){
		planet_impact_timer-=delta;
		if(planet_impact_timer<0.0){
			game_over(gs,1);
			play_planet_impact_sound_effect();
			stop_sounds();
			set_music(0);
		}else{
			update_spade_speech(gs,delta,j);
		}
		gs->npc[j].health=230;
	}

	if(gs->npc[j].cooldown>0){
		gs->npc[j].cooldown-=delta;
		npc_strafe(gs,delta,j);
		gs->npc[j].moving=1;
		return;
	}
	if(is_near(gs->game_player.location,gs->npc[j].location,15.0)
			&& los(&gs->npc[j],&gs->game_player)){
		gs->npc[j].moving=0;
		dist = distance(gs->game_player.location,gs->npc[j].location);
		face(&gs->npc[j],player_predict(gs,dist/(40)));
		d = rot_to_dvec(gs->npc[j].rotation_angle);
		if(gs->npc[j].cooldown2>0){
			gs->npc[j].cooldown2-=delta;
			return;
		}
		fire_bullet(gs,&gs->npc[j],d);
		gs->npc[j].cooldown2=0.25;
	}
	gs->npc[j].cooldown=0.5;
}

void fire_npc_missile(struct game_state * gs, double delta, int j)
{
	struct vector v;
	struct missile m = {0};
	if(gs->n_npcs<=0)
		return;
	m.target = &gs->game_player;
	m.direction = gs->npc[j].rotation_angle;
	m.location = gs->npc[j].location;
	m.primer=0;

	add_missile(gs,m);
	play_rocket_sound_effect();
}

void update_tank(struct game_state * gs, double delta, int j)
{
	if(!is_near(gs->game_player.location,gs->npc[j].location,2.0)){
		npc_chase(gs,delta,j);
	}

	if(is_near(gs->game_player.location,gs->npc[j].location,15.0)
			&& los(&gs->npc[j],&gs->game_player)){
		face(&gs->npc[j],gs->game_player.location);
		if(gs->npc[j].cooldown>0){
			gs->npc[j].cooldown-=delta;
		}else{
			fire_npc_missile(gs,delta,j);
			gs->npc[j].cooldown=3;
		}
	}
}

void update_shotty_ranger(struct game_state * gs, double delta, int j)
{
	if(gs->npc[j].cooldown>0){
		gs->npc[j].cooldown-=delta;
		npc_strafe(gs,delta,j);
		return;
	}
	if(is_near(gs->game_player.location,gs->npc[j].location,7.5)
			&& los(&gs->npc[j],&gs->game_player)){
			face(&gs->npc[j],gs->game_player.location);
			if(gs->npc[j].cooldown2>0){
				gs->npc[j].cooldown2-=delta;
				return;
			}
			fire_shotty(gs,gs->npc[j],
					rot_to_dvec(gs->npc[j].rotation_angle));
			gs->npc[j].cooldown2=0.25;
	}
	gs->npc[j].cooldown=0.5;
	if(gs->npc[j].speed<gs->npc[j].max_speed)
		gs->npc[j].speed+=delta*5;
	if(gs->npc[j].speed>gs->npc[j].max_speed)
		gs->npc[j].speed=gs->npc[j].max_speed;
}
void update_boss(struct game_state * gs, double delta, int j)
{
	static double spawn_health=0;
	struct unit npc;
	struct vector loc;

	gs->npc[j].rotation_angle+=delta*180;
	if(gs->npc[j].cooldown>0){
		gs->npc[j].cooldown-=delta;
		return;
	}
	else{
		spawn_health+=0.2;
	}
	loc = gs->npc[j].location;
	loc.x+=(rand()%5)-2;
	loc.z+=(rand()%5)-2;
	gs->npc[j].cooldown=2;
	npc = scavenger_npc(loc);
	npc.health+=(int)spawn_health;
	npc.max_health+=(int)spawn_health;
	add_npc(gs,npc);
}

void update_item_npc(struct game_state * gs, double delta, int j)
{
	struct vector dvec;
	dvec=rot_to_dvec(face_angle(gs->npc[j].location,gs->game_player.location));
	gs->npc[j].rotation_angle += 90*delta;
	if(is_near(gs->game_player.location, gs->npc[j].location,1.0)){
		play_coin_sound_effect();
		gs->game_player.score+=gs->npc[j].score;
		remove_npc(gs,j);
		gs->game_player.coins++;
		return;
	}
	if(is_near(gs->game_player.location, gs->npc[j].location,5)){
		dvec = v_s_mul(0.05*(5-distance(gs->game_player.location, gs->npc[j].location)),dvec);
		gs->npc[j].location=v_add(gs->npc[j].location,dvec);
	}
	gs->npc[j].cooldown-=delta;
	if(gs->npc[j].cooldown<0)
		remove_npc(gs,j);
}

void update_shieldmans_shield(struct game_state * gs,double delta,int j)
{
	/*figure out the shield line*/
	double gsd = gs->npc[j].rotation_angle;
	double r1 = ((45)+gsd);
	double r2 = ((-45)+gsd);
	struct vector s1 = rot_to_dvec(r1);
	struct vector s2 = rot_to_dvec(r2);

	s1 = v_add(gs->npc[j].location,s1);
	s2 = v_add(gs->npc[j].location,s2);

	struct vector b1,b2;
	int i;
	for(i=0;i<gs->n_bullets;i++){
		b1 = gs->bullet[i].location;
		b2 = v_add(b1,gs->bullet[i].direction);
		if(line_intersects_line(s1,s2,b1,b2)){
			gs->bullet[i].direction.x *=-1;
			gs->bullet[i].direction.z *=-1;
			gs->bullet[i].shooter = &gs->npc[j];
		}
	}
}

void update_shieldman(struct game_state * gs, double delta, int j)
{
	if(gs->npc[j].cooldown2<=0)
		npc_chase(gs,delta,j);
	if(is_near(gs->game_player.location,gs->npc[j].location,1.5)){
		if(gs->npc[j].cooldown<=0){
			gs->npc[j].cooldown=2.0;
			gs->npc[j].cooldown2=1.0;
		}else{
			if(gs->npc[j].cooldown2<0.75 &&
					gs->npc[j].cooldown2>0.25){
				damage_unit(&gs->game_player,
						delta*gs->npc[j].damage*
						gs->game_player.resist);
			}
		}
	}else{
		if(gs->npc[j].cooldown2<=0)
			update_shieldmans_shield(gs,delta,j);
	}
	if(gs->npc[j].cooldown>=0)
		gs->npc[j].cooldown-=delta;
	if(gs->npc[j].cooldown2>=0)
		gs->npc[j].cooldown2-=delta;
	if(gs->npc[j].speed<gs->npc[j].max_speed)
		gs->npc[j].speed+=delta*5;
	if(gs->npc[j].speed>gs->npc[j].max_speed)
		gs->npc[j].speed=gs->npc[j].max_speed;
}

void update_scavenger(struct game_state * gs, double delta, int j)
{
	npc_chase(gs,delta,j);
	if(is_near(gs->game_player.location,gs->npc[j].location,1.5)){
		damage_unit(&gs->game_player,
				delta*gs->npc[j].damage*gs->game_player.resist);
	}
	if(gs->npc[j].speed<gs->npc[j].max_speed)
		gs->npc[j].speed+=delta*10;
	if(gs->npc[j].speed>gs->npc[j].max_speed)
		gs->npc[j].speed=gs->npc[j].max_speed;
}

void update_spinner(struct game_state * gs, double delta,int j)
{
	struct vector d;
	d = rot_to_dvec(gs->npc[j].rotation_angle);
	gs->npc[j].rotation_angle+=delta*250;
	fire_bullet(gs,&gs->npc[j],d);
}

void teleport_mole(struct unit * mole)
{
	switch(rand()%4)
	{
		case 0:
			mole->location = (struct vector) {5,0,5};
			break;
		case 1:
			mole->location = (struct vector) {5,0,10};
			break;
		case 2:
			mole->location = (struct vector) {10,0,5};
			break;
		case 3:
			mole->location = (struct vector) {10,0,10};
			break;
	}
	mole->cooldown2 = 2;
}

void update_mole(struct game_state * gs, double delta,int j)
{
	struct vector d = rot_to_dvec(gs->npc[j].rotation_angle);

	if(gs->npc[j].cooldown2 > 0){
		gs->npc[j].cooldown2-=delta;
	}else{
		teleport_mole(&gs->npc[j]);
	}

	face(&gs->npc[j],gs->game_player.location);
	if(gs->npc[j].cooldown>0){
		gs->npc[j].cooldown-=delta;
	}else{
		fire_bullet(gs,&gs->npc[j],d);
		gs->npc[j].cooldown=0.4;
	}
}

void update_yo(struct game_state * gs, double delta, int j)
{
	/* wheter or not we're the (top_left|bottom_right) one */
	int w = (j < (gs->npc[j].connected_to))? 1 : 0;

	struct vector a = gs->npc[j].location;
	struct vector b = gs->npc[gs->npc[j].connected_to].location;
	if(unit_intersects_line(&gs->game_player,a,b)){
		damage_unit(&gs->game_player,
				delta*gs->npc[j].damage*gs->game_player.resist);
	}

	gs->npc[j].speed = 5+((100-(gs->npc[j].health))/4);

	/*
	   i---o
	   |   |
	   |   |
	   k---l
	 */
	int i = (b.x==1 && b.z==1);
	int o = (b.x==11 && b.z==1);
	int k = (b.x==1 && b.z==11);
	int l = (b.x==11 && b.z==11);

	double d = gs->npc[j].speed * delta;

	if(w){
		if(o){
			if(is_near(gs->npc[j].location,(struct vector) {11,0,11},1)){
				gs->npc[j].location=(struct vector) {11,0,11};
				return;
			}

			gs->npc[j].location.x+=d;
			gs->npc[j].location.z+=d;
		}else if (k){
			if(is_near(gs->npc[j].location,(struct vector) {1,0,1},1)){
				gs->npc[j].location=(struct vector) {1,0,1};
				return;
			}
			gs->npc[j].location.x-=d;
			gs->npc[j].location.z-=d;
		}
	}else{
		if(i){
			if(is_near(gs->npc[j].location,(struct vector) {11,0,1},1)){
				gs->npc[j].location=(struct vector) {11,0,1};
				return;
			}
			gs->npc[j].location.x+=d;
			gs->npc[j].location.z-=d;
		}else if (l){
			if(is_near(gs->npc[j].location,(struct vector) {1,0,11},1)){
				gs->npc[j].location=(struct vector) {1,0,11};
				return;
			}
			gs->npc[j].location.x-=d;
			gs->npc[j].location.z+=d;
		}
	}
}

void update_double_sentry(struct game_state * gs, double delta, int j)
{
	struct vector d = rot_to_dvec(gs->npc[j].rotation_angle);

	struct vector p1 = rot_to_dvec(gs->npc[j].rotation_angle+90);
	struct vector p2 = rot_to_dvec(gs->npc[j].rotation_angle-90);
	p1 = v_s_mul(0.5,p1);
	p2 = v_s_mul(0.5,p2);
	p1 = v_add(gs->npc[j].location,p1);
	p2 = v_add(gs->npc[j].location,p2);

	if(is_near(gs->game_player.location,gs->npc[j].location,20.0)
			&& los(&gs->npc[j],&gs->game_player)){
		face(&gs->npc[j],gs->game_player.location);
		if(gs->npc[j].cooldown>0){
			gs->npc[j].cooldown-=delta;
		}else{
			fire_bullet_at(gs,p1,&gs->npc[j],d);
			gs->npc[j].cooldown=gs->npc[j].max_cooldown;
			if(gs->npc[j].max_cooldown>0.3){
				gs->npc[j].max_cooldown-=0.2;
			}
		}
		if(gs->npc[j].cooldown2>0){
			gs->npc[j].cooldown2-=delta;
		}else{
			fire_bullet_at(gs,p2,&gs->npc[j],d);
			gs->npc[j].cooldown2=gs->npc[j].max_cooldown;
			if(gs->npc[j].max_cooldown>0.3){
				gs->npc[j].max_cooldown-=0.2;
			}
		}
	} else {
		gs->npc[j].max_cooldown=1.0;
	}
}

void update_sentry(struct game_state * gs, double delta, int j)
{
	struct vector d;
	d = rot_to_dvec(gs->npc[j].rotation_angle);

	if(is_near(gs->game_player.location,gs->npc[j].location,20.0)
			&& los(&gs->npc[j],&gs->game_player)){
		face(&gs->npc[j],gs->game_player.location);
		if(gs->npc[j].cooldown>0){
			gs->npc[j].cooldown-=delta*1.5;
		}else{
			fire_bullet(gs,&gs->npc[j],d);
			gs->npc[j].cooldown=gs->npc[j].max_cooldown;
			if(gs->npc[j].max_cooldown>0.3){
				gs->npc[j].max_cooldown-=0.2;
			}
		}
	} else {
		gs->npc[j].max_cooldown=1.0;
	}
}

void update_npcs(struct game_state * gs, double delta)
{
	int j;
	for(j=0;j<gs->n_npcs;j++){
		if(gs->npc[j].health<=0){
			death(gs,j);
		}
		if(gs->npc[j].poison_timer>0.0){
			damage_unit(&gs->npc[j],delta*2);
			gs->npc[j].poison_timer-=delta;
		}
		flaming_effect(gs,delta,&gs->npc[j]);

		switch(gs->npc[j].type){
			case UNIT_TYPE_YO:
				update_yo(gs,delta,j);
				break;
			case UNIT_TYPE_SHIELDMAN:
				update_shieldman(gs,delta,j);
				personal_space(gs,delta,j);
				break;
			case UNIT_TYPE_NEUTRAL_CREEP:
				update_scavenger(gs,delta,j);
				personal_space(gs,delta,j);
				break;
			case UNIT_TYPE_MOLE:
				update_mole(gs,delta,j);
				break;
			case UNIT_TYPE_RANGER:
				update_ranger(gs,delta,j);
				personal_space(gs,delta,j);
				break;
			case UNIT_TYPE_SHOTTY_RANGER:
				update_shotty_ranger(gs,delta,j);
				personal_space(gs,delta,j);
				break;
			case UNIT_TYPE_ITEM:
				update_item_npc(gs,delta,j);
				break;
			case UNIT_TYPE_BOSS:
				update_boss(gs,delta,j);
				break;
			case UNIT_TYPE_SIGN:
				break;
			case UNIT_TYPE_ANTENNA:
				update_antenna(gs,delta,j);
				break;
			case UNIT_TYPE_SPIDER:
				update_spider(gs,delta,j);
				break;
			case UNIT_TYPE_SENTRY:
				update_sentry(gs,delta,j);
				break;
			case UNIT_TYPE_DOUBLE_SENTRY:
				update_double_sentry(gs,delta,j);
				break;
			case UNIT_TYPE_SPINNER:
				update_spinner(gs,delta,j);
				break;
			case UNIT_TYPE_SPINNER_BOSS:
				update_spinner_boss(gs,delta,j);
				break;
			case UNIT_TYPE_TANK:
				update_tank(gs,delta,j);
				personal_space(gs,delta,j);
				break;
			case UNIT_TYPE_SPADE:
				update_spade(gs,delta,j);
				break;
		}
	}
}
