#include <stdio.h>
#include <SDL2/SDL.h>
#include <math.h>

#include "engine.h"
#include "save.h"
#include "ai.h"
#include "camera.h"
#include "card.h"
#include "pathfinding.h"
#include "game_state.h"
#include "room.h"
#include "callbacks.h"
#include "audio.h"
#include "input.h"
#include "map.h"
#include "effects.h"
#include "ui.h"
#include "field.h"
#include "particle.h"
#include "neighborhood.h"


void face(struct unit * u, point p)
{
	u->rotation_angle = face_angle(u->location,p);
	u->rotation = (struct vector) {0,1,0};
}

/*copy-pasted from the webs*/
int line_intersects_line(struct vector a, struct vector b,
		struct vector c, struct vector d)
{
	double denominator=((b.x - a.x)*(d.z - c.z))-((b.z - a.z)*(d.x - c.x));
	double numerator1=((a.z - c.z)*(d.x - c.x))-((a.x - c.x)*(d.z - c.z));
	double numerator2=((a.z - c.z)*(b.x - a.x))-((a.x - c.x)*(b.z - a.z));

	if (denominator == 0)
		return (numerator1 == 0 && numerator2 == 0);

	double r = numerator1 / denominator;
	double s = numerator2 / denominator;

	return ((r >= 0 && r <= 1) && (s >= 0 && s <= 1));
}

int unit_intersects_line(struct unit * u, struct vector a, struct vector b)
{
	/*
	   h     j
	   +-----+
	   |  .  |
	   +-----+
	   k     l
	 */
	struct vector h=u->location,j=u->location,k=u->location,l=u->location;
	h.x-=u->hit_radius;
	j.x+=u->hit_radius;
	k.x-=u->hit_radius;
	l.x+=u->hit_radius;

	h.z+=u->hit_radius;
	j.z+=u->hit_radius;
	k.z-=u->hit_radius;
	l.z-=u->hit_radius;

	if(line_intersects_line(a,b,h,j))
		return 1;
	if(line_intersects_line(a,b,h,k))
		return 1;
	if(line_intersects_line(a,b,k,l))
		return 1;
	if(line_intersects_line(a,b,j,l))
		return 1;

	return 0;
}

int line_intersects_box(struct vector boxloc,struct vector a, struct vector b)
{
	/*
	   h     j
	   +-----+
	   |  .  |
	   +-----+
	   k     l
	 */
	struct vector h=boxloc,j=boxloc,k=boxloc,l=boxloc;
	j.x+=1.0;
	l.x+=1.0;

	k.z+=1.0;
	l.z+=1.0;

	if(line_intersects_line(a,b,h,j))
		return 1;
	if(line_intersects_line(a,b,h,k))
		return 1;
	if(line_intersects_line(a,b,k,l))
		return 1;
	if(line_intersects_line(a,b,j,l))
		return 1;

	return 0;
}

int layout_wall_intersects_line(struct layout * l, struct vector a, struct vector b)
{
	int i,j;
	int max_i,max_j;

	max_i= (a.x>b.x)?(int)a.x:(int)b.x;
	max_i++;
	if(max_i > MAX_ROOM_WIDTH)
		max_i=MAX_ROOM_WIDTH;
	max_j= (a.z>b.z)?(int)a.z:(int)b.z;
	max_j++;
	if(max_j > MAX_ROOM_HEIGHT)
		max_j=MAX_ROOM_HEIGHT;

	i = (a.x<b.x)?(int)a.x:(int)b.x;
	for(;i<max_i;i++){
		j = (a.z<b.z)?(int)a.z:(int)b.z;
		for(;j<max_j;j++){
			if(l->tiles[i][j]!='#')
				continue;
			if(line_intersects_box((struct vector) {i,0,j},a,b))
				return 1;
		}
	}
	return 0;
}

int wall_intersects_line(struct vector a, struct vector b)
{
	return layout_wall_intersects_line(&world_map.current_room->layout,a,b);
}

int door_at(struct vector l)
{
	int i;
	float d;
	float lowest_d=99999;
	for(i=0;i<world_map.current_room->n_doorways;i++){
		if(!world_map.current_room->doorway[i].is_connected){
			continue;
		}
		d=distance(world_map.current_room->doorway[i].location,l);
		if(d<lowest_d)
			lowest_d=d;
		if(d<1.0){
			set_brownian_volume(0);
			return world_map.current_room->doorway[i].index;
		}
	}
	set_brownian_volume(lowest_d);
	return -1;
}

void player_dealt_damage(struct game_state * gs, float amount)
{
	int i;
	if(gs->game_player.flags&HAS_LIFESTEAL){
		global_save_state.life_stolen++;
		heal_unit(&gs->game_player,amount);
	}
	if(gs->game_player.overclock>0){
		for(i=0;i<4;i++){
			if(gs->game_player.inventory.item[i].cooldown>0){
				gs->game_player.inventory.item[i].cooldown-=
					amount*gs->game_player.overclock*1.0;
			}
		}
	}
}

void aoe_damage(struct game_state * gs,struct vector location,
		int amount, int radius)
{
	int i;
	for(i=0;i<gs->n_npcs;i++){
		if(is_near(gs->npc[i].location,location,radius)){
			player_dealt_damage(gs,amount);
			damage_unit(&gs->npc[i],amount);
		}
	}
}

/*bounce a bullet off a wall*/
void bounce(struct game_state * gs, int i,double delta)
{
	double s = gs->bullet[i].speed*delta;
	double sx = gs->bullet[i].direction.x*s;
	double sz = gs->bullet[i].direction.z*s;
	double x = gs->bullet[i].location.x-sx;
	double z = gs->bullet[i].location.z-sz;
	struct layout * l = &(world_map.current_room->layout);
	if(l->tiles[(int)(x+sx)][(int)z]==LAYOUT_WALL)
	{
		gs->bullet[i].direction.x = -(gs->bullet[i].direction.x);
		return;
	}
	if(l->tiles[(int)x][(int)(z+sz)]==LAYOUT_WALL)
	{
		gs->bullet[i].direction.z = -(gs->bullet[i].direction.z);
		return;
	}
}

void missile_npc_check(struct game_state * gs, double delta, int i)
{
	if(is_near(gs->missile[i].location, gs->missile[i].target->location,
				gs->missile[i].target->hit_radius)){
		explode_missile(gs,i);
	}
}

void bullet_npc_check(struct game_state * gs, double delta, int i)
{
	int j;
	for(j=0;j<gs->n_npcs;j++){
		if(is_near(gs->bullet[i].location, gs->npc[j].location,
					gs->npc[j].hit_radius)){
			if(gs->bullet[i].shooter!=&gs->game_player)
				return;

			if(gs->npc[j].type == UNIT_TYPE_NEUTRAL_CREEP){
				gs->npc[j].speed=0;
			}


			gs->npc[j].hit_timer=0.1;
			damage_unit(&gs->npc[j],gs->bullet[i].damage);
			if(gs->bullet[i].flags&HAS_VAIL)
				gs->npc[j].poison_timer+=5;
			if(gs->bullet[i].flags&HAS_VECTOR_FIELD)
				aoe_damage(gs,gs->bullet[i].location,5,3);
			if(gs->bullet[i].flags&HAS_LIFESTEAL){
				global_save_state.life_stolen++;
				heal_unit(gs->bullet[i].shooter,
					  (rand()%((int)gs->bullet[i].damage)));
			}
			if(gs->npc[j].health<=0){
				global_save_state.blaster_kills++;
				write_save_state();
			}
			play_hit_sound_effect();
			bullet_hit_sparks(gs,i,delta);
			impact_chunks(gs,j);
			remove_bullet(gs,i);
		}
	}
}

double spark_dir()
{
	return ((rand()%20)-10)/20.0;
}

float roundy(float f)
{
	float buf = f;
	float rounded = round(f);
	if(rounded>buf)
		return (rounded+0.0001);
	else
		return (rounded-0.0001);
}

struct vector bullet_hole_location(struct game_state * gs, int i, double delta)
{
	struct vector loc;
	double s = gs->bullet[i].speed*delta;
	double sx = gs->bullet[i].direction.x*s;
	double sz = gs->bullet[i].direction.z*s;
	double x = gs->bullet[i].location.x-sx;
	double z = gs->bullet[i].location.z-sz;
	struct layout * l = &(world_map.current_room->layout);

	sx*=0.1;
	sz*=0.1;

	int k;
	for(k=0;k<200;k++){
		if(l->tiles[(int)(x)][(int)z]==LAYOUT_WALL) {
			loc.x = x-sx;
			loc.z = z-sz;
			return loc;
		}
		x+=sx;
		z+=sz;
	}
	return loc;
}

void add_bullet_hole(struct game_state * gs,int i,double delta)
{
	double s = gs->bullet[i].speed*delta;
	double sx = gs->bullet[i].direction.x*s;
	double sz = gs->bullet[i].direction.z*s;
	double x = gs->bullet[i].location.x-sx;
	double z = gs->bullet[i].location.z-sz;
	struct layout * l = &(world_map.current_room->layout);
	struct room * r = world_map.current_room;
	int rando = rand()%MAX_PERMANENTS;
	r->permanents[rando].location = bullet_hole_location(gs,i,delta);
	r->permanents[rando].alive=1;
	r->permanents[rando].frame=0;
	r->permanents[rando].type=(rand()%4);
	r->permanents[rando].kind=PKIND_BULLET;
	if(l->tiles[(int)(x+sx)][(int)z]==LAYOUT_WALL) {
		r->permanents[rando].direction = (struct vector) {0,1,1};
	}else if(l->tiles[(int)x][(int)(z+sz)]==LAYOUT_WALL) {
		r->permanents[rando].direction = (struct vector) {1,1,0};
	}
}

void add_scorch(struct vector location)
{
	struct layout * l = &(world_map.current_room->layout);
	struct room * r = world_map.current_room;
	int rando = rand()%MAX_PERMANENTS;
	r->permanents[rando].location = location;
	r->permanents[rando].location.y += 0.002+((rand()%10)*0.0001);
	r->permanents[rando].alive=1;
	r->permanents[rando].frame=0;
	r->permanents[rando].type=(rand()%4);
	r->permanents[rando].kind=PKIND_SCORCH;
}

void bullet_wall_sparks(struct game_state * gs, int j, double delta)
{
	if(!(rand()%10) && gs->bullet[j].damage!=3){
		add_bullet_hole(gs,j,delta);
	}
	bounce(gs,j,delta);
	int i,k=0;
	for(i=rand()%MAX_SPARKS;i<MAX_SPARKS;i++){
		k++;
		if(k>10)
			return;
		spark[i].alive=1;
		spark[i].frame=0;
		spark[i].location=gs->bullet[j].location;
		spark[i].direction=gs->bullet[j].direction;
		spark[i].direction.x+=spark_dir();
		spark[i].direction.z+=spark_dir();
	}
}

void bullet_hit_sparks(struct game_state * gs, int j, double delta)
{
	int i,k=0;
	for(i=rand()%MAX_SPARKS;i<MAX_SPARKS;i++){
		k++;
		if(k>10)
			return;
		spark[i].alive=1;
		spark[i].frame=0;
		spark[i].location=gs->bullet[j].location;
		spark[i].direction=gs->bullet[j].direction;
		spark[i].direction.x+=spark_dir();
		spark[i].direction.z+=spark_dir();
	}
}

int wall_check(struct game_state * gs, struct vector l)
{
	if(world_map.current_room->layout.tiles [(int)l.x] [(int)l.z]
			== LAYOUT_WALL){
		return 1;
	}
	return 0;
}

void bullet_wall_check(struct game_state * gs, double delta, int i)
{
	if(world_map.current_room->layout.tiles
			[(int)gs->bullet[i].location.x]
			[(int)gs->bullet[i].location.z] == LAYOUT_WALL){
		if(gs->bullet[i].flags&HAS_BOUNCE){
			bounce(gs,i,delta);
			gs->bullet[i].duration--;
			if(gs->bullet[i].duration<=0){
				remove_bullet(gs,i);
			}
		}else{
			bullet_wall_sparks(gs,i,delta);
			remove_bullet(gs,i);
			return;
		}
	}else if(world_map.current_room->layout.tiles
			[(int)gs->bullet[i].location.x]
			[(int)gs->bullet[i].location.z] == 0){
		remove_bullet(gs,i);
	}
}

void player_bullet_check(struct game_state * gs, double delta, int i)
{
	if(is_near(gs->bullet[i].location,gs->game_player.location,1.0)){
		gs->game_player.hit_timer=0.1;
		damage_unit(&gs->game_player,gs->bullet[i].damage);
		bullet_hit_sparks(gs,i,delta);
		remove_bullet(gs,i);
		return;
	}
}

void update_bullet(struct game_state * gs, double delta,int i)
{
	double s;
	s = gs->bullet[i].speed*delta;
	gs->bullet[i].location.x+=gs->bullet[i].direction.x*s;
	gs->bullet[i].location.z+=gs->bullet[i].direction.z*s;

	gs->bullet[i].duration-=delta;
	if(gs->bullet[i].duration<0){
		remove_bullet(gs,i);
		return;
	}

	bullet_wall_check(gs,delta,i);

	if(gs->bullet[i].shooter==&gs->game_player){
		bullet_npc_check(gs,delta,i);
	}else{
		player_bullet_check(gs,delta,i);
	}
}

void push_away_particles(struct vector location)
{
	int i;
	struct room * room = world_map.current_room;
	struct vector away;
	double angle;
	double d;
	for(i=0;i<MAX_CHUNKS;i++){
		if(!room->chunks[i].alive)
			continue;
		if(room->chunks[i].type==CHUNK_DOT)
			continue;
		d=distance(location,room->chunks[i].location);

		if(d>5.0)
			continue;

		angle = face_angle(location,room->chunks[i].location);
		away = rot_to_dvec(angle);
		away.x*=(1.0/d);
		away.z*=(1.0/d);
		room->chunks[i].direction = v_add(away,
				room->chunks[i].direction);
		room->chunks[i].direction.y+=1.6;
		room->chunks[i].location.y+=0.2;
	}
	for(i=0;i<MAX_ROOM_LIGHTS;i++){
		d=distance(location,room->light[i].location);
		if(d>10.0)
			continue;
		angle = face_angle(location,room->light[i].location);
		away = rot_to_dvec(angle);
		away = v_s_mul(50.0/d,away);

		room->light[i].velocity = v_add(away,room->light[i].velocity);
	}
}

void explosion_graphic(struct vector location)
{
	int rando = rand()%MAX_EXPLOSIONS;
	explosion[rando].alive=1;
	explosion[rando].frame=0;
	explosion[rando].location=location;
	explosion[rando].direction=(struct vector) {0,0,0};
	explosion[rando].type=(rand()%7)+1;
	explosion[rando].size=(struct vector){2,2,2};
	push_away_particles(location);
	int i,k=0;
	for(i=rand()%MAX_SPARKS;i<MAX_SPARKS;i++){
		k++;
		if(k>20)
			return;
		spark[i].alive=1;
		spark[i].frame=0;
		spark[i].location=location;
		spark[i].direction=random_direction();
	}
}

void explode_missile(struct game_state * gs, int i)
{
	double d;
	int j;

	explosion_graphic(gs->missile[i].location);

	for(j=0;j<gs->n_npcs;j++){
		d=10-distance(gs->npc[j].location,gs->missile[i].location);
		if(d<0 || wall_intersects_line(gs->npc[j].location,
					gs->missile[i].location)){
			continue;
		}
		player_dealt_damage(gs,d);
		damage_unit(&gs->npc[j],d);
	}
	d=10-distance(gs->game_player.location,gs->missile[i].location);
	if(!(d<0) && !(wall_intersects_line(gs->game_player.location,
					gs->missile[i].location))){
		damage_unit(&gs->game_player,d);
	}

	remove_missile(gs,i);
}

double correct_missile(struct game_state * gs, double delta, int i)
{
	double mts = 160;//missile turn speed
	double fa = face_angle(gs->missile[i].location,
			gs->missile[i].target->location);
	fa = correct_angle(fa);

	gs->missile[i].direction=correct_angle(gs->missile[i].direction);

	gs->missile[i].direction+=delta*mts*
		angle_wise(gs->missile[i].direction,fa);
}

void missile_sparks(struct game_state * gs, double delta, int j)
{
	int i;
	i=rand()%MAX_SPARKS;
	spark[i].alive=1;
	spark[i].frame=0;
	spark[i].location=gs->missile[j].location;
	spark[i].location.y+=0.4;
	spark[i].direction=v_neg(gs->bullet[j].direction);
	spark[i].direction.x+=spark_dir();
	spark[i].direction.z+=spark_dir();
}

void update_missile(struct game_state * gs, double delta, int i)
{
	double missile_speed = 15.0;
	correct_missile(gs,delta,i);
	struct vector dvec = rot_to_dvec(gs->missile[i].direction);
	gs->missile[i].location.x+= dvec.x*delta*missile_speed;
	gs->missile[i].location.z+= dvec.z*delta*missile_speed;
	missile_sparks(gs,delta,i);

	if(wall_check(gs,gs->missile[i].location))
		explode_missile(gs,i);
	if(gs->missile[i].primer>0.2){
		missile_npc_check(gs,delta,i);
	}else{
		gs->missile[i].primer+=delta;
	}
}

void update_bullets(struct game_state * gs, double delta)
{
	int i;
	for(i=0;i<gs->n_bullets;i++){
		update_bullet(gs,delta,i);
	}
}

void update_missiles(struct game_state * gs, double delta)
{
	int i;
	for(i=0;i<gs->n_missiles;i++){
		update_missile(gs,delta,i);
	}
}

double spread()
{
	int a = 150;
	return ((rand()%a)-(a/2))/200.0;
}

void fire_shotty(struct game_state * gs,
		struct unit u, struct vector direction)
{
	int i;
	for(i=0;i<8;i++){
		struct bullet b = {0};
		b.location.x = u.location.x+(2*direction.x);
		b.location.y = u.location.y+(2*direction.y);
		b.location.z = u.location.z+(2*direction.z);
		b.direction = direction;
		b.direction.x+= spread();
		b.direction.z+= spread();

		normalize(&b.direction);

		b.speed = 20;
		b.duration = 10;
		b.damage = 1;
		b.flags=u.flags;
		add_bullet(gs,b);
	}
}

void muzzle_flash(struct vector location,struct vector direction){
	int r = rand()%MAX_MUZZLE;
	muzzle[r].frame=0;
	muzzle[r].alive=2;
	direction.x/=2;
	direction.y/=2;
	direction.z/=2;
	muzzle[r].location=v_add(location,direction);
	muzzle[r].location.y+=1;
	muzzle[r].direction=direction;
}

void fire_bullet_at(struct game_state * gs, struct vector location,
		struct unit * u, struct vector direction){
	struct bullet b = {0};
	b.location = location;
	b.direction = direction;
	b.direction.x+=((rand()%10)-5)/100.0;
	b.direction.z+=((rand()%10)-5)/100.0;
	if(wall_intersects_line(b.location,v_add(b.location,b.direction))){
		return;
	}
	b.speed = u->bullet_speed;
	b.duration = 10;
	if(u->bullet_duration)
		b.duration = u->bullet_duration;
	b.damage = u->damage;
	b.flags=u->flags;
	b.shooter = u;
	add_bullet(gs,b);
	if(u->type!=UNIT_TYPE_SPINNER){
		play_laser_sound_effect(
				distance(gs->game_player.location,location));
		muzzle_flash(b.location,b.direction);
	}
}

void fire_bullet(struct game_state * gs,
		struct unit *u, struct vector direction)
{
	fire_bullet_at(gs,u->location,u,direction);
}

void move_unit(struct unit * u,struct vector d)
{
	struct vector vec;
	if(isnan(d.x) || isnan(d.z)){
		printf("NAN d passed to move_unit()\n");
		return;
	}

	vec.x = (d.x>0) ? 1 : -1;
	vec.z = (d.z>0) ? 1 : -1;
	d.x*=u->speed;
	d.z*=u->speed;
	struct layout  * l = &(world_map.current_room->layout);
	if((u->location.x+d.x)>MAX_ROOM_WIDTH)
		return;
	if((u->location.z+d.z)>MAX_ROOM_HEIGHT)
		return;
	if((u->location.x+d.x)<0)
		return;
	if((u->location.z+d.z)<0)
		return;
	if(
			(l->tiles[(int)(u->location.x+d.x)][(int)(u->location.z)]!= LAYOUT_WALL)&&
			(l->tiles[(int)(u->location.x+vec.x)][(int)(u->location.z)]!= LAYOUT_WALL)
	  ){
		u->location.x+=d.x;
	}

	if(
			(l->tiles[(int)(u->location.x)][(int)(u->location.z+d.z)]!= LAYOUT_WALL)&&
			(l->tiles[(int)(u->location.x)][(int)(u->location.z+vec.z)]!= LAYOUT_WALL)
	  ){
		u->location.z+=d.z;
	}

	if(!l->tiles[(int)(u->location.x)][(int)(u->location.z)])
		u->health=0;/* oops, they went out of the map */
}

void field_set(struct game_state * gs, int x, int z,int freq,float amount)
{
	struct field * f;
	switch(freq)
	{
		case 0:
			f = &gs->rfield;
			break;
		case 1:
			f = &gs->gfield;
			break;
		case 2:
			f = &gs->bfield;
			break;
		default:
			printf("unrecognized field specified");
	}
	f->cell[x][z].y=amount;
}

void splash (struct game_state * gs, int x, int z,int freq)
{
	struct field * f;
	switch(freq)
	{
		case 0:
			f = &gs->rfield;
			break;
		case 1:
			f = &gs->gfield;
			break;
		case 2:
			f = &gs->bfield;
			break;
		default:
			printf("unrecognized field specified");
	}
	f->cell[x][z].y=3;
	f->cell[x+1][z].y=2;
	f->cell[x+1][z-1].y=1.4141;
	f->cell[x+1][z+1].y=1.4141;
	f->cell[x-1][z].y=2;
	f->cell[x-1][z+1].y=1.4141;
	f->cell[x-1][z-1].y=1.4141;
	f->cell[x][z+1].y=2;
	f->cell[x][z-1].y=2;
}

struct card pick_card_no_repeats(struct game_state * gs)
{
	struct card c;
	c = pick_card();
	if(gs->game_player.flags&HAS_UNIBAR && c.type==UNIBAR_CARD){
		c=pick_card();
	}
	return c;
}

void player_movement(struct game_state * gs, double delta)
{
	struct vector dvec = (struct vector){0,0,0};

	if(gs->game_player.dash_timer>0){

		dvec = rot_to_dvec((gs->game_player.rotation_angle+
			dir_to_rot(gs->game_player.dash_dir)-180));

		dvec.x*=delta*11;
		dvec.z*=delta*11;

		move_unit(&gs->game_player,dvec);
		gs->game_player.dash_timer-=delta;
		return;
	}

	gs->game_player.moving=0;
	if(pi.keys['W'] || pi.keys['w'] || pi.controller.a1y<0){
		gs->game_player.moving|=1;
		dvec.x-=delta;
		dvec.z-=delta;
	}
	if(pi.keys['A'] || pi.keys['a']|| pi.controller.a1x<0){
		gs->game_player.moving|=2;
		dvec.x-=delta;
		dvec.z+=delta;
	}
	if(pi.keys['S'] || pi.keys['s']|| pi.controller.a1y>0){
		gs->game_player.moving|=4;
		dvec.x+=delta;
		dvec.z+=delta;
	}
	if(pi.keys['D'] || pi.keys['d']|| pi.controller.a1x>0){
		gs->game_player.moving|=8;
		dvec.x+=delta;
		dvec.z-=delta;
	}
	if(pi.keys['`'] || pi.keys['~']){
		if(gs->game_player.lvld){
			gs->card1=pick_card_no_repeats(gs);
			gs->card2=pick_card_no_repeats(gs);
			while(gs->card1.type==gs->card2.type)
				gs->card2=pick_card_no_repeats(gs);
			gs->game_player.lvling=1;
		}
	}

	move_unit(&gs->game_player,dvec);
}

void game_over(struct game_state * gs,int win)
{
	gameover_ui = gameover_menu(gs->game_player.score,win);
	int i;
	for(i=0;i<4;i++){
		gs->game_player.inventory.item[i].dragging=0;
	}
	paused=1;
	is_game_over=1;
}

void reset_items(struct game_state * gs)
{
	int i;
	for(i=0;i<4;i++){
		gs->game_player.inventory.item[i].duration=0;
		gs->game_player.inventory.item[i].cooldown=0;
	}
}

void door_check(struct game_state * gs)
{
	int t;
	t = door_at(gs->game_player.location);
	if(t!=-1){
		play_jump_sound_effect();
		transition_effect_timer=1;
		gs->game_player.hit_timer=0.2;
		gs->game_player.speed*=0.5;
		reset_items(gs);
		reset_particles();
		move_through_doorway(&world_map,t);
	}
}

void damage_unit(struct unit * u, double amount)
{
	if(u->type==UNIT_TYPE_ITEM)
		return;
	u->health-=amount;
	if(amount<1){
		u->damage_buffer+=amount;
		if(u->damage_buffer>1){
			add_damage_number(u->location,u->damage_buffer);
			u->damage_buffer = 0;
		}
	}else{
		add_damage_number(u->location,amount);
	}
}

void heal_unit(struct unit * u, double amount)
{
	u->health+=amount;
	if(u->health > u->max_health)
		u->health = u->max_health;
}

void correct_speed(struct game_state * gs, double delta)
{
	if(gs->game_player.speed < gs->game_player.max_speed){
		gs->game_player.speed+=delta*10;
		if(gs->game_player.speed > gs->game_player.max_speed)
			gs->game_player.speed = gs->game_player.max_speed;
	}
	if(gs->game_player.speed > gs->game_player.max_speed){
		gs->game_player.speed-=delta*10;
		if(gs->game_player.speed < gs->game_player.max_speed)
			gs->game_player.speed = gs->game_player.max_speed;
	}

	if(gs->game_player.cooldown < gs->game_player.max_cooldown){
		gs->game_player.cooldown+=delta*5;
		if(gs->game_player.cooldown > gs->game_player.max_cooldown)
			gs->game_player.cooldown = gs->game_player.max_cooldown;
	}
	if(gs->game_player.cooldown > gs->game_player.max_cooldown){
		gs->game_player.cooldown-=delta*5;
		if(gs->game_player.cooldown < gs->game_player.max_cooldown)
			gs->game_player.cooldown = gs->game_player.max_cooldown;
	}
}

void correct_damage(struct game_state * gs, double delta)
{
	if(gs->game_player.damage < gs->game_player.max_damage){
		gs->game_player.damage+=delta*10;
		if(gs->game_player.max_damage > gs->game_player.max_damage)
			gs->game_player.damage = gs->game_player.max_damage;
	}

	if(gs->game_player.damage > gs->game_player.max_damage){
		if((gs->game_player.damage - gs->game_player.max_damage) > 10)
			delta*=10;
		gs->game_player.damage-=delta*10;
		if(gs->game_player.damage < gs->game_player.max_damage)
			gs->game_player.damage = gs->game_player.max_damage;
	}
}

void add_mana(struct game_state * gs,struct unit * u, double amount)
{
	if(u->flags&HAS_UNIBAR &&( (u->health + amount) < u->max_health)){
		u->health+=amount;
	}
	if((u->mana + amount)<=u->max_mana){
		u->mana+=amount;
	}
}

void correct_color(struct game_state * gs, double delta)
{
	if(gs->game_player.color.x<1)
		gs->game_player.color.x+=delta;
	if(gs->game_player.color.y<1)
		gs->game_player.color.y+=delta;
	if(gs->game_player.color.z<1)
		gs->game_player.color.z+=delta;
}

void boss_door_check(struct game_state * gs)
{
	int t;
	t = door_at(gs->game_player.location);
	if(t!=-1){
		play_jump_sound_effect();
		transition_effect_timer=1;
		gs->game_player.hit_timer=0.2;
		gs->game_player.speed*=0.5;
		reset_items(gs);
		reset_particles();
		transfer_map(world_map.level+1);
	}
}

double portal_timer ;
double max_portal_timer;

void update_portal_timer(struct game_state * gs, double delta)
{
	struct room * r = world_map.current_room;
	if(portal_timer<=0){
		if(r->boss_room)
		{
			boss_door_check(gs);
		}else{
			door_check(gs);
		}
	}else{
		if(gs->n_npcs<=0)
			portal_timer-=delta*6;
		if(r->starting_room)
			portal_timer-=delta*4;
		if(r->boss_room)
			portal_timer+=delta*4;
		portal_timer-=delta;
	}
}

#define FIRE_EMIT_RADIUS 0.1

void emit_fire(struct game_state * gs, struct vector at)
{
	int i;
	int rando;
	for(i=0;i<4;i++){
		rando = rand()%MAX_FIRES;
		gs->fire[rando] = (struct particle) {0};
		gs->fire[rando].location=at;
		gs->fire[rando].direction=v_s_mul(FIRE_EMIT_RADIUS,
				random_direction());
		gs->fire[rando].alive=1;
	}
}

void flaming_effect(struct game_state * gs, double delta, struct unit * u)
{
	if((u->flaming)<=0)
		return;
	if( (u->flame_timer) > 0.0){
		u->flame_timer-=delta;
	}else{
		u->speed=(u->max_speed*0.3);
		u->flaming--;
		u->flame_timer=0.1;
		if(!(rand()%10))
			emit_fire(gs,u->location);
	}
}

void tell_current_neighborhoods(struct vector loc)
{
	struct graph * g = &(world_map.current_room->neighborhoods);
	struct neighborhood * n;
	int i;
	for(i=0;i<g->n_elements;i++){
		n = (struct neighborhood *) g->element[i];
		if(point_in_neighborhood(*n,loc))
				printf("%i,",i);
	}
	printf("\n");
}

void controller_face(struct game_state * gs)
{
	struct vector cv = {pi.controller.a2x/200, 0 ,pi.controller.a2y/200};
	if(cv.x==0 && cv.z==0)
		return;
	double angle = 45+face_angle((struct vector){0,0,0},cv);
	cv = rot_to_dvec(angle);
	face(&gs->game_player,v_add(cv,gs->game_player.location));
}

void update_player(struct game_state * gs,double delta)
{
	if(gs->game_player.health<=0){
		game_over(gs,0);
	}
	add_mana(gs,&gs->game_player,gs->game_player.delta_mana*delta);

	if(gs->n_npcs<=0){
		add_mana(gs,&gs->game_player,gs->game_player.delta_mana*delta*2);
	}

	if((gs->game_player.wrath)>0){
		add_mana(gs,&gs->game_player,
				((gs->game_player.max_health)-
				(gs->game_player.health))*gs->game_player.wrath
				*delta*0.5);
	}

	player_movement(gs,delta);
	player_items(gs,delta);

	update_portal_timer(gs,delta);

	field_effect(gs,&gs->game_player,delta,0);
	field_effect(gs,&gs->game_player,delta,1);
	field_effect(gs,&gs->game_player,delta,2);

	correct_speed(gs,delta);
	correct_damage(gs,delta);
	correct_color(gs,delta);

	if(gs->game_player.dash_timer<=0){
		if(pi.using_controller){
			controller_face(gs);
		}else{
			face(&gs->game_player,
				screen_to_world(gs,pi.mouse_x,pi.mouse_y));
		}
	}
}

void move_towards_facing(struct unit * u,double delta)
{
	struct vector v = (struct vector) {0,0,0};

	v = rot_to_dvec(u->rotation_angle);
	v = v_s_mul(delta,v);

	if(isnan(v.x) || isnan(v.z)){
		return;
	}

	move_unit(u,v);
}

void impact_chunks(struct game_state * gs, int j)
{
	struct layout * l = &(world_map.current_room->layout);
	struct room * r = world_map.current_room;
	int rando;
	int i;
	for(i=0;i<3;i++){
		rando = rand()%MAX_CHUNKS;
		r->chunks[rando].location = gs->npc[j].location;
		r->chunks[rando].location.y=2;
		r->chunks[rando].alive=1;
		r->chunks[rando].frame=0;
		r->chunks[rando].type=(rand()%4);
		r->chunks[rando].direction=random_direction();
		r->chunks[rando].type = ((rand()%5)!=4)+1;
	}
}

void spew_chunks(struct game_state * gs, int j)
{
	struct layout * l = &(world_map.current_room->layout);
	struct room * r = world_map.current_room;
	int rando;
	int i;
	for(i=0;i<5;i++){
		rando = rand()%MAX_CHUNKS;
		r->chunks[rando].location = gs->npc[j].location;
		r->chunks[rando].location.y=2;
		r->chunks[rando].alive=1;
		r->chunks[rando].frame=0;
		r->chunks[rando].type=(rand()%4);
		r->chunks[rando].direction=random_direction();
		r->chunks[rando].type = ((rand()%5)!=4)+1;
	}
}

void death_effect(struct game_state * gs, int j)
{
	last_camera.x+=((rand()%10)-5)/10.0;
	last_camera.z+=((rand()%10)-5)/10.0;

	play_explosion_sound_effect();
	explosion_graphic(gs->npc[j].location);
	add_scorch(gs->npc[j].location);
	spew_chunks(gs,j);
}

void level_up(struct game_state * gs){
	gs->game_player.level++;
	gs->game_player.lvld++;
	gs->game_player.exp=0;
	play_lvlup_sound_effect();
	global_save_state.times_leveled++;
}

void death(struct game_state * gs, int j)
{
	death_effect(gs,j);

	gs->game_player.score+=gs->npc[j].score;
	gs->game_player.exp+=gs->npc[j].exp * gs->game_player.learn_rate;
	if(gs->game_player.exp>=next_level(gs->game_player.level)){
		level_up(gs);
	}
	if(!(rand()%10)
			&& gs->npc[j].type!=UNIT_TYPE_SPIDER){
		add_npc(gs,item_npc(gs->npc[j].location));
	}
	if((gs->npc[j].type == UNIT_TYPE_BOSS) ||
			(gs->npc[j].type == UNIT_TYPE_MOLE)) {
		portal_timer=0;
	}
	if(gs->npc[j].type == UNIT_TYPE_YO){
		portal_timer=0;
		remove_npc(gs,gs->npc[j].connected_to);
	}
	if(gs->npc[j].type == UNIT_TYPE_SPINNER_BOSS){
		remove_npc(gs,j);
		gameover_ui = gameover_menu(gs->game_player.score,1);
		game_over(gs,1);
		return;
	}
	if(gs->npc[j].type == UNIT_TYPE_SPADE){
		gs->npc[j].health=500;
		gs->npc[j].connected_to=3;
		return;
	}
	remove_npc(gs,j);
}

/* move a unit away from a point*/
void push_away(struct unit * u, struct vector loc, double delta)
{
	if(u->location.x < loc.x)
		u->location.x-=delta;
	if(u->location.y < loc.y)
		u->location.y-=delta;

	if(u->location.x > loc.x)
		u->location.x+=delta;
	if(u->location.y > loc.y)
		u->location.y+=delta;
}

void personal_space(struct game_state * gs, double delta, int j)
{
	int i;
	for(i=0;i<gs->n_npcs;i++){
		if(i==j)
			continue;
		if(is_near(gs->npc[i].location,gs->npc[j].location,1)){
			push_away(&gs->npc[j],gs->npc[i].location,delta*1.2);
		}
	}
}


void count_fps(double d)
{
	frames++;
	seconds+=d;
	fps=frames/seconds;
	if(seconds>0.3){
		frames=1;
		seconds=d;
	}
}

int spend(struct unit * u, int amount)
{
	if( u->coins >= amount ) {
		u->coins-=amount;
		return 1;
	}
	return 0;
}

void update_shop(struct game_state * gs, double delta)
{
	struct shop * s = &(world_map.current_room->shop);
	int i;
	for(i=0;i<MAX_TRANSACTIONS;i++){
		if(is_near(gs->game_player.location,s->t[i].location,1)){
			if(!(s->t[i].sold) &&
			      spend(&gs->game_player,s->t[i].price)){
				s->t[i].sold=1;
				add_item(&gs->game_player.inventory,
						s->t[i].item);
				global_save_state.coins_spent+=s->t[i].price;
				write_save_state();
			}
		}
	}
}

double card_delay=0.2;

void card_controls(struct game_state * gs, double d)
{
	struct vector v = pixel_to_screen(pi.mouse_x,pi.mouse_y);
	if(pi.left_click){
		if(v.x>0)
			card_effect(gs,gs->card1.type);
		else if(v.x<=0)
			card_effect(gs,gs->card2.type);

		gs->game_player.lvling=0;
		gs->game_player.lvld--;
		card_delay=0.2;
	}
}

void update_cards(struct game_state * gs, double d)
{
	struct vector v = pixel_to_screen(pi.mouse_x,pi.mouse_y);
	struct vector s = {0};
	s.x = -v.y; s.z = v.x;

	double off = 4.0;

	gs->card1.rotation=s;
	gs->card1.rotation.z-=off;
	gs->card1.rotation_angle=15;

	gs->card2.rotation=s;
	gs->card2.rotation.z+=off;
	gs->card2.rotation_angle=15;

	if(card_delay<=0){
		card_controls(gs,d);
	}else{
		card_delay-=d;
	}
}

void fire_npc_check(struct game_state * gs, double delta, int i)
{
	int j;
	for(j=0;j<gs->n_npcs;j++){
		if(is_near(gs->fire[i].location, gs->npc[j].location,
					gs->npc[j].hit_radius)){
			player_dealt_damage(gs,delta/10.0);
			damage_unit(&gs->npc[j],delta/10.0);
			if(!(rand()%30)){
				gs->npc[j].hit_timer=0.1;
				if(gs->npc[j].flaming<3)
					gs->npc[j].flaming++;
			}
		}
	}
}

#define FIRE_DURATION 1.0
#define FIRE_JANKNESS 2.0
#define FIRE_SPEED 15.0
#define FIRE_RANGE 7.0

void update_fire(struct game_state * gs, double d,int i)
{
	struct vector velocity;
	gs->fire[i].frame+=d*(FIRE_SPEED/FIRE_RANGE);
	if( (gs->fire[i].frame>FIRE_DURATION) || 
			(wall_check(gs,gs->fire[i].location)) ){
		gs->fire[i].alive=0;
	}
	fire_npc_check(gs,d,i);
	gs->fire[i].direction= v_add(gs->fire[i].direction,
			v_s_mul(FIRE_JANKNESS*d,random_direction()));
	velocity=v_s_mul(FIRE_SPEED*d,gs->fire[i].direction);
	gs->fire[i].location = v_add(velocity,gs->fire[i].location);
}

void update_fires(struct game_state * gs,double d)
{
	int i;
	for(i=0;i<MAX_FIRES;i++){
		if(gs->fire[i].alive)
			update_fire(gs,d,i);
	}
}

void update_game_state(struct game_state * gs,double d)
{
	if(gs->game_player.lvling){
		update_cards(gs,d);
		return;
	}
	update_field(&gs->rfield,d);
	update_field(&gs->gfield,d);
	update_field(&gs->bfield,d);
	update_bullets(gs,d);
	update_missiles(gs,d);
	update_fires(gs,d);
	update_npcs(gs,d);
	if(world_map.current_room->has_shop){
		update_shop(gs,d);
	}
	update_player(gs,d);
}

void update_menus(struct game_state * gs, double d)
{
	if(is_game_over)
		update_ui(&gameover_ui);
	else
		update_ui(ui);
}

void engine_tick(struct game_state * gs,double d)
{
	if(!paused){
		update_game_state(gs,d);
	}else{
		update_menus(gs,d);
	}
	count_fps(d);
}
