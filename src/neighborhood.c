#include "engine.h"
#include "neighborhood.h"
#include "room.h"
#include "dimensions.h"
#include "map.h"

int point_in_neighborhood(struct neighborhood n, struct vector p)
{
	if(point_in_triangle(p,n.a,n.b,n.c)){
		return 1;
	}
	if(point_in_triangle(p,n.a,n.d,n.c)){
		return 1;
	}
	return 0;
}	

int point_in_neighborhood_graph(struct graph * g, struct vector p)
{
	int i;
	struct neighborhood *buf;
	for(i=0;i<g->n_elements;i++){
		buf = (struct neighborhood *) g->element[i];
		if(point_in_neighborhood(*buf,p)){
			return i;
		}
	}
	return -1;
}

int should_generate_neighborhood(struct graph * g, struct layout * l, 
		int i,int j)
{
	if((point_in_neighborhood_graph(g,(struct vector){i+0.5,0,j+0.5})==-1) 
			&& walkable_char(l->tiles[i][j])){
		return 1;
	}
	return 0;
}

int wall_intersects_neighborhood(struct layout * l, struct neighborhood * n)
{
	if(layout_wall_intersects_line(l,n->a,n->b))
		return 1;
	if(layout_wall_intersects_line(l,n->a,n->d))
		return 1;
	if(layout_wall_intersects_line(l,n->b,n->c))
		return 1;
	if(layout_wall_intersects_line(l,n->d,n->c))
		return 1;
	return 0;
}

void generate_neighborhood(struct graph * g, struct layout * l, int i, int j)
{
	struct neighborhood * n = calloc(1,sizeof(struct neighborhood));

	int w=i,h=j+1;

	while(walkable_char(l->tiles[i-1][j]))
		i--;
	while(walkable_char(l->tiles[w+1][j]))
		w++;

	n->a = (struct vector) {i,0,j};
	n->b = (struct vector) {w,0,j};
	n->c = (struct vector) {w,0,h};
	n->d = (struct vector) {i,0,h};

	while(!wall_intersects_neighborhood(l,n)){
		n->a.z--;
		n->b.z--;
	}
	n->a.z++;
	n->b.z++;

	while(!wall_intersects_neighborhood(l,n)){
		n->c.z++;
		n->d.z++;
	}
	/*
	n->c.z--;
	n->d.z--;
	*/
	n->b.x++;
	n->c.x++;

	add_element_to_graph(g,n);
}

void generate_neighborhoods(struct graph * g, struct layout * l)
{
	int i,j;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			if(should_generate_neighborhood(g,l,i,j)){
				generate_neighborhood(g,l,i,j);
			}
		}
	}
}

int neighborhoods_intersect(struct graph * g, int i, int j)
{
	struct neighborhood * n1 = (struct neighborhood *) g->element[i];
	struct neighborhood * n2 = (struct neighborhood *) g->element[j];
	
	if(line_intersects_line(n1->a,n1->b,n2->a,n2->d)) return 1;
	if(line_intersects_line(n1->a,n1->b,n2->b,n2->c)) return 1;
	if(line_intersects_line(n1->d,n1->c,n2->a,n2->d)) return 1;
	if(line_intersects_line(n1->d,n1->c,n2->b,n2->c)) return 1;

	if(line_intersects_line(n2->a,n2->b,n1->a,n1->d)) return 1;
	if(line_intersects_line(n2->a,n2->b,n1->b,n1->c)) return 1;
	if(line_intersects_line(n2->d,n2->c,n1->a,n1->d)) return 1;
	if(line_intersects_line(n2->d,n2->c,n1->b,n1->c)) return 1;

	return 0;
}

void connect_neighborhoods(struct graph * g, struct layout * l)
{
	int i,j;
	for(i=0;i<(g->n_elements);i++){
		for(j=0;j<(g->n_elements);j++){
			if(neighborhoods_intersect(g,i,j)){
				add_edge_to_graph(g,(struct edge) {i,j});
			}
		}
	}
}

struct graph layout_to_neighborhoods(struct layout * layout)
{
	struct graph g = (struct graph) {0};
	generate_neighborhoods(&g,layout);
	connect_neighborhoods(&g,layout);
	return g;
}
