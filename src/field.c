#include "map.h"
#include "field.h"

double wave_destruction;

static inline void propogate(struct vector * to, struct vector * from,
		double delta)
{
	double d = (2*(delta))*(from->y - to->y);
	to->x +=d;
	from->x -=d*1.1;
}

void update_cell(struct field * field,int i, int j,double delta)
{
	if(!walkable(i,j))
		return;
	struct vector *s = &(field->cell[i][j]);/*the center piece*/

	struct vector *a = (walkable(i-1,j)) ? &(field->cell[i-1][j]) :NULL;
	struct vector *w = (walkable(i,j+1)) ? &(field->cell[i][j+1]) :NULL;
	struct vector *d = (walkable(i+1,j)) ? &(field->cell[i+1][j]) :NULL;
	struct vector *x = (walkable(i,j-1)) ? &(field->cell[i][j-1]) :NULL;
	if(w) propogate(s,w,delta);
	if(a) propogate(s,a,delta);
	if(d) propogate(s,d,delta);
	if(x) propogate(s,x,delta);
	field->cell[i][j].y+= (delta*5)*(field->cell[i][j].x);
	field->cell[i][j].y/=1+(wave_destruction*delta*0.3);
	if(field->cell[i][j].y>2.0)
		field->cell[i][j].y=2.0;
	if(field->cell[i][j].y<(-2.0))
		field->cell[i][j].y=-2.0;
}

void update_field(struct field * field, double delta)
{
	int i,j;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			update_cell(field,i,j,delta);
		}
	}
}

