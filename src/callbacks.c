#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "save.h"
#include "input.h"
#include "callbacks.h"
#include "engine.h"
#include "game.h"
#include "graphics.h"
#include "audio.h"

void resize_callback(int w, int h){
	glViewport(0, 0, w, h);
	window_width = w;
	window_height = h;
	frame_ratio = w / (float)h;
	resize_fbo(w, h);
}

void err_callback(int err, const char* description)
{
	printf("ERR CALLBACK[%i]:%s\n",err,description);
}

#define JOYSTICK_DEADZONE 8000

void joy_axis_callback(SDL_Event event)
{
	pi.using_controller=1;
	int value = 0;
	if(event.jaxis.axis == 2 || event.jaxis.axis == 5)// triggers
	{
		if(event.jaxis.value<-5000)
			value = 0;
		else
			value = event.jaxis.value;
	}else{
		if((event.jaxis.value < JOYSTICK_DEADZONE ) && 
				(event.jaxis.value > -JOYSTICK_DEADZONE)){
			value = 0;
		}else{
			value = event.jaxis.value;
		}
	}

	switch(event.jaxis.axis)
	{
		case 0:
			pi.controller.a1x = value;
			break;
		case 1:
			pi.controller.a1y = value;
			break;
		case 2:
			pi.controller.lt = value;
			break;
		case 3:
			pi.controller.a2x = value;
			break;
		case 4:
			pi.controller.a2y = value;
			break;
		case 5:
			pi.controller.rt = value;
			break;
	}
}

void joy_button_callback(SDL_Event event)
{
	pi.using_controller=1;
	if(event.jbutton.button==7 && event.jbutton.state==0){
		if(!is_game_over){
			paused=!paused;
			if(paused)
				Mix_PauseMusic();
			else
				Mix_ResumeMusic();
		}
	}
	switch(event.jbutton.button)
	{
		case 0:
			pi.controller.a=event.jbutton.state;
			break;
		case 1:
			pi.controller.b=event.jbutton.state;
			break;
		case 2:
			pi.controller.x=event.jbutton.state;
			break;
		case 3:
			pi.controller.y=event.jbutton.state;
			break;
		case 4:
			pi.controller.lb=event.jbutton.state;
			break;
		case 5:
			pi.controller.rb=event.jbutton.state;
			break;
	}
}

void joy_hat_callback(SDL_Event event)
{
	pi.using_controller=1;
	printf("joy hat callback:%i %i\n",event.jhat.hat,
			event.jhat.value);
}

void key_callback(SDL_Event event)
{
	pi.using_controller=0;
	int key = event.key.keysym.sym;
	int action = event.key.state == SDL_PRESSED;

	if(key == 1073742049){
		pi.shift_key=action;
	}

	if(key>400 && key!=342){
//		printf("key>400: %c\n",key);
		return;
	}

	if(key==27 && action ==0){
		if(!is_game_over){
			paused=!paused;
			if(paused)
				Mix_PauseMusic();
			else
				Mix_ResumeMusic();
		}
	}
	pi.keys[key]=(char)action; /*action is a binary (press||depress)*/
}

void cursor_callback(SDL_Event event)
{
	pi.using_controller=0;
	pi.mouse_x=event.motion.x;
	pi.mouse_y=event.motion.y;
}

void cursor_button_callback(SDL_Event event)
{
	pi.using_controller=0;
	int button = event.button.button;
	int action = (event.button.type == SDL_MOUSEBUTTONDOWN);
	if(button==SDL_BUTTON_LEFT)
		pi.left_click=action;
	if(button==SDL_BUTTON_RIGHT)
		pi.right_click=action;
}

void simple_press_callback()
{
	printf("simple press callback\n");
}

void options_callback()
{
	current_menu_screen=SCREEN_OPTIONS;
}

void back_callback()
{
	current_menu_screen=SCREEN_MAIN;
	write_save_state();
}

void toggle_audio_callback()
{
	global_save_state.audio=!global_save_state.audio;
	if(global_save_state.audio==0){
		Mix_HaltMusic();
		stop_sounds();
	}
	write_save_state();
}

void exit_callback()
{
	game_exit(0);
}

void reset_callback()
{
	reset_lights();
	generate_map(&world_map,1);
	is_game_over=0;
	planet_impact_timer=0;
	paused=0;
	stop_sounds();
	set_music(1);
}

void process_events()
{
	SDL_Event event;

	while( SDL_PollEvent(&event) ) {
		switch( event.type ) {
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				cursor_button_callback(event);
				break;
			case SDL_MOUSEMOTION:
				cursor_callback(event);
				break;
			case SDL_WINDOWEVENT:
				if(event.window.event==SDL_WINDOWEVENT_RESIZED)
					resize_callback(event.window.data1, 
							event.window.data2);
				break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				key_callback(event );
				break;
			case SDL_JOYAXISMOTION:
				joy_axis_callback(event);
				break;
			case SDL_JOYBUTTONDOWN:
			case SDL_JOYBUTTONUP:
				joy_button_callback(event);
				break;
			case SDL_JOYHATMOTION:
				joy_hat_callback(event);
				break;
			case SDL_QUIT:
				game_exit(0);
				break;
		}
	}
}
