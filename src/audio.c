#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <math.h>

#include "audio.h"
#include "save.h"

int audio_initalized;

Mix_Music * sfx_music = NULL;

Mix_Chunk * sfx_coin[4];
Mix_Chunk * sfx_laser[6];
Mix_Chunk * sfx_hit[6];
Mix_Chunk * sfx_kick;
Mix_Chunk * sfx_explosion[6];
Mix_Chunk * sfx_blip[1];

Mix_Chunk * sfx_lvlup;
Mix_Chunk * sfx_jump;
Mix_Chunk * sfx_teledice;
Mix_Chunk * sfx_risset;
Mix_Chunk * sfx_oom;
Mix_Chunk * sfx_rocket;
Mix_Chunk * sfx_dash;
Mix_Chunk * sfx_chips;
Mix_Chunk * sfx_lucky_sevens;
Mix_Chunk * sfx_reel;

Mix_Chunk * sfx_chainsaw;
Mix_Chunk * sfx_plasma;
Mix_Chunk * sfx_big_laser;
Mix_Chunk * sfx_brownian;

Mix_Chunk * sfx_planet_impact;

#define CHAINSAW_CHANNEL 	190
#define PLASMA_CHANNEL 		191
#define BIG_LASER_CHANNEL 	192
#define OOM_CHANNEL 		193
#define BROWNIAN_CHANNEL 	194

void init_sounds()
{
	sfx_music = Mix_LoadMUS("sounds/starAmbience.mp3");
	if(!sfx_music){
		printf("ERR: couldn't load music \n%s\n",Mix_GetError());
	}
	sfx_coin[0] = Mix_LoadWAV("sounds/coin1.wav");
	if(!sfx_coin[0])
		printf("ERR: couldn't load coin sfx\n");
	sfx_coin[1] = Mix_LoadWAV("sounds/coin2.wav");
	sfx_coin[2] = Mix_LoadWAV("sounds/coin3.wav");
	sfx_coin[3] = Mix_LoadWAV("sounds/coin4.wav");

	sfx_laser[0] = Mix_LoadWAV("sounds/laser1.wav");
	sfx_laser[1] = Mix_LoadWAV("sounds/laser2.wav");
	sfx_laser[2] = Mix_LoadWAV("sounds/laser3.wav");
	sfx_laser[3] = Mix_LoadWAV("sounds/laser4.wav");
	sfx_laser[4] = Mix_LoadWAV("sounds/laser5.wav");
	sfx_laser[5] = Mix_LoadWAV("sounds/laser6.wav");
	 
	int i;
	for(i=0;i<6;i++){
		Mix_VolumeChunk(sfx_laser[i],50);
	}

	sfx_hit[0] = Mix_LoadWAV("sounds/hit1.wav");
	sfx_hit[1] = Mix_LoadWAV("sounds/hit2.wav");
	sfx_hit[2] = Mix_LoadWAV("sounds/hit3.wav");
	sfx_hit[3] = Mix_LoadWAV("sounds/hit4.wav");
	sfx_hit[4] = Mix_LoadWAV("sounds/hit5.wav");
	sfx_hit[5] = Mix_LoadWAV("sounds/hit6.wav");

	sfx_kick = Mix_LoadWAV("sounds/Dry-Kick.wav");

	sfx_explosion[0] = Mix_LoadWAV("sounds/explosion1.wav");
	sfx_explosion[1] = Mix_LoadWAV("sounds/explosion2.wav");
	sfx_explosion[2] = Mix_LoadWAV("sounds/explosion3.wav");
	sfx_explosion[3] = Mix_LoadWAV("sounds/explosion4.wav");
	sfx_explosion[4] = Mix_LoadWAV("sounds/explosion5.wav");
	sfx_explosion[5] = Mix_LoadWAV("sounds/explosion6.wav");

	sfx_blip[0] = Mix_LoadWAV("sounds/blip1.wav");

	sfx_lvlup = Mix_LoadWAV("sounds/lvlup.wav");
	sfx_jump = Mix_LoadWAV("sounds/jump.wav");
	sfx_teledice = Mix_LoadWAV("sounds/teledice.wav");
	sfx_big_laser = Mix_LoadWAV("sounds/big_laser.wav");
	sfx_risset = Mix_LoadWAV("sounds/risset.wav");
	sfx_oom = Mix_LoadWAV("sounds/outofmana.wav");
	sfx_rocket = Mix_LoadWAV("sounds/rocket.wav");
	sfx_dash = Mix_LoadWAV("sounds/dash.wav");
	sfx_lucky_sevens = Mix_LoadWAV("sounds/lucky_seven.wav");
	sfx_reel = Mix_LoadWAV("sounds/reel.wav");
	sfx_chips = Mix_LoadWAV("sounds/chips.wav");
	Mix_VolumeChunk(sfx_chips,25);

	sfx_chainsaw = Mix_LoadWAV("sounds/chainsaw.wav");
	Mix_PlayChannel(CHAINSAW_CHANNEL,sfx_chainsaw,-1);
	Mix_Pause(CHAINSAW_CHANNEL);

	sfx_plasma = Mix_LoadWAV("sounds/plasma.wav");
	Mix_PlayChannel(PLASMA_CHANNEL,sfx_plasma,-1);
	Mix_Pause(PLASMA_CHANNEL);

	sfx_brownian = Mix_LoadWAV("sounds/brownian.wav");
	Mix_PlayChannel(BROWNIAN_CHANNEL,sfx_brownian,-1);
	Mix_Volume(BROWNIAN_CHANNEL,0);

	sfx_planet_impact = Mix_LoadWAV("sounds/planet_impact.wav");
}

void print_mixer_versions()
{
	SDL_version compile_version;
	const SDL_version *link_version=Mix_Linked_Version();
	SDL_MIXER_VERSION(&compile_version);
	printf("compiled with SDL_mixer version: %d.%d.%d\n", 
			compile_version.major,
			compile_version.minor,
			compile_version.patch);
	printf("linked with SDL_mixer version: %d.%d.%d\n", 
			link_version->major,
			link_version->minor,
			link_version->patch);
}

void game_play_sound(struct Mix_Chunk * chunk)
{
	if(global_save_state.audio)
		Mix_PlayChannel(-1,chunk,0);
}

void play_lvlup_sound_effect()
{
	game_play_sound(sfx_lvlup);
}

void play_risset_sound_effect(float distance)
{
	int v = (int)(100-(distance*5));
	if(v<0)
		v=0;
	Mix_VolumeChunk(sfx_risset,v);

	game_play_sound(sfx_risset);
}

void play_explosion_sound_effect()
{
	int r = rand()%6;
	game_play_sound(sfx_explosion[r]);
}

void play_hit_sound_effect()
{
	int r = rand()%6;
	game_play_sound(sfx_hit[r]);
	game_play_sound(sfx_kick);
}

void play_laser_sound_effect(float distance)
{
	int r = rand()%6;
	if(distance<0)
		distance=0;
	Mix_VolumeChunk(sfx_laser[r],(int)(100-(distance*5)));
	game_play_sound(sfx_laser[r]);
}

void play_coin_sound_effect()
{
	int r = rand()%4;
	game_play_sound(sfx_coin[r]);
}

void play_blip_sound_effect()
{
	int r = rand()%1;
	game_play_sound(sfx_blip[r]);
}

void play_jump_sound_effect()
{
	game_play_sound(sfx_jump);
}

void play_teledice_sound_effect()
{
	game_play_sound(sfx_teledice);
}

void play_rocket_sound_effect()
{
	game_play_sound(sfx_rocket);
}

void play_dash_sound_effect()
{
	game_play_sound(sfx_dash);
}

void play_chips_sound_effect()
{
	game_play_sound(sfx_chips);
}

void play_planet_impact_sound_effect()
{
	game_play_sound(sfx_planet_impact);
}

void play_sevens_sound_effect()
{
	game_play_sound(sfx_lucky_sevens);
}

void play_reel_sound_effect()
{
	game_play_sound(sfx_reel);
}

void play_oom_sound_effect()
{
	if(!Mix_Playing(OOM_CHANNEL)){
		if(global_save_state.audio)
			Mix_PlayChannel(OOM_CHANNEL,sfx_oom,0);
	}
}

void stop_big_laser_sound_effect()
{
	Mix_Pause(BIG_LASER_CHANNEL);
}

void play_big_laser_sound_effect()
{
	if(global_save_state.audio)
		Mix_PlayChannel(BIG_LASER_CHANNEL,sfx_big_laser,-1);
}

void stop_chainsaw_sound_effect()
{
	Mix_Pause(CHAINSAW_CHANNEL);
}

void play_chainsaw_sound_effect()
{
	if(global_save_state.audio)
		Mix_Resume(CHAINSAW_CHANNEL);
}

void stop_plasma_sound_effect()
{
	Mix_Pause(PLASMA_CHANNEL);
}

void play_plasma_sound_effect()
{
	if(global_save_state.audio)
		Mix_Resume(PLASMA_CHANNEL);
}

void set_brownian_volume(float volume)
{
	if(!global_save_state.audio || volume==0){
		Mix_Volume(BROWNIAN_CHANNEL,0);
	}else{
		Mix_Volume(BROWNIAN_CHANNEL,100-((int)volume*10));
	}
}

void set_music(int on){
	if(on && (global_save_state.audio)){
		Mix_FadeInMusic(sfx_music,1,1000);
	}else{
		Mix_FadeOutMusic(1000);
	}
}

void stop_sounds(){
	stop_chainsaw_sound_effect();
	stop_plasma_sound_effect();
	stop_big_laser_sound_effect();
	set_brownian_volume(0);
}

void init_audio()
{
	int error;

	error = Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT,2,1024);
	if(error==-1){		
		printf("Mix_OpenAudio error\n");
		exit(-35);
	}

//	print_mixer_versions();
	Mix_AllocateChannels(200);
	init_sounds();
	audio_initalized=1;
	Mix_VolumeMusic(50);
}

