#include <stdlib.h>
/* adds both the yoyo units */
#include "game.h"
#include "callbacks.h"
#include "model.h"
#include "engine.h"
#include "map.h"
#include <dirent.h>
#include <string.h>

struct map world_map;

struct unit antenna_npc(struct vector location,int freq)
{
	struct unit npc={0}; 
	npc.cooldown2=freq;/*this determines the frequency(color) of the ant*/
	npc.speed=0.0;
	npc.exp=30;
	npc.cooldown = 2;
	npc.damage=0;
	npc.max_damage=0;
	npc.health=20;
	npc.location=location;
	npc.type = UNIT_TYPE_ANTENNA;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 1;
	npc.poison_timer = 0.0;
	npc.score=300;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit tank_npc(struct vector location)
{
	struct unit npc={0}; 
	npc.destination=location;
	npc.speed=2.0;
	npc.damage=10;
	npc.max_damage=40;
	npc.health=50;
	npc.exp=20;
	npc.location=location;
	npc.type = UNIT_TYPE_TANK;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 1;
	npc.poison_timer = 0.0;
	npc.score=50;
	npc.color = (struct vector) {1,1,1};
	npc.bullet_speed=40;
	return npc;
}

struct unit yo_npc(struct vector location)
{
	struct unit npc={0}; 
	npc.speed=5.0;
	npc.damage=40;
	npc.max_damage=40;
	npc.health=100;
	npc.exp=50;
	npc.location=location;
	npc.type = UNIT_TYPE_YO;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 1;
	npc.poison_timer = 0.0;
	npc.score=300;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit item_npc(struct vector location)
{
	struct unit npc={0};
	npc.speed=0.0;
	npc.health=2000;
	npc.location=location;
	npc.type = UNIT_TYPE_ITEM;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.cooldown = 10;
	npc.hit_radius = 0;
	npc.poison_timer=0.0;
	npc.score=1;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit sign_npc(struct vector location)
{
	struct unit npc={0};
	npc.speed=0.0;
	npc.health=200;
	npc.location=location;
	npc.type = UNIT_TYPE_SIGN;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 1.0;
	npc.poison_timer=0.0;
	npc.score=1;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit intercom_npc(struct vector location,int level)
{
	struct unit npc={0};
	npc.speed=0.0;
	npc.health=200;
	npc.location=location;
	npc.type = UNIT_TYPE_INTERCOM;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 0.0;
	npc.poison_timer=0.0;
	npc.score=1;
	if(level==1){
		npc.speech.line[0]="Are you up?";
		npc.speech.line[1]="Good.";
		npc.speech.line[2]="The ship we're on is about to crash.";
		npc.speech.line[3]="Three floors down is the engine.";
		npc.speech.line[4]="Reset it so we don't explode.";
		npc.speech.line[5]="Good luck.";
	} else if(level==2){
		npc.speech.line[0]="Good work, two floors to go.";
		npc.speech.line[1]="This is the farm floor.";
		npc.speech.line[2]="Watch out for the tanks.";
	} else if(level==3){
		npc.speech.line[0]="Alright,";
		npc.speech.line[1]="We're finally at the engine floor";
	}
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit spinner_npc(struct vector location)
{
	struct unit npc={0};
	npc.speed=1.0;
	npc.health=15;
	npc.exp=7;
	npc.damage=3;
	npc.max_damage=3;
	npc.bullet_speed=50;
	npc.bullet_duration=0.25;
	npc.location=location;
	npc.type = UNIT_TYPE_SPINNER;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 0.8;
	npc.score=7;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit shieldman_npc(struct vector location)
{
	struct unit npc={0};
	npc.destination=location;
	npc.path_timer=0;
	npc.max_speed=7.0;
	npc.speed=npc.max_speed;
	npc.health=15;
	npc.exp=10;
	npc.damage=20;
	npc.max_damage=20;
	npc.location=location;
	npc.type = UNIT_TYPE_SHIELDMAN;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 1.0;
	npc.score=5;
	npc.color = (struct vector) {1,1,1};
	npc.cooldown=0;
	npc.cooldown2=0;
	return npc;
}

struct unit scavenger_npc(struct vector location)
{
	struct unit npc={0};
	npc.destination=location;
	npc.path_timer=0;
	npc.speed=2.0;
	npc.max_speed=8.0;
	npc.health=15;
	npc.exp=10;
	npc.damage=20;
	npc.max_damage=20;
	npc.location=location;
	npc.type = UNIT_TYPE_NEUTRAL_CREEP;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 1.0;
	npc.score=5;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit sentry_npc(struct vector location)
{
	struct unit npc={0};
	npc.path_timer=0;
	npc.speed=2.5;
	npc.cooldown=2;
	npc.max_cooldown=1.0;
	npc.exp=15;
	npc.health=10;
	npc.damage=5;
	npc.max_damage=5;
	npc.bullet_speed=40;
	npc.location=location;
	npc.type = UNIT_TYPE_SENTRY;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 1.0;
	npc.score=10;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit double_sentry_npc(struct vector location)
{
	struct unit npc={0};
	npc.path_timer=0;
	npc.speed=2.5;
	npc.cooldown=1;
	npc.cooldown2=0.5;
	npc.max_cooldown=1.0;
	npc.exp=15;
	npc.health=10;
	npc.damage=10;
	npc.max_damage=10;
	npc.bullet_speed=40;
	npc.location=location;
	npc.type = UNIT_TYPE_DOUBLE_SENTRY;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 1.0;
	npc.score=10;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit ranger_npc(struct vector location)
{
	struct unit npc={0};
	npc.destination=location;
	npc.max_speed=4.5;
	npc.speed=npc.max_speed;
	npc.cooldown=2;
	npc.max_cooldown=0.7;
	npc.exp=15;
	npc.health=10;
	npc.damage=5;
	npc.max_damage=5;
	npc.bullet_speed=40;
	npc.location=location;
	npc.type = UNIT_TYPE_RANGER;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 1.0;
	npc.score=10;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit shotty_ranger_npc(struct vector location)
{
	struct unit npc={0};
	npc.destination=location;
	npc.max_speed=5.5;
	npc.speed=npc.max_speed;
	npc.cooldown=1;
	npc.exp=15;
	npc.health=10;
	npc.damage=10;
	npc.max_damage=10;
	npc.location=location;
	npc.type = UNIT_TYPE_SHOTTY_RANGER;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 1.0;
	npc.score=10;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit spade_npc(struct vector location)
{
	struct unit npc={0};
	npc.speed=5;
	npc.damage=10;
	npc.max_damage=10;
	npc.bullet_speed=40;
	npc.health=251;
	npc.location=location;
	npc.type = UNIT_TYPE_SPADE;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 1.0;
	npc.poison_timer = 0.0;
	npc.score=1000;
	npc.inventory.item[0]=side_blaster_item();
	npc.color = (struct vector) {1,1,1};
	npc.speech.line[0] = "You and I aren't so different.";
	return npc;
}

struct unit mole_npc(struct vector location)
{
	struct unit npc={0};
	npc.speed=0;
	npc.damage=10;
	npc.max_damage=10;
	npc.bullet_speed=40;
	npc.health=150;
	npc.location=location;
	npc.type = UNIT_TYPE_MOLE;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 1.0;
	npc.poison_timer = 0.0;
	npc.score=100;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit boss_npc(struct vector location)
{
	struct unit npc={0};
	npc.speed=0;
	npc.health=100;
	npc.location=location;
	npc.type = UNIT_TYPE_BOSS;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.hit_radius = 2.0;
	npc.poison_timer = 0.0;
	npc.score=100;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

struct unit spider_npc(struct vector location)
{
	struct unit npc={0};
	npc.speed=3.5;
	npc.cooldown=0;
	npc.exp=2;
	npc.health=1;
	npc.damage=5;
	npc.max_damage=5;
	npc.location=location;
	npc.type = UNIT_TYPE_SPIDER;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 0.5;
	npc.score=10;
	npc.color = (struct vector) {1,1,1};
	return npc;
}
struct unit spinner_boss_npc(struct vector location)
{
	struct unit npc={0};
	npc.moving=1;
	npc.speed=1.0;
	npc.health=200;
	npc.exp=7;
	npc.damage=10;
	npc.max_damage=10;
	npc.bullet_speed=40;
	npc.bullet_duration=1.0;
	npc.location=location;
	npc.type = UNIT_TYPE_SPINNER_BOSS;
	npc.rotation_angle = 90;
	npc.rotation = (struct vector) {0,1,0};
	npc.poison_timer = 0.0;
	npc.hit_radius = 1.5;
	npc.score=7;
	npc.color = (struct vector) {1,1,1};
	return npc;
}

/* adds both the yoyo units */
void add_yoyo(struct game_state * gs)
{
        struct unit y,z;
        y = yo_npc((struct vector) {1,0,1});
        z = yo_npc((struct vector) {11,0,1});

        /*this (hacky thing) connects the yo's together*/
        y.connected_to=gs->n_npcs+1;
        z.connected_to=gs->n_npcs;

        add_npc(gs,y);
        add_npc(gs,z);
}

void add_spinner_boss(struct game_state * gs)
{
	struct unit y = spinner_boss_npc((struct vector) {12,0,10});
	add_npc(gs,y);
}

void spawn_boss(struct room * room)
{
	struct vector loc = {10,0,10};

	switch(room->boss_room)
	{
		case 1:
			add_npc(&room->gs,boss_npc(loc));
			break;
		case 2:
			add_npc(&room->gs,mole_npc(loc));
			break;
		case 3:
			add_npc(&room->gs,spade_npc(loc));
			break;
		default:
			printf("ERR:unrecognized boss type");
			break;
	}
}

void spawn_intercom(struct game_state * gs,int n_room,int level)
{
	struct unit s_npc = intercom_npc((struct vector){5,0,1},level);

	add_npc(gs,s_npc);
}

void spawn_spiders(struct game_state * gs, struct vector loc)
{
	struct unit spider = spider_npc(loc);
	add_npc(gs,spider);
	spider.location.x+=0.75;
	add_npc(gs,spider);
	spider.location.z+=0.75;
	add_npc(gs,spider);
	spider.location.x-=0.75;
	add_npc(gs,spider);
}

int npc_in_room(struct room * room, int type)
{
	int n=0;
	int i;
	for(i=0;i<room->gs.n_npcs;i++){
		if(room->gs.npc[i].type==type)
			n++;
	}
	return n;
}

void spawn_mob(struct room * room,int i, int j, int level)
{
	struct vector loc = {i,0,j};
spawn:
	switch(rand()%10){
		case 0:
			add_npc(&room->gs,ranger_npc(loc));
			break;
		case 1:
			add_npc(&room->gs,scavenger_npc(loc));
			break;
		case 2:
			if(!npc_in_room(room,UNIT_TYPE_ANTENNA)){
				add_npc(&room->gs,antenna_npc(loc,rand()%3));
			}
			else goto spawn;
			break;
		case 3:
			add_npc(&room->gs,shotty_ranger_npc(loc));
			break;
		case 4:
			/*
			if(level==1)
				spawn_spiders(&room->gs,loc);
			else
			*/
			goto spawn;
			break;
		case 5: 
			add_npc(&room->gs,sentry_npc(loc));
			break;
		case 6: 
			if(level>2){
				add_npc(&room->gs,spinner_npc(loc));
			}else{
				goto spawn;
			}
			break;
		case 7: 
			if(level>2){
				add_npc(&room->gs,double_sentry_npc(loc));
			}else{
				goto spawn;
			}
			break;
		case 8: 
			if(level>1){
				add_npc(&room->gs,tank_npc(loc));
			}else{
				goto spawn;
			}
			break;
		case 9: 
			if(level>1){
				add_npc(&room->gs,shieldman_npc(loc));
			}else{
				goto spawn;
			}
			break;
	}
}

void spawn_mobs(struct room * room,int level)
{
	int i,j;
	struct vector loc = {4,0,4};
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			if(room->layout.tiles[i][j] == 's'){
				spawn_mob(room,i,j,level);
			}
		}
	}
	if(room->boss_room)
		spawn_boss(room);
	if(room->starting_room){
		spawn_intercom(&room->gs,room->starting_room,level);
	}
}

struct unit init_player(int sp)
{
	struct unit player = {0};
	player.location = (struct vector){5,0,5};
	player.destination = player.location;
	player.moving=0;
	player.speed=6.0;
	player.max_speed=6.0;
	player.coins=0;
	player.level=0;
	player.lvld=0;
	player.lvling=0;
	player.exp = 0;
	player.health=50;
	player.max_health=50;
	player.mana= 100;
	player.max_mana= 100;
	player.delta_mana= 10;
	player.type=sp;
	player.cooldown=1;
	player.max_cooldown=1.0;
	player.damage=4;
	player.max_damage=4;
	player.bullet_speed=40;
	player.inventory.n_items=0;
	player.flags=0;
	player.resist=1;
	player.hit_radius = 1.0;
	player.lucky_sevens=0;
	player.score=0;
	portal_timer=5;
	max_portal_timer=15;
	player.color = (struct vector) {1,1,1};
	player.inventory.item[2]=side_blaster_item();
	player.learn_rate=1.0;
	player.inventory.item[1]=pick_item();
	return player;
}

void init_fields(struct game_state * gs)
{
	wave_destruction=0.9;
}

struct game_state init_game(struct room * room,int level)
{
	room->gs = (struct game_state) {0};
	init_fields(&room->gs);
	room->gs.game_player=init_player(PLAYER_TYPE_TELE);
	room->gs.n_bullets=0;
	room->gs.n_npcs=0;
	spawn_mobs(room,level);
	return room->gs;
}

int next_level(int lvl)
{
        return 50+(50*lvl);
}

