#include <math.h>

#include "map.h"
#include "room.h"
#include "pathfinding.h"
#include "engine.h"

struct vector parent[MAX_ROOM_WIDTH][MAX_ROOM_HEIGHT];
int visited[MAX_ROOM_WIDTH][MAX_ROOM_HEIGHT];
double dis[MAX_ROOM_WIDTH][MAX_ROOM_HEIGHT];

int wall_near(int i, int j)
{
	if(!walkable(i,j)) return 1;
	if(!walkable(i,j+1)) return 1;
	if(!walkable(i+1,j+1)) return 1;
	if(!walkable(i+1,j)) return 1;

	if(!walkable(i,j-1)) return 1;
	if(!walkable(i+1,j-1)) return 1;
	if(!walkable(i-1,j-1)) return 1;
	if(!walkable(i-1,j+1)) return 1;

	return 0;
}

void init_layouts(struct vector starting)
{
	int i,j;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			visited[i][j]=0;
			dis[i][j]=120;
			parent[i][j]=(struct vector) {0,0,0};
		}
	}
	dis[(int)starting.x][(int)starting.z]=0;
}

double length(struct vector a , struct vector b)
{
	return sqrt(pow((b.x-a.x),2)+pow((b.z-a.z),2));
}

int unit_near(int x, int z)
{
	int i;
	struct game_state * gs;
	gs = &world_map.current_room->gs;
	for(i=0;i<gs->n_npcs;i++){
		if(x == (int)gs->npc[i].location.x &&
				z ==(int)gs->npc[i].location.z){
			return 1;
		}
	}
	return 0;
}

double f(struct vector goal, int i, int j)
{
	double d = dis[i][j]+length(goal,(struct vector) {i,0,j});
	if(wall_near(i,j))
		d+=20;
	if(unit_near(i,j))
		d+=20;
	return d;
}

struct vector min_dis(struct vector goal)
{
	int i,j;
	double min = 120;
	struct vector node;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			if(!visited[i][j] &&
					dis[i][j] <= min &&
					walkable(i,j)){
				double d = f(goal,i,j);
				if(d<=min){
					min=d;
					node = (struct vector) {i,0,j};
				}
			}
		}
	}
	return node;
}

void check_node(int i, int j,struct vector current)
{
	if(visited[i][j] || !walkable(i,j)){
		return;
	}

	double d = dis[(int)current.x][(int)current.z]+
		length(current,(struct vector) {i,0,j});

	if(((dis[(int)current.x][(int)current.z] != 120) &&
				(d < dis[i][j]))){
		dis[i][j] = d;
		parent[i][j] = current;
	}
}

void visit_next( struct vector goal)
{
	struct vector current = min_dis(goal);

	visited[(int)current.x][(int)current.z]=1;

	int i = (int)current.x;
	int j = (int)current.z;

	check_node(i,j+1,current);
	check_node(i+1,j,current);
	check_node(i+1,j+1,current);
	check_node(i+1,j-1,current);

	check_node(i,j-1,current);
	check_node(i-1,j,current);
	check_node(i-1,j-1,current);
	check_node(i-1,j+1,current);

	if(!wall_intersects_line(current,goal)){
		visited[(int)goal.x][(int)goal.z]++;
		parent[(int) goal.x][(int)goal.z] = current;
		return;
	}
}

int path_push(struct path * p, struct vector v)
{
	if(p->n_interpoints >= MAX_INTERPOINTS){
		printf("tried to add over N_INTERPOINTS\n");
		return -1;
	}
	p->interpoint[p->n_interpoints]=v;
	p->n_interpoints++;
	return 0;
}

struct vector path_pop(struct path * p)
{
	if(p->n_interpoints>0){
		p->n_interpoints--;
		return p->interpoint[p->n_interpoints];
	}else{
		return p->destination;
	}
}

/*read the path from the parent data*/
struct path generate_path(struct vector starting, struct vector goal)
{
	int err;
	struct path p = {0};
	p.n_interpoints=0;
	p.destination=goal;
	struct vector n;
	for(		n=goal;
	    		!((n.x == starting.x && n.z == starting.z));
	    		n=parent[(int)n.x][(int)n.z]){
		if(n.x == 0 && n.z==0)
			break;
		if(is_near(n,goal,1))
			break;
		if(v_eq(n,parent[(int)n.x][(int)n.z]))
			break;

		err = path_push(&p,n);
		if(err){
			printf("path generation error\n");
			printf("starting:(%f,%f,%f),goal:(%f,%f,%f)\n",
					starting.x,starting.y,starting.z,
					goal.x,goal.y,goal.z);

			p = (struct path) {0};
			//game_exit(-28);
			break;
		}
	}
	return p;
}

void remove_interpoint(struct path * p, int index)
{
	int i;
	for(i=index;i<p->n_interpoints;i++){
		p->interpoint[i] = p->interpoint[i+1];
	}
	p->n_interpoints--;
}

void simplify_path(struct path * p,int starting_point)
{
	int i;
	for(i=starting_point+1;i<p->n_interpoints;i++){
		if(wall_intersects_line(p->interpoint[starting_point],
					p->interpoint[i]))
			break;
		remove_interpoint(p,i);
	}
	if(i < p->n_interpoints){
		simplify_path(p,i);
	}
}

struct path pathfind(struct vector starting, struct vector goal)
{
	struct path path = {0};

	int i,j;
	i=((int)goal.x);j=((int)goal.z);

	if(!walkable(i,j) || (i<0 || j<0))
		return path;

	init_layouts(starting);

	for(i=0;i<MAX_INTERPOINTS;i++){
		visit_next(goal);
		if(visited[(int)goal.x][(int)goal.z]>3){
			path = generate_path(starting,goal);
			simplify_path(&path,0);
			break;
		}
	}
	return path;
}
