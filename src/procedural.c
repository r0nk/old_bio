#include "dimensions.h"
#include "procedural.h"
#include "neighborhood.h"
#include "room.h"
#include "map.h"
#include "engine.h"

int touching_out_of_bounds(struct layout * l, int i, int j)
{
	if(i<=0 || i>= (MAX_ROOM_WIDTH-1) || j<=0 || j>=(MAX_ROOM_HEIGHT-1))
		return 1;
	if( (i > 0)  && l->tiles[i-1][j]=='\0')
		return 1;
	if( (i < MAX_ROOM_WIDTH)  && l->tiles[i+1][j]=='\0')
		return 1;
	if( (j > 0)  && l->tiles[i][j-1]=='\0')
		return 1;
	if( (j < MAX_ROOM_HEIGHT)  && l->tiles[i][j+1]=='\0')
		return 1;
	return 0;
}

int closed(struct layout * layout)
{
	int i,j;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			if(walkable_char(layout->tiles[i][j]) && 
					touching_out_of_bounds(layout,i,j)){
				return 0;
			}
		}
	}
	return 1;
}

int is_in_room_range(int i, int j)
{
	return !((i<0) || (j<0) || (i>=MAX_ROOM_HEIGHT) || (j>=MAX_ROOM_WIDTH));
}

int is_void_space(struct layout * l, int i, int j)
{
	return (is_in_room_range(i,j) && l->tiles[i][j]=='\0');
}

void place_floor(struct layout * l,int i, int j)
{
	if(!is_in_room_range(i,j))
		return;

	l->tiles[i][j]='.';
	if(i<=0 || i>= (MAX_ROOM_WIDTH-1) || j<=0 || j>=(MAX_ROOM_HEIGHT-1))
		l->tiles[i][j]='#';

	if(is_void_space(l,i-1,j)) l->tiles[i-1][j]='#';
	if(is_void_space(l,i+1,j)) l->tiles[i+1][j]='#';
	if(is_void_space(l,i,j-1)) l->tiles[i][j-1]='#';
	if(is_void_space(l,i,j+1)) l->tiles[i][j+1]='#';

	if(is_void_space(l,i-1,j-1)) l->tiles[i-1][j-1]='#';
	if(is_void_space(l,i+1,j+1)) l->tiles[i+1][j+1]='#';
	if(is_void_space(l,i+1,j-1)) l->tiles[i+1][j-1]='#';
	if(is_void_space(l,i-1,j+1)) l->tiles[i-1][j+1]='#';
}

/*how much of this slab overlaps with other slabs */
int overlap_count(struct layout * l, struct neighborhood n)
{
	int i,j;
	int count=0;
	for(i=n.a.x;i<n.c.x;i++){
		for(j=n.a.z;j<n.c.z;j++){
			//if(l->tiles[i][j]=='#' || l->tiles[i][j]=='.'){
			if(l->tiles[i][j]=='.'){
				count++;
			}
		}
	}
	return count;
}

void place_inital_slab(struct layout * l)
{
	struct neighborhood n = {0};

	int min = 10;
	int range = 5;

	n.a.x=rand()%(MAX_ROOM_WIDTH-(min+range));
	n.a.z=rand()%(MAX_ROOM_HEIGHT-(min+range));
	n.c.x=n.a.x + (min+(rand()%range));
	n.c.z=n.a.z + (min+(rand()%range));

	int i,j;
	for(i=n.a.x;i<=n.c.x;i++){
		for(j=n.a.z;j<=n.c.z;j++){
			place_floor(l,i,j);
		}
	}
}

struct neighborhood generate_slab(struct layout * l)
{
	struct neighborhood n;
	int min = 8;
	int range = 5;
	int oc;
	do {
		n.a.x=rand()%(MAX_ROOM_WIDTH-(min+range));
		n.a.z=rand()%(MAX_ROOM_HEIGHT-(min+range));
		n.c.x=n.a.x + (min+(rand()%range));
		n.c.z=n.a.z + (min+(rand()%range));
		oc = overlap_count(l,n);
	} while ( oc<3 || oc>(min*range));

	return n;
}

void place_slab(struct layout * l)
{
	struct neighborhood n = generate_slab(l);
	int i,j;
	for(i=n.a.x;i<n.c.x;i++){
		for(j=n.a.z;j<n.c.z;j++){
			place_floor(l,i,j);
		}
	}
}

void generate_tiles(struct layout * l)
{
	int i,j;
	for(i=0;i<MAX_ROOM_WIDTH;i++){
		for(j=0;j<MAX_ROOM_HEIGHT;j++){
			l->tiles[i][j]='\0';
		}
	}

	place_inital_slab(l);

	for(i=0;i<16;i++){
		place_slab(l);
	}
}

struct light light_at(struct vector v)
{
	struct light l;
	v.y=3;
	v.x+=((rand()%10)-5)/10.0;
	v.z+=((rand()%10)-5)/10.0;
	l.location = v;
	l.origin = v;
	l.velocity=(struct vector){0};
	l.color=(struct vector){0};
	return l;
}

int light_check(struct layout * l, struct vector v,struct room * room)
{
	int i,j;
	int r = 4;
	for(i=0;i<room->n_lights;i++){
		if(distance(room->light[i].location,v)<10){
			return 0;
		}
	}
	for(i=-r;i<r;i++){
		for(j=-r;j<r;j++){
			if(l->tiles[(int)v.x+i][(int)v.z+j]!='.'){
				return 0;
			}
		}
	}
	return 1;
}

void populate_lights(struct layout *l, struct graph * g, struct room * room)
{
	struct vector v;
	int count=0;
	struct neighborhood * n;
	while(room->n_lights<4){
		if(count++>300) break;
		n = (struct neighborhood *) g->element[rand()%g->n_elements];
		v = neighborhood_midpoint(*n);
		if(!light_check(l,v,room)){
			continue;
		}
		room->light[room->n_lights]=light_at(v);;
		room->n_lights++;
	}
}

int place_door_check(struct layout * l, struct vector v, struct room * room)
{
	int i,j;
	int r = 4;
	for(i=0;i<4;i++){
		if(distance(room->doorway[i].location,v)<10.0){
			return 0;
		}
	}
	return 1;
}

int populate_doorways(struct layout * l, struct graph * g, struct room * room)
{
	struct vector v;
	int runs=0;
	int deg;
	int rando;
	struct neighborhood * n;

	while(room->n_doorways!=4){
		if(runs++>800){
			return 1;
		}

		rando=rand()%g->n_elements;

		n=(struct neighborhood *) g->element[rando];
		v = neighborhood_midpoint(*n);

		deg = degree(g,rando);
		if(!place_door_check(l,v,room))
			continue;
		if(walkable_char(l->tiles[(int)v.x][(int)v.z]) && deg<=10){
			room->doorway[room->n_doorways].location=v;
			l->tiles[(int)v.x][(int)v.z]='0';
			room->n_doorways++;
		}
	}
	return 0;
}

int populate_spawn_check(struct layout * l, struct graph * g,
	       	struct room * room,struct vector v,int deg)
{
	int i;
	for(i=0;i<room->n_doorways;i++){
		if(distance(v,room->doorway[i].location)<5)
		{
			return 0;
		}
	}
	if(walkable_char(l->tiles[(int)v.x][(int)v.z]) && deg>7){
		return 1;
	}
	return 0;
}

void populate_spawns(struct layout * l, struct graph * g, struct room * room)
{
	struct vector v;
	int i;
	int deg;
	struct neighborhood * n;
	int n_spawns=0;
	for(i=0;i<g->n_elements;i++){
		n=(struct neighborhood *) g->element[i];
		v = neighborhood_midpoint(*n);
		deg = degree(g,i);
		if(populate_spawn_check(l,g,room,v,deg)){
			l->tiles[(int)v.x][(int)v.z]='s';
			n_spawns++;
			if(n_spawns>10)
				return;
		}
	}
}

int populate_layout(struct layout * l,struct graph * g, struct room * room)
{
	int ret;
	ret = populate_doorways(l,g,room);
	populate_lights(l,g,room);
	populate_spawns(l,g,room);
	return ret;
}

void dump_neighborhoods(struct graph * g)
{
	int i;
	struct vector v;
	struct neighborhood * n;
	for(i=0;i<g->n_elements;i++){
		n=(struct neighborhood *) g->element[i];
		v = neighborhood_midpoint(*n);
		printf("neighborhood[%i]:",i);
		dump_vector(v);
		printf("\n");
	}
}

void generate_layout(struct room * room)
{
	int err = 0;
	do {
		room->n_doorways=0;
		room->n_lights=0;
		generate_tiles(&(room->layout));
		room->neighborhoods = layout_to_neighborhoods(&room->layout);
		err = populate_layout(&room->layout,&room->neighborhoods, room);
	} while (err);
	return;
}
