#include "ui.h"
#include "input.h"
#include "engine.h"

void update_button(struct button * b,struct vector pmv)
{
	if((pmv.x>b->location.x) && (pmv.x< (b->location.x+b->size.x)) &&
		(pmv.y>b->location.y) && (pmv.y< (b->location.y+b->size.y))){

		if(b->over==0){
			play_blip_sound_effect();
		}
		b->over=1;
		b->color.x=0.5;
		b->color.y=0.5;

		if(pi.left_click){
			if(!b->down){
				b->down=1;
			}
		}else{
			if(b->down && b->callback)
				b->callback();
			b->down=0;
		}
	}else{
		b->color.x=0.0;
		b->color.y=0.0;
		b->down=0;
		b->over=0;
	}
}

void update_ui(struct ui * ui)
{
	int i;
	struct vector pmv = pixel_to_screen(pi.mouse_x,pi.mouse_y);
	for(i=0;i<ui->n_buttons;i++){
		update_button(&(ui->button[i]),pmv);
	}
}

void init_ui()
{
	paused_ui = paused_menu();
	options_ui = options_menu();
	ui = &paused_ui;
	current_menu_screen=0;
}

void deinit_ui(){
	;
}
