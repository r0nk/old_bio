#include "shop.h"
#include "room.h"
#include "engine.h"
#include "map.h"

char * pick_line();

#define N_LINES 22

const char * lines[] = {
	"\" Thank you come again. \"",
	"\" Just kidding, I love mondays. \"",
	"\" Are we there yet? \"",
	"\" giff coin \"",
	"\" You cannot beat Sebulba. \"",
	"\" Have you seen my chance cube? \"",
	"\" Always wear socks. \"",
	"\" Sometimes. \"",
	"\" DETERMINATION. \"",
	"\" Build more pylons. \"",
	"\" Our scvs are under attack. \"",
	"\" No post on sundays. \"",
	"\" My life is going no where. \"",
	"\" 2b||!2b. \"",
	"\" swag. \"",
	"\" dank. \"",
	"\" Television is a spy. \"",
	"\" We couldn't afford an artist. \"",
	"\" Drag items to me to sell. \"",
	"\" Based Rexy, Mothergod of ports. \"",
	"\" Torusulous.\"",
	"\" I love money.\"",
};

struct transaction generate_transaction(struct shop * s,int x, int z)
{
	struct game_state * gs = &world_map.current_room->gs;
	struct transaction transaction;
	transaction.item=pick_item();
	int i;
	for(i=0;i<3;i++){
		while(s->t[i].item.type==transaction.item.type)
			transaction.item=pick_item();
	}
	transaction.price=5+(rand()%8);
	transaction.location = (struct vector) {x,0,z};
	transaction.sold=0;
	return transaction;
}

struct shop generate_shop(struct room * room)
{
	struct shop shop;
	int i=0;
	int x,z;
	for(z=0;z<MAX_ROOM_HEIGHT;z++){
		for(x=0;x<MAX_ROOM_WIDTH;x++){
			if(room->layout.tiles[x][z]=='K')
				shop.keeper_location=(struct vector){x,0,z};
			if(room->layout.tiles[x][z]=='i'){
				shop.t[i]=generate_transaction(&shop,x,z);
				i++;
			}
		}
	}
	shop.keeper_line = lines[rand()%N_LINES];
	return shop;
}
