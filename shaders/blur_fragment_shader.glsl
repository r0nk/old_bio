uniform vec3 colorkey;
uniform sampler2D colorMap;

vec4 blur(float x, float y)
{
	vec4 tc = gl_TexCoord[0];
	vec2 point = vec2(x,y);
	float s = 0.001;
	return texture2D(colorMap, vec2(tc.x+(point.x*s),tc.y+(point.y*s)));
}

void main () {
	vec4 sum = vec4(0.0);

	sum+=blur(-1.0,-1.0)*0.176777;
	sum+=blur(-1.0,1.0)*0.176777;
	sum+=blur(1.0,1.0)*0.176777;
	sum+=blur(1.0,-1.0)*0.176777;

	if(sum==vec4(0.0)){
		gl_FragColor = sum;
		return;
	}

	sum+=blur(-2.0,-1.0)*0.111803;
	sum+=blur(-1.0,2.0)*0.111803;
	sum+=blur(-2.0,1.0)*0.111803;
	sum+=blur(1.0,-2.0)*0.111803;
	sum+=blur(1.0,2.0)*0.111803;
	sum+=blur(2.0,-1.0)*0.111803;
	sum+=blur(-1.0,-2.0)*0.111803;
	sum+=blur(2.0,1.0)*0.111803;

	sum+=blur(-2.0,0.0)*0.125000;
	sum+=blur(0.0,-2.0)*0.125000;
	sum+=blur(0.0,2.0)*0.125000;
	sum+=blur(2.0,0.0)*0.125000;

	sum+=blur(0.0,0.0)*0.300000;

	gl_FragColor = sum;
}


