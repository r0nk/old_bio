uniform vec3 colorkey;
uniform sampler2D colorMap;

uniform vec4 tcord;

vec4 blur(float x, float y)
{
	vec4 tc = tcord;
	vec2 point = vec2(x,y);
	float s = 0.001;
	return texture2D(colorMap, vec2(tc.x+(point.x*s),tc.y+(point.y*s)));
}

vec4 blur_sum () {
	vec4 sum = vec4(0.0);

	sum+=blur(-1.0,-1.0)*0.176777;
	sum+=blur(-1.0,1.0)*0.176777;
	sum+=blur(1.0,1.0)*0.176777;
	sum+=blur(1.0,-1.0)*0.176777;

	if(sum==vec4(0.0)){
		return sum;
	}

	sum+=blur(-2.0,-1.0)*0.111803;
	sum+=blur(-1.0,2.0)*0.111803;
	sum+=blur(-2.0,1.0)*0.111803;
	sum+=blur(1.0,-2.0)*0.111803;
	sum+=blur(1.0,2.0)*0.111803;
	sum+=blur(2.0,-1.0)*0.111803;
	sum+=blur(-1.0,-2.0)*0.111803;
	sum+=blur(2.0,1.0)*0.111803;

	sum+=blur(-2.0,0.0)*0.125000;
	sum+=blur(0.0,-2.0)*0.125000;
	sum+=blur(0.0,2.0)*0.125000;
	sum+=blur(2.0,0.0)*0.125000;

	sum+=blur(0.0,0.0)*0.300000;

	return sum*0.2;
}


void main () {
	vec4 color;
	vec4 tcord = gl_TexCoord[0];
	color = texture2D(colorMap,tcord.st) * gl_Color;
	color.xyz += sin(tcord.t*2411.52) * 0.05;
	color += blur_sum();
	gl_FragColor = color;
}
