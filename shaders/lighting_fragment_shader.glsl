uniform sampler2D colorMap;

uniform vec4 light_pos;

varying vec3 N;
varying vec3 v;

void main () {
	vec4 surfaceColor = texture2D(colorMap,gl_TexCoord[0].st) * gl_Color;
	vec3 L;
	vec4 Idiff;
	vec4 final_color = vec4(0,0,0,0);
	float brightness;
	int i;
	for(i=0;i<4;i++){
		L = gl_LightSource[i].position.xyz - v;
		brightness = dot(N,L)/(pow(length(L),2.0));
		brightness = clamp(brightness,0.0,1.0);
		Idiff = gl_LightSource[i].diffuse * brightness;
		final_color += (Idiff * surfaceColor);
	}
	final_color.xyz+= (surfaceColor.xyz*0.5);
	final_color.a = surfaceColor.a;

	gl_FragColor = final_color;
}

