#!/bin/bash
#figure out how much time left (estimated) until we're finished
cat TODO bugs | grep -o "\<[0-9]*\>" | awk '{ sum += $1 } END { print sum }'
