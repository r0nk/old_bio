#Copyright 2017 Rexy712 of Rexy & Co

#Binary and Directory naming
SOURCE_DIRS:=src src/graphics
OBJ_DIR:=obj
INCLUDE_DIRS:=include
EXT:=c
DEP_DIR:=$(OBJ_DIR)/dep
EXECUTABLE:=back_into_orbit

#Compiler/Linker settings
CC:=gcc
CFLAGS=-c -g -w
LDFLAGS:=
LDLIBS:=-lgcc -lGLEW -lGL -lGLU -lX11 -lXrandr -lpthread -lXi -lm -lXinerama -lXcursor -lSDL2 -lassimp -lSOIL -lreadline  -lSDL2_mixer
windows: CC:=i686-w64-mingw32-gcc
windows: LDFLAGS:=-Wl,--stack,10000000
windows: LDLIBS:=-lmingw32 -lSOIL -lassimp -lglew32s -lopengl32 -lglu32 -lSDL2 -lSDL2_mixer -lz -Wl,-Bstatic -lstdc++ -Wl,-Bstatic -lpthread

#Multiplatform compatability bullshit
ifeq ($(OS),Windows_NT)
	mkdir=mkdir $(subst /,\,$(1)) > NUL 2>&1 || (exit 0)
	rm=del /F $(subst /,\,$(1)) > NUL 2>&1 || (exit 0)
	rmdir=rd /s /q $(subst /,\,$(1)) > NUL 2>&1 || (exit 0)
	move=move /y $(subst /,\,$(1)) $(subst /,\,$(2)) > NUL 2>&1 || (exit 0)
	EXECUTABLE:=$(EXECUTABLE).exe
else
	mkdir=mkdir -p $(1)
	rm=rm -f $(1)
	rmdir=rm -rf $(1)
	move=mv $(1) $(2)
endif

#Automatically filled vars
SOURCES:=$(foreach source,$(SOURCE_DIRS),$(foreach ext,$(EXT),$(wildcard $(source)/*.$(ext))))
OBJECTS:=$(addprefix $(OBJ_DIR)/,$(notdir $(SOURCES:.c=.o)))
CFLAGS+=$(foreach dir,$(INCLUDE_DIRS),-I"$(dir)") -MMD -MP -MF"$(DEP_DIR)/$*.Td"

all: | $(OBJ_DIR) $(DEP_DIR) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $^ -o "$(basename $@)" $(LDLIBS)

define GENERATE_OBJECTS

$(OBJ_DIR)/%.o: $(1)/%.$(EXT)
#Windows is a little bitch and can't IO properly
ifeq ($(OS),Windows_NT)
	$$(call mkdir,"$$(OBJ_DIR)")
	$$(call mkdir,"$$(DEP_DIR)")
endif
	$$(CC) $$(CFLAGS) "$$<" -o "$$@"
	$$(call move,"$$(DEP_DIR)/$$*.Td","$$(DEP_DIR)/$$*.d")

endef

$(foreach dir,$(SOURCE_DIRS),$(eval $(call GENERATE_OBJECTS,$(dir))))

$(OBJ_DIR):
	$(call mkdir,"$@")
$(DEP_DIR):
	$(call mkdir,"$@")

.PHONY: windows
windows: all

.PHONY: clean
clean:
	$(call rm,"$(EXECUTABLE)")
	$(call rmdir,"$(DEP_DIR)")
	$(call rmdir,"$(OBJ_DIR)")

-include $(wildcard $(DEP_DIR)/*.d)
