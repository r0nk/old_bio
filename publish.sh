make clean;
make -j8;
zip back_into_orbit.zip back_into_orbit layouts/* sounds/* shaders/* models/*.dae textures/*.png
butler push back_into_orbit.zip r0nk/back-into-orbit:linux-universal
rm back_into_orbit.zip
#also make the windows version
make clean; 
make windows -j8;
cp dlls/* .
zip back_into_orbit.zip back_into_orbit.exe layouts/* sounds/* shaders/* models/*.dae textures/*.png *.dll
butler push back_into_orbit.zip r0nk/back-into-orbit:windows
rm back_into_orbit.zip
rm *.dll
make clean;
